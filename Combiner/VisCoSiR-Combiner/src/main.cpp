#include "MainWindow.h"

#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Read and apply the style
    QFile styleFile("../../style/Eclippy/Eclippy.qss");
    styleFile.open(QFile::ReadOnly);
    QString style = QLatin1String(styleFile.readAll());
    app.setStyleSheet(style);

    MainWindow w;
    w.show();
    return app.exec();
}
