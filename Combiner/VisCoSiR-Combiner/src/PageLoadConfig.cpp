#include "IOnConfigLoadedListener.h"
#include <PageLoadConfig.h>
#include <ui_PageLoadConfig.h>
#include <iostream>
#include <QFileDialog>
#include <ViscosirCombiner.h>

PageLoadConfig::PageLoadConfig(QWidget *parent) :
    ViscosirPage(parent),
    ui(new Ui::PageLoadConfig)
{
    ui->setupUi(this);

    // Config files
    QObject::connect(ui->btnLoadConfig, &QPushButton::clicked, this, &PageLoadConfig::onLoadConfigClick);
    QObject::connect(ui->btnSaveConfig, &QPushButton::clicked, this, &PageLoadConfig::onSaveConfigClick);

    // Pvd files
    QObject::connect(ui->btLoadPvd1, &QPushButton::clicked, this, &PageLoadConfig::initPvd1Selection);
    QObject::connect(ui->btLoadPvd1, &QPushButton::clicked, this, &PageLoadConfig::initPvd2Selection);

    // Use PointingHandCursors for all buttons
    ui->btnLoadConfig->setCursor(QCursor(Qt::PointingHandCursor));
    ui->btLoadPvd1->setCursor(QCursor(Qt::PointingHandCursor));
}

void PageLoadConfig::onBecomingVisible()
{
    std::cout << "Entering Config" << std::endl;
}

void PageLoadConfig::onLeaving()
{
    std::cout << "Leaving Config" << std::endl;
}

void PageLoadConfig::initPvd1Selection()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select Pvd File for Dataset #1"), "", tr("Pvd Files (*.pvd)"));
    std::cout << fileName.toStdString() << std::endl;
}

void PageLoadConfig::initPvd2Selection()
{

}

void PageLoadConfig::setCallbackListener(IOnConfigLoadedListener *configLoadedlistener)
{
    mConfigLoadedlistener = configLoadedlistener;
}

void PageLoadConfig::onSaveConfigClick()
{
    auto result = mConfigManager->writeConfigToFile("../../config/debugConfig.json");
    ui->lbResultMessage->setText(QString::fromStdString(result));
}

void PageLoadConfig::onLoadConfigClick()
{
    auto result = mConfigManager->loadConfigFromFile("../../config/debugConfig.json");
    ui->lbResultMessage->setText(QString::fromStdString(result));
    mConfigLoadedlistener->onConfigLoaded();
}


PageLoadConfig::~PageLoadConfig()
{
    delete ui;
}


