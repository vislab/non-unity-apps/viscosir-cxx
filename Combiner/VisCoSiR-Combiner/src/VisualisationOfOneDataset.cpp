#include "VisualisationOfOneDataset.h"

#include <vtkOrientationMarkerWidget.h>
#include <vtkOutlineFilter.h>
#include <vtkPolyDataMapper.h>

VisualisationOfOneDataset::VisualisationOfOneDataset(){}


void VisualisationOfOneDataset::SetRenderingUtils(vtkSmartPointer<vtkRenderer> vtkRenderer, vtkSmartPointer<vtkRenderWindow> vtkRenderWindow){
    mRenderer = vtkRenderer;
    mRenderWindow = vtkRenderWindow;
}

void VisualisationOfOneDataset::OnTransformationsChange(){
    currentDataset->mVisProps->SetTransformInFilter(currentDataset->GetCombinedTransform());
}

void VisualisationOfOneDataset::SetDataset(std::shared_ptr<Dataset> dataset){
    currentDataset = dataset;
    if(currentDataset->mGrid)
    {
        std::cout << "Try to print grid" << std::endl;

        // THE GRID IS INVALID! :(

        currentDataset->mGrid->Print(std::cout);
        std::cout << "Try to set grid" << std::endl;
        currentDataset->mVisProps->mFilterTransform->SetInputData(currentDataset->mGrid);
    }
    else
    {
        std::cout << "ERROR: NO valid grid" << std::endl;
    }
}

void VisualisationOfOneDataset::SetBoundingBoxVisibility(bool isVisible, bool resetCamera){
    currentDataset->mVisProps->mShowBoundingBox = isVisible;
    if(isVisible)
    {
        mRenderer->AddActor(currentDataset->mVisProps->mActorBoundingBox);
        if(resetCamera)
            mRenderer->ResetCamera();
        mRenderWindow->Render();
    }
    else
    {
        mRenderer->RemoveActor(currentDataset->mVisProps->mActorBoundingBox);
        if(resetCamera)
            mRenderer->ResetCamera();
        mRenderWindow->Render();
    }
}

void VisualisationOfOneDataset::SetMeshVisibility(bool isVisible, bool resetCamera){
    currentDataset->mVisProps->mShowMesh = isVisible;
    if(isVisible)
    {
        mRenderer->AddActor(currentDataset->mVisProps->mActorWireframe);
        if(resetCamera)
            mRenderer->ResetCamera();
        mRenderWindow->Render();
    }
    else
    {
        mRenderer->RemoveActor(currentDataset->mVisProps->mActorWireframe);
        if(resetCamera)
            mRenderer->ResetCamera();
        mRenderWindow->Render();
    }
}

void VisualisationOfOneDataset::UpdateColor(VisualisationProperties::colorProp colorProp, vtkColor3d colorValue)
{
    currentDataset->mVisProps->UpdateColor(colorProp, colorValue);
    mRenderWindow->Render();
}


