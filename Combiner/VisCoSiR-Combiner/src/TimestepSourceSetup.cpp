#include "TimestepSourceSetup.h"
#include "ui_TimestepSourceSetup.h"

#include <Dataset.h>
#include <QPushButton>

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <map>

using std::cout; using std::cerr;
using std::endl; using std::string;
using std::ifstream; using std::ostringstream;
using std::istringstream;


const QString TimestepSourceSetup::SOURCE_NONE = "Please select timestep source";
const QString TimestepSourceSetup::SOURCE_PVD = "From pvd file";
const QString TimestepSourceSetup::SOURCE_VTU = "From vtu-filename";
const QString TimestepSourceSetup::SOURCE_CSV = "From csv file";

struct CalcMode { QString displayName, calcString; };

const CalcMode CALC_NONE = { "None", "time" };
const CalcMode CALC_SEC2MIN = { "Seconds -> Minutes", "time / 60.0" };
const CalcMode CALC_SEC2HRS = { "Seconds -> Hours", "time / 3600.0" };
const CalcMode CALC_CUSTOM = { "Custom", "time" };

TimestepSourceSetup::TimestepSourceSetup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TimestepSourceSetup)
{
    ui->setupUi(this);
    mRawTimevaluesFromSource = {};

    // Fill Source Dropdown and hide source-layouts
    QStringList list=(QStringList()<<SOURCE_NONE<<SOURCE_PVD<<SOURCE_VTU<<SOURCE_CSV);
    ui->cbTimeSource->addItems(list);
    ui->widgetFilenames->setVisible(false);
    ui->widgetCSV->setVisible(false);
    ui->widgetPVD->setVisible(false);
    ui->tablePreview->setVisible(false);

    // Prepare error-message-label
    ui->lbErrormessage->setVisible(false);
    ui->lbErrormessage->setStyleSheet("color : red;");

    // Fill column index list
    ui->cbColumn->addItems(QStringList() << "0" << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9" << "10");

    // Fill Calculation Dropdown
    ui->cbTimestepCalculation->addItem(CALC_NONE.displayName, CALC_NONE.calcString);
    ui->cbTimestepCalculation->addItem(CALC_SEC2MIN.displayName, CALC_SEC2MIN.calcString);
    ui->cbTimestepCalculation->addItem(CALC_SEC2HRS.displayName, CALC_SEC2HRS.calcString);
    ui->cbTimestepCalculation->addItem(CALC_CUSTOM.displayName, CALC_CUSTOM.calcString);
    ui->tfTimestepCalculation->setText(ui->cbTimestepCalculation->currentData().toString());

    // Prepare calculation setup
    ui->widgetCalculation->setVisible(false);
    mCurrentValueForCalculation = 0;
    mSymbolTable.add_variable("time", mCurrentValueForCalculation);
    mExpression.register_symbol_table(mSymbolTable);

    // Preview list
    ui->tablePreview->setColumnCount(2);
    ui->tablePreview->setRowCount(5);

    // No selections, no editing
    ui->tablePreview->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tablePreview->setSelectionMode(QAbstractItemView::NoSelection);

    // Header background
    ui->tablePreview->setStyleSheet("QHeaderView::section { background-color:black }");

    // Bind elements
    QObject::connect(ui->btUpdatePreviewFromFilenames, &QPushButton::clicked, this, &TimestepSourceSetup::CropFilenames);
    QObject::connect(ui->btLoadFromCsv, &QPushButton::clicked, this, &TimestepSourceSetup::LoadCsv);
    QObject::connect(ui->btLoadFromPvd, &QPushButton::clicked, this, &TimestepSourceSetup::LoadPvd);
    QObject::connect(ui->btTimestepCalculation, &QPushButton::clicked, this, &TimestepSourceSetup::UpdateCalculatedValues);

    connect(ui->cbTimeSource, &QComboBox::currentTextChanged, this, &TimestepSourceSetup::OnSourceSelectionChange);
    connect(ui->cbTimestepCalculation, &QComboBox::currentTextChanged, this, &TimestepSourceSetup::OnCalculationSelectionChange);

    connect(ui->tfTimestepCalculation, &QLineEdit::textChanged, this, &TimestepSourceSetup::OnCalculationStringUserEdit);
}

void TimestepSourceSetup::OnCalculationStringUserEdit()
{
    if(ui->cbTimestepCalculation->currentText() == CALC_CUSTOM.displayName)
    {
        lastCustomCalculationString = ui->tfTimestepCalculation->text();
    }
}

void TimestepSourceSetup::OnCalculationSelectionChange()
{
    // Update the calculation string
    if(ui->cbTimestepCalculation->currentText() != CALC_CUSTOM.displayName)
    {
        ui->tfTimestepCalculation->setText(ui->cbTimestepCalculation->currentData().toString());
    }
    else {
        ui->tfTimestepCalculation->setText(lastCustomCalculationString);
    }

    // Disable/Enable text input
    bool isCustomCalcString = ui->cbTimestepCalculation->currentText() == CALC_CUSTOM.displayName;
    ui->tfTimestepCalculation->setEnabled(isCustomCalcString);
}

void TimestepSourceSetup::UpdateCalculatedValues()
{
    std::string expression_string = ui->tfTimestepCalculation->text().toStdString();
    mParser.compile(expression_string, mExpression);
    float currentResult;

    for (int i = 0; i < mRawTimevaluesFromSource.size(); ++i) {
        // Calculation
        QString alreadyCheckedSuggestion = QString::fromStdString(mRawTimevaluesFromSource.at(i));
        mCurrentValueForCalculation = alreadyCheckedSuggestion.toFloat();
        currentResult = mExpression.value();
        mDataset->mTimestepValues[i] = currentResult;
        // Display in table
        QTableWidgetItem* newItem = new QTableWidgetItem();
        newItem->setText(QString::fromStdString(std::to_string(currentResult)));
        newItem->setBackgroundColor(QColorConstants::DarkGray);
        ui->tablePreview->setItem(i, 1, newItem);
    }

    // Display in Chart
    mTimestepsPage->DisplayTimevaluesInChart(mDatasetID);
}

void TimestepSourceSetup::OnSourceSelectionChange(const QString& newSelection)
{
    // Remove the first info entry after the user selected something else
    if(ui->cbTimeSource->currentIndex()!=0 && ui->cbTimeSource->itemText(0) == SOURCE_NONE)
    {
        ui->cbTimeSource->removeItem(0);
    }

    // Hide calculator
    ui->widgetCalculation->setVisible(false);

    // Update visibilites based on source selection
    if(ui->cbTimeSource->currentText() == SOURCE_VTU)
    {
        ui->widgetFilenames->setVisible(true);
        ui->widgetCSV->setVisible(false);
        ui->widgetPVD->setVisible(false);
        ui->tablePreview->setVisible(true);
    }
    else if(ui->cbTimeSource->currentText() == SOURCE_CSV)
    {
        ui->widgetFilenames->setVisible(false);
        ui->widgetCSV->setVisible(true);
        ui->widgetPVD->setVisible(false);
        ui->tablePreview->setVisible(false);
    }
    else if(ui->cbTimeSource->currentText() == SOURCE_PVD)
    {
        ui->widgetFilenames->setVisible(false);
        ui->widgetCSV->setVisible(false);
        ui->widgetPVD->setVisible(true);
        ui->tablePreview->setVisible(false);
    }
    ui->lbErrormessage->setVisible(false);
}

void TimestepSourceSetup::CropFilenames()
{
    mRawTimevaluesFromSource = {};
    // Crop the filenames to get time value suggestions
    for (int rowIndex = 0; rowIndex < ui->tablePreview->rowCount(); ++rowIndex)
    {
        std::string originalContent = ui->tablePreview->item(rowIndex, 0)->text().toStdString();
        std::string marker = ui->tfMarker->text().toStdString();
        int indexOfMarker = originalContent.rfind(marker);
        std::string cropped = originalContent;
        if(indexOfMarker != -1)
        {
            cropped = originalContent.substr(indexOfMarker + marker.size());
        }
        mRawTimevaluesFromSource.push_back(cropped);
    }

    // Display the suggestions in the preview table
    UpdatePreviewTable();
}



void TimestepSourceSetup::LoadPvd()
{
    mRawTimevaluesFromSource = {};
    // Load the suggested time values from the pvd file
    QString fileName = ui->tfPvdFile->text();
    QFile pvdFile(fileName);
    bool fileOpenSuccess = pvdFile.open(QIODevice::ReadOnly | QIODevice::Text);
    if(fileOpenSuccess) {
        // Read xml
        QXmlStreamReader xmlReader(&pvdFile);

        while (!xmlReader.atEnd()) {
            xmlReader.readNext();

            if(xmlReader.isStartElement() && xmlReader.name().toString() == "DataSet" ){
                std::string time = xmlReader.attributes().value("timestep").toString().toStdString();
                mRawTimevaluesFromSource.push_back(time);
            }
        }
        if (xmlReader.hasError()) {
            std::cout << "ERROR in XML: " << xmlReader.errorString().toStdString() << endl;
        }
        else {
            // Display the suggestions in the preview table
            UpdatePreviewTable();
        }

    } else {
        ui->lbErrormessage->setText(QString::fromStdString("Could not open the given pvd-file"));
        ui->lbErrormessage->setVisible(true);
    }
}


void TimestepSourceSetup::LoadCsv()
{
    mRawTimevaluesFromSource = {};
    // Load the suggested time values from the csv file
    string csvFile = ui->tfCsvFile->text().toStdString();
    int columnIndex = ui->cbColumn->currentIndex();
    char delimiter = ui->tfDelimiter->text().toStdString().at(0);
    auto ss = ostringstream{};
    ifstream input_file(csvFile);

    if (!input_file.is_open())
    {
        ui->lbErrormessage->setText(QString::fromStdString("Could not open the given csv-file"));
        ui->lbErrormessage->setVisible(true);
    }
    else
    {
        ss << input_file.rdbuf();
        string fileContent = ss.str();

        istringstream sstream(fileContent);
        std::vector<string> items;
        string record;

        int counter = 0;
        while (std::getline(sstream, record))
        {
            // For each line
            istringstream line(record);
            while (std::getline(line, record, delimiter))
            {
                // For each column
                items.push_back(record);
            }
            if(columnIndex < items.size())
            {
                mRawTimevaluesFromSource.push_back(items.at(columnIndex));
            }
            else
            {
                cout << "Could not read column " << columnIndex << " of csv-file " << csvFile << endl;
            }
            items.clear();
        }


        // Display the suggestions in the preview table
        UpdatePreviewTable();
    }
}


void TimestepSourceSetup::SetCallbackReference(PageTimesteps *timestepsPage)
{
    mTimestepsPage = timestepsPage;
}

void TimestepSourceSetup::SetDataset(std::shared_ptr<Dataset> dataset, int datasetID)
{
    mDataset = dataset;
    mDatasetID = datasetID;
    ui->lbTitle->setText(QString::fromStdString(dataset->mDescriptiveName));
    InitPreviewTable(dataset->mTimestepFiles);
}

void TimestepSourceSetup::InitPreviewTable(std::vector<std::string> timestepFilenames)
{
    ui->tablePreview->clear();
    ui->tablePreview->setRowCount(timestepFilenames.size());
    ui->tablePreview->setColumnCount(2);
    ui->tablePreview->setHorizontalHeaderLabels({"Filename", "Extracted Time Preview"});
    ui->tablePreview->verticalHeader()->setVisible(false);
    ui->tablePreview->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    for (int index = 0; index < timestepFilenames.size(); ++index)
    {
        std::string fullPath = timestepFilenames.at(index);
        std::string fileName = fullPath.substr(fullPath.find_last_of("/\\") + 1);
        std::string fileNameStripped = fileName.substr(0, fileName.size() - 4);
        ui->tablePreview->setItem(index, 0, new QTableWidgetItem(QString::fromStdString(fileNameStripped)));
    }
}


void TimestepSourceSetup::UpdatePreviewTable()
{
    // Check if the number of time values matches the number of vtu-files
    if(mRawTimevaluesFromSource.size() != ui->tablePreview->rowCount())
    {
        string errorMessage = "Error: Number of vtu-files (" + std::to_string(ui->tablePreview->rowCount()) + ") does not match number of time values in csv-file (" + std::to_string(mRawTimevaluesFromSource.size()) + ")";
        ui->lbErrormessage->setText(QString::fromStdString(errorMessage));
        ui->lbErrormessage->setVisible(true);
        InitPreviewTable(mDataset->mTimestepFiles);
        return;
    }
    else
    {
        ui->lbErrormessage->setVisible(false);
    }

    // Test if all suggested time values are numbers
    ui->tablePreview->setVisible(true);
    bool areAllValuesValid = true;
    mDataset->mTimestepValues = {};
    for (int rowIndex = 0; rowIndex < ui->tablePreview->rowCount(); ++rowIndex)
    {
        QString suggestion = QString::fromStdString(mRawTimevaluesFromSource.at(rowIndex));
        // Test if the suggestion is a valid float
        bool isValid;
        float time_as_float = suggestion.toFloat(&isValid);
        QColor background;
        if (!isValid)
        {
            // No valid float cast possible
            background = QColorConstants::Red;
            areAllValuesValid = false;
        }
        else
        {
            // Valid float cast possible
            background = QColorConstants::DarkGray;
            mDataset->mTimestepValues.push_back(time_as_float);
        }
        QTableWidgetItem* newItem = new QTableWidgetItem( QString::fromStdString(std::to_string(time_as_float)));
        newItem->setBackgroundColor(background);
        ui->tablePreview->setItem(rowIndex, 1, newItem);
    }

    // Display the time values in the chart or the error-message
    if(areAllValuesValid)
    {
        // Display values and update status to success
        mTimestepsPage->DisplayTimevaluesInChart(mDatasetID);
        UpdateStatusMessage("Successfully loaded time values!", mDataset->GetColorAsHexString(), false);
    }
    else
    {
        UpdateStatusMessage("Error: Not all time values are numerical", "red");
    }
    // Set visibility for the calculation-panel
    ui->widgetCalculation->setVisible(areAllValuesValid);
}

void TimestepSourceSetup::UpdateStatusMessage(string text, string colorString, bool isTextColor)
{
    ui->lbErrormessage->setText(QString::fromStdString(text));
    ui->lbErrormessage->setVisible(true);
    if(isTextColor)
        ui->lbErrormessage->setStyleSheet(QString::fromStdString("color : " + colorString +";"));
    else
        ui->lbErrormessage->setStyleSheet(QString::fromStdString("border-radius: 4px; border: 2px solid " + colorString + ";"));
}

TimestepSourceSetup::~TimestepSourceSetup()
{
    delete ui;
}
