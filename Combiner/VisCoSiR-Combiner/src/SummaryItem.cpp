#include "SummaryItem.h"
#include "ui_SummaryItem.h"

#include <QFont>
#include <QIcon>

SummaryItem::SummaryItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SummaryItem)
{
    ui->setupUi(this);
}

SummaryItem::~SummaryItem()
{
    delete ui;
}

void SummaryItem::SetContent(std::string headline, std::string content)
{
    ui->lbHeadline->setText(QString::fromStdString(headline));
    ui->lbFirstEntry->setText(QString::fromStdString(content));
    ui->hlContent->addStretch();
    ui->lbSecondEntry->setVisible(false);
}

void SummaryItem::SetContent(std::string headline, std::string contentLeft, std::string contentRight)
{
    SetContent(headline, contentLeft, contentRight, false, false);
}

void SummaryItem::SetContent(std::string headline, std::string contentLeft, std::string contentRight, bool highlightLeft, bool highlightRight)
{
    // Set content
    ui->lbHeadline->setText(QString::fromStdString(headline));
    ui->lbFirstEntry->setText(QString::fromStdString(contentLeft));
    ui->lbSecondEntry->setText(QString::fromStdString(contentRight));
    // Make visible
    ui->lbFirstEntry->setVisible(true);
    ui->lbSecondEntry->setVisible(true);
    // Set highlights
    if(highlightLeft)
        ui->lbFirstEntry->setStyleSheet("font-weight: bold");
    if(highlightRight)
        ui->lbSecondEntry->setStyleSheet("font-weight: bold");
}

SummaryItem *SummaryItem::CreateSingleContentItem(std::string headline, std::string content)
{
    SummaryItem* newItem = new SummaryItem();
    newItem->SetContent(headline, content);
    return newItem;
}

SummaryItem *SummaryItem::CreateContentItem(std::string headline, std::string contentLeft, std::string contentRight, bool isHighlighted)
{
    SummaryItem* newItem = new SummaryItem();
    newItem->SetContent(headline, contentLeft, contentRight, isHighlighted, isHighlighted);
    return newItem;
}

SummaryItem *SummaryItem::CreateCheckItem(std::string headline, bool isLeftSelected, bool isRightSelected)
{
    SummaryItem* newItem = new SummaryItem();
    newItem->ui->lbHeadline->setText(QString::fromStdString(headline));
    QIcon* iconCheck = new QIcon(":/icons/checkmark_white.png");
    QIcon* iconCross = new QIcon(":/icons/cross_grey.png");
    if(isLeftSelected)
        newItem->ui->lbFirstEntry->setPixmap(iconCheck->pixmap(QSize(30, 30)));
    else
        newItem->ui->lbFirstEntry->setPixmap(iconCross->pixmap(QSize(30, 30)));
    if(isRightSelected)
        newItem->ui->lbSecondEntry->setPixmap(iconCheck->pixmap(QSize(30, 30)));
    else
        newItem->ui->lbSecondEntry->setPixmap(iconCross->pixmap(QSize(30, 30)));
    delete(iconCross);
    delete(iconCheck);
    return newItem;
}

SummaryItem *SummaryItem::CreateSelectionItem(std::string headline, bool isLeftSelected, bool isRightSelected)
{
    SummaryItem* newItem = new SummaryItem();
    newItem->ui->lbHeadline->setText(QString::fromStdString(headline));
    QIcon* iconSelected = new QIcon(":/icons/radio_filled.png");
    QIcon* iconUnselected = new QIcon(":/icons/radio_empty.png");
    if(isLeftSelected)
        newItem->ui->lbFirstEntry->setPixmap(iconSelected->pixmap(QSize(30, 30)));
    else
        newItem->ui->lbFirstEntry->setPixmap(iconUnselected->pixmap(QSize(30, 30)));
    if(isRightSelected)
        newItem->ui->lbSecondEntry->setPixmap(iconSelected->pixmap(QSize(30, 30)));
    else
        newItem->ui->lbSecondEntry->setPixmap(iconUnselected->pixmap(QSize(30, 30)));
    delete(iconSelected);
    delete(iconUnselected);
    return newItem;
}
