#include "VisualisationProperties.h"
#include "vtkProperty.h"

#include <vtkArrowSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkAxesActor.h>
#include <vtkOrientationMarkerWidget.h>

VisualisationProperties::VisualisationProperties()
{

    // Create a Transform Filter
    mFilterTransform = vtkTransformFilter::New();
    mFilterTransform->SetTransform(vtkTransform::New());


    // BOUNDING BOX (from Transform Filter Output)
    // Filter
    mFilterBoundingBox = vtkOutlineFilter::New();
    mFilterBoundingBox->SetInputConnection(mFilterTransform->GetOutputPort());
    // Mapper
    vtkNew<vtkPolyDataMapper> mapperBB;
    mapperBB->SetInputConnection(mFilterBoundingBox->GetOutputPort());
    // Actor
    mActorBoundingBox = vtkActor::New();
    mActorBoundingBox->SetMapper(mapperBB);
    mActorBoundingBox->GetProperty()->SetColor(0, 0, 0);

    // WIREFRAME (from Transform Filter Output)
    // Mapper
    vtkNew<vtkDataSetMapper> mapperMesh;
    mapperMesh->SetInputConnection(mFilterTransform->GetOutputPort());
    // Actor
    mActorWireframe = vtkActor::New();
    mActorWireframe->SetMapper(mapperMesh);
    mActorWireframe->GetProperty()->SetColor(0, 0, 0);
    mActorWireframe->GetProperty()->EdgeVisibilityOn();
    mActorWireframe->GetProperty()->SetEdgeColor(0,0,0);
    mActorWireframe->GetProperty()->SetDiffuseColor(9,9,9);
    mActorWireframe->GetProperty()->SetRepresentationToWireframe();

}



void VisualisationProperties::UpdateColor(colorProp colorProp, vtkColor3d colorValue)
{
    switch (colorProp) {
    case boundingBox:
        mActorBoundingBox->GetProperty()->SetColor(colorValue.GetData());
        break;
    default: // all
        mActorBoundingBox->GetProperty()->SetColor(colorValue.GetData());
        break;
    }
}

void VisualisationProperties::SetTransformInFilter(vtkSmartPointer<vtkTransform> currTransformation){
    mFilterTransform->SetTransform(currTransformation);
}
