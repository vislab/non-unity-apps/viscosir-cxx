#include "ParameterCombination.h"
#include "Model/ParameterCombinationModel.h"
#include "ui_ParameterCombination.h"
#include <iostream>

ParameterCombination::ParameterCombination(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ParameterCombination)
{
    ui->setupUi(this);

    this->setStyleSheet(".QFrame { background-color: #626262; border: 1px solid #303030; border-radius: 10px; margin-top: 15px; margin-bottom: 15px; }");

    // Prepare button
    ui->btDelete->setCursor(QCursor(Qt::PointingHandCursor));
    QObject::connect(ui->btDelete, &QPushButton::clicked, this, [=](){ mUserInputHandler->onDeleteButtonClick(this); });
}

ParameterCombination::~ParameterCombination()
{
    delete ui;
}

void ParameterCombination::SetDatasets(std::shared_ptr<Dataset> ds1, std::shared_ptr<Dataset> ds2)
{
    ui->psLeft->SetDataset(ds1);
    ui->psRight->SetDataset(ds2);
}

void ParameterCombination::SetUserInputHandler(IParameterCombinationUserInputHandler *userInputHandler)
{
    this->mUserInputHandler = userInputHandler;
}

ParameterCombinationModel ParameterCombination::getData()
{
    ParameterCombinationModel data = ParameterCombinationModel();
    data.resultName = ui->tfName->text().toStdString();
    data.mapDatasetNameToArrayName.clear();
    data.mapDatasetNameToArrayName[ui->psLeft->getDatasetName()] = ui->psLeft->getSelectedArrayName();
    data.mapDatasetNameToArrayName[ui->psRight->getDatasetName()] = ui->psRight->getSelectedArrayName();
    return data;
}
