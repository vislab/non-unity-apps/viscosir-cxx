#include "CombinerPreprocessingPipeline.h"
#include "vtkPointData.h"

#include <vtkXMLUnstructuredGridReader.h>



CombinerPreprocessingPipeline::CombinerPreprocessingPipeline(std::shared_ptr<Dataset> inputDataset, std::vector<std::string> namesOfRelevantArrays, vtkDataSet* spatialReference, int currentTimestepIndex)
{
    TemporalInterpolationInfo* interInfo = &inputDataset->mTemporalInterpolationInfos[currentTimestepIndex];

    // Read the two timesteps from files
    mReaderFirstTimestep = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
    mReaderFirstTimestep->SetFileName(interInfo->fileTimestep1.c_str());
    mReaderFirstTimestep->Update();
    mReaderSecondTimestep = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
    mReaderSecondTimestep->SetFileName(interInfo->fileTimestep2.c_str());
    mReaderSecondTimestep->Update();

    // Temporal interpolation of the two relevant timesteps
    mTimestepInterpolationFilter = vtkSmartPointer<vtkProgrammableFilter>::New();
    ViscosirCombiner::ParamsTemporalInterpolation paramsTemporalInterpolation;
    paramsTemporalInterpolation.namesOfRelevantArrays = namesOfRelevantArrays;
    paramsTemporalInterpolation.ratioFirst = interInfo->ratioFirst;
    paramsTemporalInterpolation.pointDataFirstTS = mReaderFirstTimestep->GetOutput()->GetPointData();
    paramsTemporalInterpolation.pointDataSecondTS = mReaderSecondTimestep->GetOutput()->GetPointData();
    paramsTemporalInterpolation.filter = mTimestepInterpolationFilter;
    mTimestepInterpolationFilter->SetInputConnection(mReaderSecondTimestep->GetOutputPort());
    mTimestepInterpolationFilter->SetExecuteMethod(ViscosirCombiner::InterpolateTwoTimesteps, &paramsTemporalInterpolation);

    // Apply Transform
    mTransformFilter = vtkSmartPointer<vtkTransformFilter>::New();
    mTransformFilter->SetTransform(inputDataset->GetCombinedTransform());
    mTransformFilter->SetInputConnection(mTimestepInterpolationFilter->GetOutputPort());

    // Resample the datasets based on the spatial referenceGrid
    mResamplingFilter = vtkSmartPointer<vtkResampleWithDataSet>::New();
    // Input specifies the 'structure-provider'
    mResamplingFilter->SetInputData(spatialReference);
    // Source specifies the 'values-provider'
    mResamplingFilter->SetSourceConnection(mTransformFilter->GetOutputPort());
    mResamplingFilter->Update();
}

vtkDataObject* CombinerPreprocessingPipeline::GetOutput()
{
    return mResamplingFilter->GetOutput();
}

vtkAlgorithmOutput* CombinerPreprocessingPipeline::GetOutputPort()
{
    return mResamplingFilter->GetOutputPort();
}
