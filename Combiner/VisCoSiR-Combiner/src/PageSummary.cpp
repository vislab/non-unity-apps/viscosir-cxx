#include "SummaryItem.h"
#include "qfuturewatcher.h"

#include <PageSummary.h>
#include <ui_PageSummary.h>
#include <iostream>
#include <QLabel>
#include <QFileDialog>
#include <QtConcurrent/QtConcurrent>

PageSummary::PageSummary(QWidget *parent) :
    ViscosirPage(parent),
    ui(new Ui::PageSummary)
{
    ui->setupUi(this);

    ui->btStartCalculation->setCursor(QCursor(Qt::PointingHandCursor));
    QObject::connect(ui->btStartCalculation, &QPushButton::clicked, this, &PageSummary::StartTheCalculation);

}

void PageSummary::StartTheCalculation()
{
    outFolder = QFileDialog::getExistingDirectory(this, tr("Output Folder"), "/");
    if(!outFolder.isNull() && !outFolder.isEmpty())
    {
        auto futureWatcher = new QFutureWatcher<void>(this);
        QObject::connect(futureWatcher, &QFutureWatcher<void>::finished, this, &PageSummary::OnComputationFinished);
        QObject::connect(futureWatcher, &QFutureWatcher<void>::finished, futureWatcher, &QFutureWatcher<void>::deleteLater);
        auto future = QtConcurrent::run( this, &PageSummary::RunCalculation );
        futureWatcher->setFuture(future);
    }
}

void PageSummary::RunCalculation()
{
    ViscosirCombiner::calculateFullCombination(mDatasetManager, mConfigManager, outFolder.toStdString(), this);
}


void PageSummary::UpdateStatusOfCalculation(int countDone, int countAll)
{
    std::string message = "Calculation is running ... " + std::to_string(countDone) + "/" + std::to_string(countAll);
    ui->lbCalculationInfo->setText( QString::fromStdString(message) );
}


void PageSummary::OnComputationFinished()
{
    ui->lbCalculationInfo->setText("The calculation has been completed!");
}


void PageSummary::onBecomingVisible()
{
    std::cout << "Entering Summary" << std::endl;

    // CLEAR ITEMS
    QVBoxLayout* layout =  qobject_cast<QVBoxLayout*>(ui->vlSummaryItemsHolder->layout());
    while (QLayoutItem* item = layout->takeAt(0))
    {
        if (QWidget* widget = item->widget())
            widget->deleteLater();
        delete item;
    }

    // ITEM Datasets
    SummaryItem* datasetsItem = SummaryItem::CreateContentItem("", mConfigManager->firstDataset->mDescriptiveName, mConfigManager->secondDataset->mDescriptiveName, true);
    AddNewItemToLayout(datasetsItem);

    // ITEM Timesteps
    auto firstCountTS = std::to_string(mConfigManager->firstDataset->mTimestepFiles.size());
    auto secondCountTS = std::to_string(mConfigManager->secondDataset->mTimestepFiles.size());
    SummaryItem* timestepsItem = SummaryItem::CreateContentItem("Timesteps", firstCountTS, secondCountTS, false);
    AddNewItemToLayout(timestepsItem);

    // ITEM Time Values
    auto firstValidTimevalues = mConfigManager->firstDataset->mTimestepFiles.size() == mConfigManager->firstDataset->mTimestepValues.size();
    auto secondValidTimevalues = mConfigManager->secondDataset->mTimestepFiles.size() == mConfigManager->secondDataset->mTimestepValues.size();
    SummaryItem* timevaluesItem = SummaryItem::CreateCheckItem("Time-Values", firstValidTimevalues, secondValidTimevalues);
    AddNewItemToLayout(timevaluesItem);

    // ITEM Transforms
    auto firstHasTF = mConfigManager->firstDataset->mTransformationList.size() > 0;
    auto secondHasTF = mConfigManager->secondDataset->mTransformationList.size() > 0;
    SummaryItem* transformsItem = SummaryItem::CreateCheckItem("Transform applied", firstHasTF, secondHasTF);
    AddNewItemToLayout(transformsItem);

    // ITEM Spatial Structure
    SummaryItem* spatialStructureItem = SummaryItem::CreateSelectionItem("Spatial Structure for Combination:", mConfigManager->structMode==0, mConfigManager->structMode==1);
    AddNewItemToLayout(spatialStructureItem);

    // ITEM Temporal Structure
    SummaryItem* temporalStructureItem = SummaryItem::CreateSelectionItem("Temporal Structure for Combination:", mConfigManager->timestepSource==0, mConfigManager->timestepSource==1);
    AddNewItemToLayout(temporalStructureItem);


    // SEPARATOR
    QFrame *line;
    line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    ui->vlSummaryItemsHolder->addSpacing(40);
    AddNewItemToLayout(line);
    ui->vlSummaryItemsHolder->addSpacing(40);


    // ITEM Reference
    std::string referenceText = "       ";
    switch (mConfigManager->referenceForCalculation) {
    case ConfigurationManager::CALC_REFERENCE_AVG:
        referenceText += "Average";
        break;
    case ConfigurationManager::CALC_REFERENCE_FIRST:
        referenceText += mConfigManager->firstDataset->mDescriptiveName;
        break;
    case ConfigurationManager::CALC_REFERENCE_SECOND:
        referenceText += mConfigManager->secondDataset->mDescriptiveName;
        break;
    default:
        break;
    }
    SummaryItem* referenceItem = SummaryItem::CreateSingleContentItem("Reference for calculation:", referenceText);
    AddNewItemToLayout(referenceItem);

    // ITEM Metrics
    std::string metricsSummary = "       ";
    if(mConfigManager->calculateDeviationMax) metricsSummary += "Deviation maximum, ";
    if(mConfigManager->calculateDeviationMaxAbsolute) metricsSummary += "L-infinity norm, ";
    if(mConfigManager->calculateL2Norm) metricsSummary += "L-2 norm";
    if(metricsSummary.at(metricsSummary.size()-2) == ',')
       metricsSummary.erase(metricsSummary.size()-2);
    SummaryItem* metricsItem = SummaryItem::CreateSingleContentItem("Metrics to be calculated:", metricsSummary);
    AddNewItemToLayout(metricsItem);

    // ITEM Arrays to be calculated
    std::string arraysText = "       ";
    for (ParameterCombinationModel combi : mConfigManager->arraysToBeCalculated) {
        arraysText += combi.resultName + ", ";
    }
    if(arraysText.at(arraysText.size()-2) == ',')
       arraysText.erase(arraysText.size()-2);
    SummaryItem* arraysToBeCalculatedItem = SummaryItem::CreateSingleContentItem("Arrays to be calculated:", arraysText);
    AddNewItemToLayout(arraysToBeCalculatedItem);
}



void PageSummary::AddNewItemToLayout(QWidget* newItem)
{
    QVBoxLayout* layout =  qobject_cast<QVBoxLayout*>(ui->vlSummaryItemsHolder->layout());
    layout->insertWidget(((ui->vlSummaryItemsHolder->children().size() -1)), newItem);
}



void PageSummary::onLeaving()
{
    std::cout << "Leaving Summary" << std::endl;
}


void PageSummary::setDatasetManager(std::shared_ptr<DatasetManager> newDatasetManager)
{
    mDatasetManager = newDatasetManager;
}


PageSummary::~PageSummary()
{
    delete ui;
}


