#include "TransformSetup.h"
#include <TransformSetup.h>
#include <FileUtilsForVTK.h>
#include <QFile>
#include <QMessageBox>
#include <QMovie>
#include <QString>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <ui_TransformSetup.h>
#include <PageTransform.h>
#include <TransformSetup.h>
#include <Transformation.h>

TransformSetup::TransformSetup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TransformSetup)
{
    ui->setupUi(this);
    QObject::connect(
                ui->btAddTransformation, &QPushButton::clicked,
                this, &TransformSetup::AddTransforms);
}

TransformSetup::~TransformSetup()
{
    delete ui;
}

void TransformSetup::SetCallbackReference(PageTransform *transformsPage){
    mTransformsPage = transformsPage;
}

void TransformSetup::SetDataset(std::shared_ptr<Dataset> dataset)
{
    //set member variable dataset
    mDataset = dataset;
    //set label to descriptive name from dataset
    ui->lbNameDs->setText(QString::fromStdString(mDataset->mDescriptiveName));
    //set highlightcolor based on dataset color
    vtkStdString colorString = mDataset->GetColorAsHexString();
    QString style = QString::fromStdString("border-radius: 4px; border: 2px solid " + colorString + ";");
    ui->lbNameDs->setStyleSheet(style);
}

void TransformSetup::AddTransforms(){
    Transformation* newTrafo = new Transformation();
    newTrafo->SetTransformationSetup(this);
    QVBoxLayout* layout =  qobject_cast<QVBoxLayout*>(ui->vlTranformationContainer -> layout());
    layout -> insertWidget(((ui->vlTranformationContainer->children().size() -2)), newTrafo);
    onUserInputInTransformation();
}

void TransformSetup::updateTransformListForDataSets(){
    mDataset->mTransformationList.clear();
    QList <Transformation*> uiList = ui->vlTranformationContainer->findChildren<Transformation*>();
    foreach (Transformation* uiElement, uiList) {
       if(uiElement->IsDisabled() == false){
           mDataset->mTransformationList.append(uiElement);
       }
    }
    //mDataset->mTransformationList.append(ui->vlTranformationContainer->findChildren<Transformation*>());
}

void TransformSetup::updateTransformVisualisation(){
    mTransformsPage->DisplayTransformedDataset();
}

void TransformSetup::onUserInputInTransformation()
{
    updateTransformListForDataSets();
    updateTransformVisualisation();
}

void TransformSetup::onDeleteButtonClickInTransformation(QPushButton* btnRemove)
{
    delete btnRemove->parentWidget();
    onUserInputInTransformation();
}

