#include <PageTransform.h>
#include <ui_PageTransform.h>
#include <iostream>

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkDataSetMapper.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <QScreen>
#include <Dataset.h>
#include <DatasetManager.h>
#include <VisualisationOfOneDataset.h>

#include <vtkActor.h>
#include <vtkArrowSource.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkTextProperty.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkCaptionActor2D.h>

PageTransform::PageTransform(QWidget *parent) :
    ViscosirPage(parent),
    ui(new Ui::PageTransform),
    mRenderWindow(vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New()),
    mRenderer(vtkSmartPointer<vtkRenderer>::New()),
    mInteractor(vtkSmartPointer<QVTKInteractor>::New()),
    mInteractorStyle(vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New()),
    mColors(vtkNamedColors::New()),
    mOm2(vtkSmartPointer<vtkOrientationMarkerWidget>::New())
{
    ui->setupUi(this);
    //set callback reference
    ui->transformSetupOne->SetCallbackReference(this);
    ui->transformSetupTwo->SetCallbackReference(this);

    // Set up the rendering
    mRenderWindow->AddRenderer(mRenderer);
    mRenderWindow->SetInteractor(mInteractor);
    ui->openGLWidgetTransform->SetRenderWindow(mRenderWindow);
    mInteractor->SetInteractorStyle(mInteractorStyle);
    mInteractor->Initialize();
    mRenderer->GetActiveCamera()->Azimuth(45);
    mRenderer->GetActiveCamera()->Elevation(45);

    // Instantiate visualisations for the two datasets
    mVisualisationOfFirstDataset = new VisualisationOfOneDataset();
    mVisualisationOfSecondDataset = new VisualisationOfOneDataset();
    mVisualisationOfFirstDataset->SetRenderingUtils(mRenderer, mRenderWindow);
    mVisualisationOfSecondDataset->SetRenderingUtils(mRenderer, mRenderWindow);

    // Set the background color
    mRenderer->SetBackground(mColors->GetColor3d("Wheat").GetData());

    // Create and add Axes
    mActorAxes = MakeAxesActor();
    mOm2->SetOrientationMarker(mActorAxes);
    mOm2->SetInteractor(mInteractor);
    mOm2->EnabledOn();
    mOm2->InteractiveOn();

    // Listen to selection changes of the combo boxes
    connect(ui->cbDisplayStyle, &QComboBox::currentTextChanged, this, &PageTransform::DisplayTransformedDataset);
    connect(ui->cbSpatialStructure, &QComboBox::currentTextChanged, this, &PageTransform::OnSpatialStructureSelectionChange);
}

void PageTransform::onBecomingVisible()
{
    std::cout << "Entering Transform" << std::endl;
    DisplayDatasets();
    ui->transformSetupOne->SetDataset(mDatasetManager->firstDataset);
    ui->transformSetupTwo->SetDataset(mDatasetManager->secondDataset);

    // Update splitter size to match equally used space
    int halfWidth = (int) ui->splitter->width() / 2.0f;
    QList<int> sizeList = {halfWidth, halfWidth};
    ui->splitter->setSizes(sizeList);

    // Fill or update ComboBox
    auto previousSelection = 0;
    if(ui->cbSpatialStructure->count() != 0)
        previousSelection = ui->cbSpatialStructure->currentIndex();
    ui->cbSpatialStructure->clear();

    QPixmap pixmap1(100,100);
    pixmap1.fill(mDatasetManager->firstDataset->GetColorAsQColor());
    QIcon icon1(pixmap1);
    ui->cbSpatialStructure->addItem(icon1, QString::fromStdString(mDatasetManager->firstDataset->mDescriptiveName));

    QPixmap pixmap2(100,100);
    pixmap2.fill(mDatasetManager->secondDataset->GetColorAsQColor());
    QIcon icon2(pixmap2);
    ui->cbSpatialStructure->addItem(icon2, QString::fromStdString(mDatasetManager->secondDataset->mDescriptiveName));
    ui->cbSpatialStructure->setCurrentIndex(previousSelection);

}

void PageTransform::onLeaving()
{
    std::cout << "Leaving Transform" << std::endl;
}

void PageTransform::DisplayDatasets()
{
    // Display Bounding Box and Mesh
    mVisualisationOfFirstDataset->SetDataset(mDatasetManager->firstDataset);
    mVisualisationOfFirstDataset->SetBoundingBoxVisibility(true, true);
    mVisualisationOfSecondDataset->SetDataset(mDatasetManager->secondDataset);
    mVisualisationOfSecondDataset->SetBoundingBoxVisibility(true, true);
}

void PageTransform::OnSpatialStructureSelectionChange(){
    mConfigManager->structMode = ui->cbSpatialStructure->currentIndex();
}

    void PageTransform::DisplayTransformedDataset(){
    // Display Bounding Box and Mesh
    mVisualisationOfFirstDataset->OnTransformationsChange();
    mVisualisationOfSecondDataset->OnTransformationsChange();
    mVisualisationOfFirstDataset->SetBoundingBoxVisibility(ui->cbDisplayStyle->currentIndex()==0, false);
    mVisualisationOfSecondDataset->SetBoundingBoxVisibility(ui->cbDisplayStyle->currentIndex()==0, false);
    mVisualisationOfFirstDataset->SetMeshVisibility(ui->cbDisplayStyle->currentIndex()==1, false);
    mVisualisationOfSecondDataset->SetMeshVisibility(ui->cbDisplayStyle->currentIndex()==1, false);
}

void PageTransform::setDatasetManager(std::shared_ptr<DatasetManager> newDatasetManager)
{
    mDatasetManager = newDatasetManager;
}

vtkSmartPointer<vtkAxesActor> PageTransform::MakeAxesActor(){
  vtkNew<vtkAxesActor> axes;
  axes->SetScale(2.0, 2.0, 2.0);
  axes->SetShaftTypeToCylinder();
  axes->SetXAxisLabelText("X");
  axes->SetYAxisLabelText("Y");
  axes->SetZAxisLabelText("Z");
  axes->SetCylinderRadius(0.5 * axes->GetCylinderRadius());
  axes->SetConeRadius(1.025 * axes->GetConeRadius());
  axes->SetSphereRadius(1.5 * axes->GetSphereRadius());
  vtkTextProperty* tprop = axes->GetXAxisCaptionActor2D()->GetCaptionTextProperty();
  tprop->ItalicOn();
  tprop->ShadowOn();
  tprop->SetFontFamilyToTimes();
  tprop->SetFontSize(5);
  tprop->SetColor(0.0, 0.0, 0.0);
  // Use the same text properties on the other two axes.
  axes->GetYAxisCaptionActor2D()->GetCaptionTextProperty()->ShallowCopy(tprop);
  axes->GetZAxisCaptionActor2D()->GetCaptionTextProperty()->ShallowCopy(tprop);
  axes->GetXAxisCaptionActor2D()->SetHeight(0.15);
  axes->GetYAxisCaptionActor2D()->SetHeight(0.15);
  axes->GetZAxisCaptionActor2D()->SetHeight(0.15);
  return axes;
}

PageTransform::~PageTransform()
{
    delete ui;
}

