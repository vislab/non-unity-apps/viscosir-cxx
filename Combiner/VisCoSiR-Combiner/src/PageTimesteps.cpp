#include <PageTimesteps.h>
#include <ui_PageTimesteps.h>
#include <iostream>
#include <DatasetManager.h>
#include <ViscosirCombiner.h>


PageTimesteps::PageTimesteps(QWidget *parent) :
    ViscosirPage(parent),
    ui(new Ui::PageTimesteps)
{
    ui->setupUi(this);

    // Set reference for callbacks
    ui->tssOne->SetCallbackReference(this);
    ui->tssTwo->SetCallbackReference(this);

    // Prepare Selection View
    ui->wiSelection->setVisible(false);
    connect(ui->cbSelection, &QComboBox::currentTextChanged, this, &PageTimesteps::OnSelectionChange);

    connect(ui->pb_refresh, &QPushButton::clicked, this, &PageTimesteps::OnTemporalReferenceSelectionChange);

    // Prepare Chart View
    mChart = new QChart();
    mChartView = new QChartView(mChart);
    mChartView->setRenderHint(QPainter::Antialiasing);
    mChartView->setParent(ui->timestepChartHolder);
    QSizePolicy sizePolicy;
    sizePolicy.setHorizontalPolicy(QSizePolicy::Expanding);
    sizePolicy.setVerticalPolicy(QSizePolicy::Expanding);
    sizePolicy.setHorizontalStretch(5);
    mChartView->setSizePolicy(sizePolicy);
    ui->timestepChartHolder->setFixedHeight(180);
    mChartView->setFixedHeight(150);
    mChartView->setFixedWidth(1400);
    mChartView->setVisible(false);

    // Init the reference lines for the chart
    mLineseries = new QLineSeries();
    mLineseries->setName("referenceLines");
    mChart->addSeries( mLineseries );
}

void PageTimesteps::onBecomingVisible()
{
    // Init TimeSourceViews
    ui->tssOne->SetDataset(mDatasetManager->firstDataset, DatasetManager::DATASET_ID_FIRST);
    ui->tssTwo->SetDataset(mDatasetManager->secondDataset, DatasetManager::DATASET_ID_SECOND);

    // Clear items of SelectionComboBox and fill it (in case of e.g. dataset name updates)
    ui->cbSelection->blockSignals(true);
    ui->cbSelection->clear();
    QPixmap pixmap1(100,100);
    pixmap1.fill(mDatasetManager->firstDataset->GetColorAsQColor());
    QIcon icon1(pixmap1);
    ui->cbSelection->addItem(icon1, QString::fromStdString(mDatasetManager->firstDataset->mDescriptiveName));
    QPixmap pixmap2(100,100);
    pixmap2.fill(mDatasetManager->secondDataset->GetColorAsQColor());
    QIcon icon2(pixmap2);
    ui->cbSelection->addItem(icon2, QString::fromStdString(mDatasetManager->secondDataset->mDescriptiveName));
    ui->cbSelection->setCurrentIndex(mConfigManager->timestepSource);
    ui->cbSelection->blockSignals(false);
}


void PageTimesteps::onLeaving()
{
    std::cout << "Leaving Timesteps" << std::endl;
}

void PageTimesteps::updateSelectionVisibility()
{
    if(mSuccessfullyLoadedFirst && mSuccessfullyLoadedSecond)
    {
        ui->wiSelection->setVisible(true);
    }
}

void PageTimesteps::DisplayTimevaluesInChart(int datasetID)
{
    RecalculateExtremas();
    if(datasetID == DatasetManager::DATASET_ID_FIRST)
    {
        //delete previous loaded datapoints of this series in chart (avoid accumulation of (overlapping)series)
        deleteReloadedSeriesInChart("seriesOne");
        AddTimestepsToChart(seriesOne, -0.3, mDatasetManager->firstDataset->mTimestepValues, mDatasetManager->firstDataset->GetColorAsQColor(), "seriesOne");
        mSuccessfullyLoadedFirst = true;
    }
    else
    {
        //delete previous loaded datapoints of this series in chart (avoid accumulation of (overlapping)series)
        deleteReloadedSeriesInChart("seriesTwo");
        AddTimestepsToChart(seriesTwo, 0.3, mDatasetManager->secondDataset->mTimestepValues, mDatasetManager->secondDataset->GetColorAsQColor(), "seriesTwo");
        mSuccessfullyLoadedSecond = true;
    }
    mChartView->setVisible(true);
    updateSelectionVisibility();
}

void PageTimesteps::RecalculateExtremas()
{
    float min1, min2, max1, max2;
    bool firstDatasetHasTimes = !mDatasetManager->firstDataset->mTimestepValues.empty();
    bool secondDatasetHasTimes = !mDatasetManager->secondDataset->mTimestepValues.empty();

    // Find min & max in dataset 1
    if(firstDatasetHasTimes)
    {
        min1 = *min_element(mDatasetManager->firstDataset->mTimestepValues.begin(), mDatasetManager->firstDataset->mTimestepValues.end());
        max1 = *max_element(mDatasetManager->firstDataset->mTimestepValues.begin(), mDatasetManager->firstDataset->mTimestepValues.end());
    }

    // Find min & max in dataset 2
    if(secondDatasetHasTimes)
    {
        min2 = *min_element(mDatasetManager->secondDataset->mTimestepValues.begin(), mDatasetManager->secondDataset->mTimestepValues.end());
        max2 = *max_element(mDatasetManager->secondDataset->mTimestepValues.begin(), mDatasetManager->secondDataset->mTimestepValues.end());
    }

    // Combine all
    if(firstDatasetHasTimes && secondDatasetHasTimes)
    {
        mCurrentMin = std::min(min1, min2);
        mCurrentMax = std::max(max1, max2);
    } else if(!firstDatasetHasTimes && !secondDatasetHasTimes)
    {
        return;
    } else if(firstDatasetHasTimes)
    {
        mCurrentMin = min1;
        mCurrentMax = max1;
    } else if(firstDatasetHasTimes)
    {
        mCurrentMin = min2;
        mCurrentMax = max2;
    }

    // Add small percentage extra so that also timesteps at the left and right extreme are visible
    float extra = (mCurrentMax - mCurrentMin) / 100.f * 0.75f;
    mCurrentMin = mCurrentMin - extra;
    mCurrentMax = mCurrentMax + extra;
}

void PageTimesteps::deleteReloadedSeriesInChart(QString series_name){
    //delete series from chart if already displayed
    for(int i = 0; i < mChart->series().size(); i++){
        if(mChart->series()[i]->name().toStdString() == series_name.toStdString()){
            mChart->removeSeries(mChart->series()[i]);
        }
    }
}

void PageTimesteps::AddTimestepsToChart(QScatterSeries* series, float heightLevel, std::vector<float> timesteps, QColor color, QString series_name)
{
    series = new QScatterSeries();
    series->setName(series_name);
    series->setMarkerShape(QScatterSeries::MarkerShapeCircle);
    series->setMarkerSize(10.0);

    foreach (float ts, timesteps) {
        series->append(ts, heightLevel);
    }
    series->setColor(color);

    mChart->addSeries(series);

    // Update Chart Config
    mChart->legend()->setVisible(false);
    mChart->createDefaultAxes();
    mChart->axes(Qt::Orientation::Horizontal).back()->setRange(mCurrentMin, mCurrentMax);
    mChart->axes(Qt::Orientation::Vertical).back()->setRange(-0.4, 0.4);
    mChart->axes(Qt::Orientation::Vertical).back()->setVisible(false);
}

void PageTimesteps::UpdateReferenceLines(std::vector<float> timesteps, QColor color)
{
    mLineseries->clear();
    mLineseries->setColor(color);
    int factor = 1;
    for (int timestepIndex = 0; timestepIndex < timesteps.size(); ++timestepIndex) {
        factor *= -1;
        mLineseries->append( QPointF( timesteps[timestepIndex], factor * 5 ) );
        mLineseries->append( QPointF( timesteps[timestepIndex], factor * -5 ) );
    }
}

void PageTimesteps::OnSelectionChange(const QString& newSelection)
{
    mConfigManager->timestepSource = ui->cbSelection->currentIndex();
    OnTemporalReferenceSelectionChange();
}

void PageTimesteps::setDatasetManager(std::shared_ptr<DatasetManager> newDatasetManager)
{
    mDatasetManager = newDatasetManager;
}

void PageTimesteps:: OnTemporalReferenceSelectionChange()
{
    if(mSuccessfullyLoadedFirst && mSuccessfullyLoadedSecond)
    {
        // Prepare combined timesteps
        ViscosirCombiner::generateCombinedTimestepInfosForAllDatasets(mDatasetManager, mConfigManager);
        std::vector<float> interpolTimeValuesList = {};

        // Set reference dataset based on user's choice
        std::shared_ptr<Dataset> refDataset;
        if(mConfigManager->timestepSource == ConfigurationManager::TIMESTEP_SOURCE_FIRST){
            refDataset = std::make_shared<Dataset>(*mDatasetManager->firstDataset);
        }else{
            refDataset = std::make_shared<Dataset>(*mDatasetManager->secondDataset);
        }

        // Add timeValues of all temporalInterpoplation infos of the reference dataset to a new vector
        for(int i = 0; i < refDataset->mTemporalInterpolationInfos.size(); i++){
            interpolTimeValuesList.push_back(refDataset->mTemporalInterpolationInfos[i].timeValue);
        }

        // Delete previous series and add the new one to the chart
        deleteReloadedSeriesInChart("seriesCombined");
        AddTimestepsToChart(seriesCombined, 0.0, interpolTimeValuesList, QColor::fromRgb(150, 150, 150), "seriesCombined");

        // Add the reference lines to the chart
        UpdateReferenceLines( interpolTimeValuesList, QColor::fromRgb(150, 150, 150) );
    }
}

PageTimesteps::~PageTimesteps()
{
    delete ui;
}


