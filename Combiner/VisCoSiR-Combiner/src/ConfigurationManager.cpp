#include "ConfigurationManager.h"
#include "qjsonobject.h"
#include <QFile>
#include <QJsonDocument>
#include <iostream>

ConfigurationManager::ConfigurationManager()
{
    referenceForCalculation = CALC_REFERENCE_UNKNOWN;
    structMode = STRUCT_MODE_UNKNOWN;
    timestepSource = TIMESTEP_SOURCE_UNKNOWN;
    firstDataset = NULL;
    secondDataset = NULL;
}

std::string ConfigurationManager::loadConfigFromFile(std::string filePathAndName)
{
    // Load + check file
    QFile configFile(QString::fromStdString(filePathAndName));
    if (!configFile.open(QIODevice::ReadOnly)) {
        return "ERROR: Couldn't open config file " + filePathAndName;
    }

    // Read and convert to Json
    QByteArray configData = configFile.readAll();
    QJsonDocument configDocument(QJsonDocument::fromJson(configData));
    QJsonObject config = configDocument.object();

    // Set simple values based on json
    if (config.contains("referenceForCalculation") && config["referenceForCalculation"].isDouble())
        referenceForCalculation = config["referenceForCalculation"].toInt();
    if (config.contains("structMode") && config["structMode"].isDouble())
        structMode = config["structMode"].toInt();
    if (config.contains("timestepSource") && config["timestepSource"].isDouble())
        timestepSource = config["timestepSource"].toInt();

    // Set the datasets based on json
    QJsonObject configFirst =  config["firstDataset"].toObject();
    QJsonObject configSecond =  config["secondDataset"].toObject();
    firstDataset = std::make_shared<Dataset>();
    secondDataset = std::make_shared<Dataset>();
    JsonToDataset(firstDataset, configFirst);
    JsonToDataset(secondDataset, configSecond);

    return "Successfully loaded config from " + filePathAndName;
}

std::string ConfigurationManager::writeConfigToFile(std::string filePathAndName)
{
    // Create file and try to open it
    QFile configFile(QString::fromStdString(filePathAndName));
    QJsonObject configObject;
    if (!configFile.open(QIODevice::WriteOnly)) {
        return "ERROR: Couldn't open config file " + filePathAndName;
    }

    // Store all relevant simple values in the configObject
    configObject["referenceForCalculation"] = referenceForCalculation;
    configObject["structMode"] = structMode;
    configObject["timestepSource"] = timestepSource;

    // Store the first dataset
    QJsonObject firstDatasetObject;
    DatasetToJson(firstDataset, firstDatasetObject);
    configObject["firstDataset"] = firstDatasetObject;

    // Store the second dataset
    QJsonObject secondDatasetObject;
    DatasetToJson(secondDataset, secondDatasetObject);
    configObject["secondDataset"] = secondDatasetObject;

    // Write json into the file
    configFile.write(QJsonDocument(configObject).toJson());

    return "Successfully wrote config to " + filePathAndName;
}

void ConfigurationManager::DatasetToJson(std::shared_ptr<Dataset> dataset, QJsonObject &json)
{
    json["descriptiveName"] = QString::fromStdString(dataset->mDescriptiveName);
    json["timestepFolder"] = QString::fromStdString(dataset->mTimestepFolder);
    json["timestepPrefix"] = QString::fromStdString(dataset->mTimestepPrefix);
}

void ConfigurationManager::JsonToDataset(std::shared_ptr<Dataset> dataset, QJsonObject &json)
{
    if (json.contains("descriptiveName") && json["descriptiveName"].isString())
        dataset->mDescriptiveName = json["descriptiveName"].toString().toStdString();
    if (json.contains("timestepFolder") && json["timestepFolder"].isString())
        dataset->mTimestepFolder = json["timestepFolder"].toString().toStdString();
    if (json.contains("timestepPrefix") && json["timestepPrefix"].isString())
        dataset->mTimestepPrefix = json["timestepPrefix"].toString().toStdString();
}



std::vector<std::string> ConfigurationManager::getNamesOfRelevantArraysForDataset(std::string datasetName)
{
    std::vector<std::string> namesOfRelevantArrays;
    for (ParameterCombinationModel combiInfo : arraysToBeCalculated) {
        namesOfRelevantArrays.push_back(combiInfo.mapDatasetNameToArrayName[datasetName]);
    }
    return namesOfRelevantArrays;
}


bool ConfigurationManager::areBothDatasetsSet()
{
    return firstDataset!=NULL && secondDataset!=NULL;
}
