#include <Transformation.h>
#include <ui_Transformation.h>

#include <TransformSetup.h>

Transformation::Transformation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Transformation),
    mTransform(vtkSmartPointer<vtkTransform>::New())
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_AlwaysShowToolTips,true);
    SetDefaults();
    QObject::connect(
                ui->btDelete, &QPushButton::clicked,
                this, &Transformation::RemoveTransform);
    QObject::connect(
                ui->btDisable, &QPushButton::clicked,
                this, &Transformation::DisableTransform);
    QObject::connect(
                ui->tfX , &QLineEdit::textChanged ,
                this, &Transformation::onChangeByUser);
    QObject::connect(
                ui->tfY , &QLineEdit::textChanged ,
                this, &Transformation::onChangeByUser);
    QObject::connect(
                ui->tfZ , &QLineEdit::textChanged ,
                this, &Transformation::onChangeByUser);
    QObject::connect(
                ui->cbType , &QComboBox::currentTextChanged ,
                this, &Transformation::onTypeChanged);
}

void Transformation::SetDefaults(){
    std::string type = ui->cbType->currentText().toStdString();
    if(type == "Scale" && IsScaleInitialized == false){
       IsScaleInitialized = true;
       IsTranslateInitialized = false;
       IsRotateInitialized = false;
       ui->tfX->setText("1");
       ui->tfY->setText("1");
       ui->tfZ->setText("1");
    }else if(type == "Translate" && IsTranslateInitialized == false){
        IsTranslateInitialized = true;
        IsRotateInitialized = false;
        IsScaleInitialized = false;
        ui->tfX->setText("0");
        ui->tfY->setText("0");
        ui->tfZ->setText("0");
     }else if(type == "Rotate" && IsRotateInitialized == false){
        IsRotateInitialized = true;
        IsScaleInitialized = false;
        IsTranslateInitialized = false;
        ui->tfX->setText("0");
        ui->tfY->setText("0");
        ui->tfZ->setText("0");
     }

}

void Transformation::SetTransformation(std::string type, double x, double y, double z){
    mTransform->Identity();
    if(type == "Scale"){
        mTransform->Scale(x, y, z);
    }else if (type == "Rotate") {
        mTransform->RotateX(x);
        mTransform->RotateY(y);
        mTransform->RotateZ(z);
    }else if (type == "Translate") {
        mTransform->Translate(x, y, z); ;
    }
}

void Transformation::SetTransformationSetup(IUserInputHandler *userInputHandler){
    mUserInputHandler = userInputHandler;
}

void Transformation::onChangeByUser(){
    std::string type = ui->cbType->currentText().toStdString();
    double x = ui->tfX->text().toDouble();
    double y = ui->tfY->text().toDouble();
    double z = ui->tfZ->text().toDouble();
    SetTransformation(type, x, y, z);
    mUserInputHandler->onUserInputInTransformation();
}

void Transformation::onTypeChanged(){
    SetDefaults();
    onChangeByUser();
}

QPushButton *Transformation::GetDeleteBtn()
{
    QPushButton *btn = ui->btDelete;
    return btn;
}

bool Transformation::IsDisabled()
{
    bool isDisabled = false;
    QPushButton *btn = ui->btDisable;
    if(btn->text().toStdString() == "Enable"){
    isDisabled = true;
    }
    return isDisabled;
}


void Transformation::DisableTransform(){
    if(ui->btDisable->text() == "Disable"){
        ui->tfX->setDisabled(true);
        ui->tfY->setDisabled(true);
        ui->tfZ->setDisabled(true);
        ui->lbX->setDisabled(true);
        ui->lbY->setDisabled(true);
        ui->lbZ->setDisabled(true);
        ui->cbType->setDisabled(true);
        ui->btDisable->setText("Enable");
    }else{
        ui->btDisable->setText("Disable");
        ui->tfX->setDisabled(false);
        ui->tfY->setDisabled(false);
        ui->tfZ->setDisabled(false);
        ui->lbX->setDisabled(false);
        ui->lbY->setDisabled(false);
        ui->lbZ->setDisabled(false);
        ui->cbType->setDisabled(false);
    }
    onChangeByUser();
}

void Transformation::RemoveTransform(){
    QPushButton* btnRemove = qobject_cast<QPushButton*>(sender());
    mUserInputHandler->onDeleteButtonClickInTransformation(btnRemove);
}

QString Transformation::GetType()
{
    return ui->cbType->currentText();
}

Transformation::~Transformation()
{
    delete ui;
}
