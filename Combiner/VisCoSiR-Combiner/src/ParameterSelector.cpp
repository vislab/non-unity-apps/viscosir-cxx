#include "ParameterSelector.h"
#include "ui_ParameterSelector.h"

#include <vtkDataArray.h>
#include <vtkPointData.h>

ParameterSelector::ParameterSelector(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ParameterSelector)
{
    ui->setupUi(this);
}

ParameterSelector::~ParameterSelector()
{
    delete ui;
}

void ParameterSelector::SetDataset(std::shared_ptr<Dataset> dataset)
{
    // Store reference to the dataset
    mDataset = dataset;
    // Update Label
    ui->label->setText(QString::fromStdString(dataset->mDescriptiveName + "."));
    // Update color of ComboBox
    ui->comboBox->setStyleSheet(QString::fromStdString("border: 1px solid " + dataset->GetColorAsHexString()));
    // Populate ComboBox for array selections
    ui->comboBox->clear();
    vtkPointData* pointData = dataset->mGrid->GetPointData();
    for (int i = 0; i < pointData->GetNumberOfArrays(); i++)
    {
        vtkDataArray* data = pointData->GetArray(i);
        ui->comboBox->insertItem(0, QString::fromStdString(data->GetName()));
    }
}

std::string ParameterSelector::getSelectedArrayName()
{
    return ui->comboBox->currentText().toStdString();
}

std::string ParameterSelector::getDatasetName()
{
    return mDataset->mDescriptiveName;
}
