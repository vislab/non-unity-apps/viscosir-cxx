#include <FileUtilsForVTK.h>

#include <algorithm>
#include <vtkUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridReader.h>



FileUtilsForVTK::FileUtilsForVTK()
{
    std::cout << "Created instance of FileUtilsForVTK" << std::endl;
}

FileUtilsForVTK::~FileUtilsForVTK() {
}

vtkSmartPointer<vtkUnstructuredGrid> FileUtilsForVTK::ReadUnstructuredGrid(QString const& qFileName)
{

    // Record start time
    auto start = std::chrono::high_resolution_clock::now();

    auto fileName = qFileName.toStdString();
    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid;
    std::string extension = "";
    if (fileName.find_last_of(".") != std::string::npos)
    {
    extension = fileName.substr(fileName.find_last_of("."));
    }

    // Drop the case of the extension
    std::transform(extension.begin(), extension.end(), extension.begin(), ::tolower);

    if (extension == ".vtu")
    {
    vtkNew<vtkXMLUnstructuredGridReader> reader;
    reader->SetFileName(fileName.c_str());
    reader->Update();
    unstructuredGrid = reader->GetOutput();
    }
    else if (extension == ".vtk")
    {
    vtkNew<vtkUnstructuredGridReader> reader;
    reader->SetFileName(fileName.c_str());
    reader->Update();
    unstructuredGrid = reader->GetOutput();
    }

    // Record end time
    auto finish = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time for VTK File reading: " << elapsed.count() << " s\n";

    return unstructuredGrid;
}
