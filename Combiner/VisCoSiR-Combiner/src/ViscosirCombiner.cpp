#include "CombinerPreprocessingPipeline.h"
#include "ViscosirCombiner.h"
#include "qjsonarray.h"
#include "qjsonobject.h"

#include <ConfigurationManager.h>
#include <FileUtilsForVTK.h>
#include <QFile>
#include <QJsonDocument>
#include <vtkDataObject.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkResampleWithDataSet.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>


/**
 * @brief This class contains methods for the actual calculations of the combination and the writing to output files
 */
ViscosirCombiner::ViscosirCombiner(){}

/**
 * @brief For all datasets: Determine the temporal interpolation information (which original timesteps are used for which referenceTimestep and what is their ratio) for all referenceTimeValues and store the results in the dataset's  'mTemporalInterpolationInfos'
  * @param datasetManager The dataset manager, which stores relevant dataset information
 * @param configManager The config manager, which stores relevant setup information
 */
void ViscosirCombiner::generateCombinedTimestepInfosForAllDatasets(std::shared_ptr<DatasetManager> datasetManager, std::shared_ptr<ConfigurationManager> configManager)
{   
    //get timesteps from reference Dataset
    std::vector<float> referenceTimesteps;
    if(configManager->timestepSource == ConfigurationManager::TIMESTEP_SOURCE_FIRST){
        referenceTimesteps = datasetManager->firstDataset->mTimestepValues;
    }else if(configManager->timestepSource == ConfigurationManager::TIMESTEP_SOURCE_SECOND){
        referenceTimesteps = datasetManager->secondDataset->mTimestepValues;
    }

    //get min/max of all Datasets' last/first timesteps
    float sharedMinimum = referenceTimesteps.front();
    float sharedMaximum = referenceTimesteps.back();
    int amountDataSets = datasetManager->allDatasets.size();
    for(int i = 0; i < amountDataSets; i++){
        sharedMinimum = std::max(datasetManager->allDatasets[i]->mTimestepValues.front(), sharedMinimum);
        sharedMaximum = std::min(datasetManager->allDatasets[i]->mTimestepValues.back(), sharedMaximum);
    }

    // find index of first and last valid timestep in reference
    std::vector<float>::iterator refStartIndex = std::lower_bound(referenceTimesteps.begin(), referenceTimesteps.end(), sharedMinimum);
    std::vector<float>::iterator refEndIndex = std::upper_bound(referenceTimesteps.begin(), referenceTimesteps.end(), sharedMaximum);

    //erase invalid timesteps from reference list
    if(refEndIndex > refStartIndex){
        referenceTimesteps.erase(refEndIndex, referenceTimesteps.end());
        referenceTimesteps.erase(referenceTimesteps.begin(), refStartIndex);
    }

    //generate CombinedTimestepInfos for all datasets
    for(int i = 0; i < amountDataSets; i++){
        generateCombinedTimestepInfosForOneDataset(datasetManager->allDatasets[i], referenceTimesteps);
    }
}

/**
 * @brief For one dataset: Determine the temporal interpolation information (which original timesteps are used for which referenceTimestep and what is their ratio) for all referenceTimeValues and store the results in the dataset's  'mTemporalInterpolationInfos'
 * @param dataset The dataset for which the temporal interpolation information will be calculated and set
 * @param referenceTimeValues The reference time values
 */
void ViscosirCombiner::generateCombinedTimestepInfosForOneDataset(std::shared_ptr<Dataset> dataset, std::vector<float> referenceTimeValues)
{
    dataset->mTemporalInterpolationInfos.clear();
    for(int i = 0; i < referenceTimeValues.size(); i++){
        //find position of element in dataset that is >= reference value
        std::vector<float>::iterator lb = std::upper_bound(dataset->mTimestepValues.begin(),dataset->mTimestepValues.end(),referenceTimeValues[i]);

        //get index of element in dataset and compare its value to reference value; set parameters
        int index = std::distance(dataset->mTimestepValues.begin(), lb);
        if(referenceTimeValues[i] == dataset->mTimestepValues[index-1]){
            // exact match, no interpolation needed
            TemporalInterpolationInfo temporalInterpolationInfo = TemporalInterpolationInfo();
            temporalInterpolationInfo.ratioFirst = 1.0;
            temporalInterpolationInfo.fileTimestep1 = dataset->mTimestepFiles[index-1];
            temporalInterpolationInfo.fileTimestep2 = dataset->mTimestepFiles[index-1];
            temporalInterpolationInfo.timeValue = referenceTimeValues[i];
            dataset->mTemporalInterpolationInfos.push_back(temporalInterpolationInfo);
        }else{
            // no exact match, interpolation and ratio calculation needed
            TemporalInterpolationInfo temporalInterpolationInfo = TemporalInterpolationInfo();
            temporalInterpolationInfo.fileTimestep1 = dataset->mTimestepFiles[index-2];
            temporalInterpolationInfo.fileTimestep2 = dataset->mTimestepFiles[index-1];
            temporalInterpolationInfo.ratioFirst =
                std::abs(dataset->mTimestepValues[index-1] - referenceTimeValues[i]) /
                (std::abs(dataset->mTimestepValues[index-2] - dataset->mTimestepValues[index-1]));
            temporalInterpolationInfo.timeValue = referenceTimeValues[i];
            dataset->mTemporalInterpolationInfos.push_back(temporalInterpolationInfo);
        }
    }
}

/**
 * @brief Calculate the combination for all datasets and store the results as vtu-files in the outputPath
 * @param datasetManager The dataset manager, which stores relevant dataset information
 * @param configManager The config manager, which stores relevant setup information
 * @param outputPath The absolute path to the output folder (without / at the end)
 */
void ViscosirCombiner::calculateFullCombination(std::shared_ptr<DatasetManager> datasetManager, std::shared_ptr<ConfigurationManager> configManager, std::string outputPath, IOnCalculationUpdateListener* updateListener)
{
    // Find the dataset used as reference for calculations
    std::shared_ptr<Dataset> calculationReferenceDataset;
    switch (configManager->referenceForCalculation) {
    case ConfigurationManager::CALC_REFERENCE_FIRST:
        calculationReferenceDataset = datasetManager->firstDataset;
        break;
    case ConfigurationManager::CALC_REFERENCE_SECOND:
        calculationReferenceDataset = datasetManager->secondDataset;
        break;
    default:
        cout << "ERROR: Your selection for the calculation reference dataset (Average or No Reference) is currently not supported." << endl;
        break;
    }

    // Find the dataset used as spatial reference
    std::shared_ptr<Dataset> spatialReferenceDataset;
    switch (configManager->structMode) {
    case ConfigurationManager::STRUCT_MODE_USE_FIRST:
        spatialReferenceDataset = datasetManager->firstDataset;
        break;
    case ConfigurationManager::STRUCT_MODE_USE_SECOND:
        spatialReferenceDataset = datasetManager->secondDataset;
        break;
    default:
        cout << "ERROR: Your did not select a spatial reference dataset." << endl;
        break;
    }

    // Create list of datasets that are not used as reference dataset
    std::vector<std::shared_ptr<Dataset>> otherDatasets;
    for (int dsIndex = 0; dsIndex < datasetManager->allDatasets.size(); ++dsIndex) {
        if(datasetManager->allDatasets[dsIndex]->mDescriptiveName != calculationReferenceDataset->mDescriptiveName)
            otherDatasets.push_back(datasetManager->allDatasets[dsIndex]);
    }

    // Write the configuration log file
    writeCombinerConfigLogFile(calculationReferenceDataset, otherDatasets, outputPath);

    // Loop over all timesteps of the reference dataset and calculate the combination
    for (int index = 0; index < datasetManager->allDatasets[0]->mTemporalInterpolationInfos.size(); ++index) {
        updateListener->UpdateStatusOfCalculation( index, datasetManager->allDatasets[0]->mTemporalInterpolationInfos.size() );
        std::string fileName = constructAbsoluteFileNameForTimestep( index, outputPath );
        calculateOneCombination(calculationReferenceDataset, otherDatasets, configManager, spatialReferenceDataset->mGrid, index, fileName);
    }
}


/**
 * @brief Calculate the combination for ONE timestep and store the result as vtu-file in 'fileName'
 * @param calculationReferenceDataset The reference dataset to be used for calculations
 * @param otherDatasets The datasets that will be compared with the reference dataset
 * @param configManager The config manager, which stores relevant setup information
 * @param spatialReferenceGrid The spatial reference for resampling all other datasets with
 * @param timestepIndex The index of the current timestep, for which the calculation will be done
 * @param fileName The output file name (including the absolute path and file type prefix)
 */
void ViscosirCombiner::calculateOneCombination(std::shared_ptr<Dataset> calculationReferenceDataset, std::vector<std::shared_ptr<Dataset>> otherDatasets, std::shared_ptr<ConfigurationManager> configManager, vtkSmartPointer<vtkUnstructuredGrid> spatialReferenceGrid, int timestepIndex, std::string fileName)
{
    // Create the preprocessing pipeline (read files + interpolate timesteps + apply transform + interpolate spatially) for the other datasets
    std::vector<CombinerPreprocessingPipeline> prepPipesForOtherDatasets = std::vector<CombinerPreprocessingPipeline>();
    for (int datasetIndex = 0; datasetIndex < otherDatasets.size(); ++datasetIndex) {
        std::vector<std::string> namesOfRelevantArrays = configManager->getNamesOfRelevantArraysForDataset(otherDatasets[datasetIndex]->mDescriptiveName);
        prepPipesForOtherDatasets.push_back(CombinerPreprocessingPipeline(otherDatasets[datasetIndex], namesOfRelevantArrays, spatialReferenceGrid, timestepIndex));
    }

    // Create the preprocessing pipeline (read files + interpolate timesteps + apply transform + interpolate spatially) for the calculation reference dataset
    std::vector<std::string> namesOfRelevantArraysForCalcReference = configManager->getNamesOfRelevantArraysForDataset(calculationReferenceDataset->mDescriptiveName);
    CombinerPreprocessingPipeline prepPipeCalcReference = CombinerPreprocessingPipeline(calculationReferenceDataset, namesOfRelevantArraysForCalcReference, spatialReferenceGrid, timestepIndex);

    // Calculate the metrics
    vtkNew<vtkProgrammableFilter> metricsCalculationFilter;
    ParamsCalculateMetrics paramsCalculateMetrics;
    // Set the parameters for the metrics calculation
    paramsCalculateMetrics.configManager = configManager;
    paramsCalculateMetrics.dataOfTheReferenceDataset = OneDatasetForCalculation();
    paramsCalculateMetrics.dataOfTheReferenceDataset.originalDatasetName = calculationReferenceDataset->mDescriptiveName;
    paramsCalculateMetrics.dataOfTheReferenceDataset.pointData = static_cast<vtkUnstructuredGrid*>(prepPipeCalcReference.GetOutput())->GetPointData();
    paramsCalculateMetrics.dataOfTheOtherDatasets = std::vector<OneDatasetForCalculation>();
    // Collect references to all datasets that are not the calculation reference
    for (int datasetIndex = 0; datasetIndex < prepPipesForOtherDatasets.size(); ++datasetIndex) {
        paramsCalculateMetrics.dataOfTheOtherDatasets.push_back( OneDatasetForCalculation() );
        paramsCalculateMetrics.dataOfTheOtherDatasets[datasetIndex].originalDatasetName = otherDatasets[datasetIndex]->mDescriptiveName;
        paramsCalculateMetrics.dataOfTheOtherDatasets[datasetIndex].pointData = static_cast<vtkUnstructuredGrid*>(prepPipesForOtherDatasets[datasetIndex].GetOutput())->GetPointData();
    }
    paramsCalculateMetrics.filter = metricsCalculationFilter;
    metricsCalculationFilter->SetInputConnection(prepPipeCalcReference.GetOutputPort());
    metricsCalculationFilter->SetExecuteMethod(ViscosirCombiner::CalculateMetrics, &paramsCalculateMetrics);

    // Write output file
    vtkNew<vtkXMLUnstructuredGridWriter> writer;
    writer->SetInputConnection(metricsCalculationFilter->GetOutputPort());
    writer->SetFileName(fileName.c_str());
    writer->SetDataModeToBinary();
    writer->Update();
}

/**
 * @brief Write a log-file, which stores relevant meta information for the analyser, into the outputFolder
 * @param calculationReferenceDataset The reference dataset to be used for calculations
 * @param otherDatasets The datasets that will be compared with the reference dataset
 * @param outputFolder The absolute output folder
 */
void ViscosirCombiner::writeCombinerConfigLogFile(std::shared_ptr<Dataset> calculationReferenceDataset, std::vector<std::shared_ptr<Dataset> > otherDatasets, std::string outputFolder)
{
    // Create file and try to open it
    QFile configFile(QString::fromStdString(outputFolder + "/viscosir_combination_info.json"));
    QJsonObject configObject;
    if (!configFile.open(QIODevice::WriteOnly)) {
        cout << "ERROR: Couldn't create/open combination log file: " << configFile.fileName().toStdString() << endl;
    }

    // Store the name of the calculation reference dataset
    configObject["calculationReferenceDatasetName"] = QString::fromStdString( calculationReferenceDataset->mDescriptiveName );

    // Store the names of the other datasets
    QJsonArray namesOfTheOtherDatasets;
    for (int datasetIndex = 0; datasetIndex < otherDatasets.size(); ++datasetIndex) {
        namesOfTheOtherDatasets.append( QString::fromStdString( otherDatasets[datasetIndex]->mDescriptiveName ) );
    }
    configObject["otherDatasets"] = namesOfTheOtherDatasets;

    // Store the timesteps (files and time values)
    QJsonArray timesteps;
    for (int index = 0; index < calculationReferenceDataset->mTemporalInterpolationInfos.size(); ++index) {
        std::string fileName = constructAbsoluteFileNameForTimestep( index, outputFolder );
        double time = calculationReferenceDataset->mTemporalInterpolationInfos[index].timeValue;
        QJsonObject currentTimestep;
        currentTimestep["file"] = QString::fromStdString( fileName );
        currentTimestep["time"] = time;
        timesteps.append( currentTimestep );
    }
    configObject["timesteps"] = timesteps;

    // Write json into the file
    configFile.write(QJsonDocument(configObject).toJson());

    cout << "Successfully wrote combination log to: " << configFile.fileName().toStdString() << endl;
}

/**
 * @brief Helper to make sure the same file name is used everywhere (e.g. in config and fileWriter)
 * @param timestepIndex the index
 * @param outputPath the folder
 * @return the absolute file name
 */
std::string ViscosirCombiner::constructAbsoluteFileNameForTimestep(int timestepIndex, std::string outputPath)
{
    return outputPath + "/CombinerResult_TS" + std::to_string(timestepIndex) + ".vtu";
}

/**
 * @brief Functionality to interpolate between two timesteps, used for a ProgrammableFilter
 * @param paramsTemporalInterpolation Pointer to an object of type ParamsTemporalInterpolation
 */
void ViscosirCombiner::InterpolateTwoTimesteps(void* paramsTemporalInterpolation)
{
    ParamsTemporalInterpolation* parameters = static_cast<ParamsTemporalInterpolation*>(paramsTemporalInterpolation);
    vtkProgrammableFilter* programmableFilter = parameters->filter;

    // For all relevant arrays:
    for (int arrayIndex = 0; arrayIndex < parameters->namesOfRelevantArrays.size(); ++arrayIndex) {
        // Prepare array name and arrays
        const char* currentArrayName = parameters->namesOfRelevantArrays[arrayIndex].c_str();
        vtkDoubleArray* arrayFirstTS = static_cast<vtkDoubleArray*>(parameters->pointDataFirstTS->GetArray(currentArrayName));
        vtkDoubleArray* arraySecondTS = static_cast<vtkDoubleArray*>(parameters->pointDataSecondTS->GetArray(currentArrayName));
        vtkDoubleArray* arrayInterpolated = static_cast<vtkDoubleArray*>(vtkDataArray::CreateDataArray(VTK_DOUBLE));
        arrayInterpolated->DeepCopy(parameters->pointDataSecondTS->GetArray(currentArrayName));
        // Interpolate values between the two timesteps
        for (int currentIndex = 0; currentIndex < arrayInterpolated->GetSize(); ++currentIndex) {
            vtkVariant newValue = arrayFirstTS->GetValue(currentIndex) * parameters->ratioFirst + arraySecondTS->GetValue(currentIndex) * (1.0 - parameters->ratioFirst);
            arrayInterpolated->SetVariantValue( currentIndex, newValue );
        }
        // Set the interpolated array as output
        programmableFilter->GetUnstructuredGridOutput()->GetPointData()->AddArray(arrayInterpolated);
    }

}


/**
 * @brief Functionality to calculate the metrics, used for a ProgrammableFilter
 * @param paramsCalculateMetrics Pointer to an object of type ParamsCalculateMetrics
 */
void ViscosirCombiner::CalculateMetrics(void* paramsCalculateMetrics)
{
    // Store references to the parameters and the filter itself
    ParamsCalculateMetrics* parameters = static_cast<ParamsCalculateMetrics*>(paramsCalculateMetrics);
    std::shared_ptr<ConfigurationManager> configManager = parameters->configManager;
    vtkProgrammableFilter* programmableFilter = parameters->filter;
    bool isDeviationMaxNeeded = configManager->calculateDeviationMaxAbsolute || configManager->calculateDeviationMax;

    // ------------------------------------------------------
    //   START: FOR ALL ARRAYS
    // ------------------------------------------------------

    for (int arrayIndex = 0; arrayIndex < configManager->arraysToBeCalculated.size(); ++arrayIndex) {
        // Set the current combination model / information
        ParameterCombinationModel combinationInfo = configManager->arraysToBeCalculated[arrayIndex];

        // Prepare the relevant input arrays for calculation
        std::string arrayNameForReferenceDataset = combinationInfo.mapDatasetNameToArrayName[parameters->dataOfTheReferenceDataset.originalDatasetName];
        vtkDoubleArray* arrayOfReferenceDataset = static_cast<vtkDoubleArray*>( parameters->dataOfTheReferenceDataset.pointData->GetArray( arrayNameForReferenceDataset.c_str() ) );
        std::vector<vtkDoubleArray*> arraysOfOtherDatasets = std::vector<vtkDoubleArray*>();
        for (int datasetIndex = 0; datasetIndex < parameters->dataOfTheOtherDatasets.size(); ++datasetIndex) {
            std::string arrayName = combinationInfo.mapDatasetNameToArrayName[parameters->dataOfTheOtherDatasets[datasetIndex].originalDatasetName];
            arraysOfOtherDatasets.push_back( static_cast<vtkDoubleArray*>( parameters->dataOfTheOtherDatasets[datasetIndex].pointData->GetArray( arrayName.c_str() ) ) );
        }

        // Prepare the output arrays
        vtkDoubleArray* arrayOriginalReferenceValues = static_cast<vtkDoubleArray*>(vtkDataArray::CreateDataArray(VTK_DOUBLE));
        arrayOriginalReferenceValues->DeepCopy(arrayOfReferenceDataset);
        arrayOriginalReferenceValues->SetName( combinationInfo.resultName.c_str() );
        vtkDoubleArray* arrayDeviationMax = static_cast<vtkDoubleArray*>(vtkDataArray::CreateDataArray(VTK_DOUBLE));
        vtkDoubleArray* arrayDeviationAbs = static_cast<vtkDoubleArray*>(vtkDataArray::CreateDataArray(VTK_DOUBLE));
        vtkIntArray* arrayDeviationSource = static_cast<vtkIntArray*>(vtkDataArray::CreateDataArray(VTK_INT));
        vtkDoubleArray* arrayL2Norm = static_cast<vtkDoubleArray*>(vtkDataArray::CreateDataArray(VTK_DOUBLE));
        arrayDeviationMax->SetNumberOfValues(arrayOfReferenceDataset->GetNumberOfValues());
        arrayDeviationAbs->SetNumberOfValues(arrayOfReferenceDataset->GetNumberOfValues());
        arrayDeviationSource->SetNumberOfValues(arrayOfReferenceDataset->GetNumberOfValues());
        arrayL2Norm->SetNumberOfValues(arrayOfReferenceDataset->GetNumberOfValues());
        arrayDeviationMax->SetName( (combinationInfo.resultName + ARRAY_NAME_SEPARATOR + ARRAY_NAME_DEVIATION_MAX).c_str() );
        arrayDeviationAbs->SetName( (combinationInfo.resultName + ARRAY_NAME_SEPARATOR + ARRAY_NAME_DEVIATION_MAX_ABS).c_str() );
        arrayDeviationSource->SetName( (combinationInfo.resultName + ARRAY_NAME_SEPARATOR + ARRAY_NAME_DEVIATION_SOURCE).c_str() );
        arrayL2Norm->SetName( (combinationInfo.resultName + ARRAY_NAME_SEPARATOR + ARRAY_NAME_L2NORM).c_str() );

        double deviationMax, sumOfSquaredDeviations, deviationOfCurrentDataset;
        int deviationSourceDatasetID;

        // ------------------------------------------------------
        //   START: FOR ALL VALUES IN THE REFERENCE ARRAY
        // ------------------------------------------------------
        for (int currentValueIndex = 0; currentValueIndex < arrayOfReferenceDataset->GetSize(); ++currentValueIndex) {
            // Find the strongest deviation (independet of the 'direction' of deviation) and calculate the L2 norm
            if(isDeviationMaxNeeded)
            {
                deviationMax = arraysOfOtherDatasets[0]->GetValue(currentValueIndex) - arrayOfReferenceDataset->GetValue(currentValueIndex);
                deviationSourceDatasetID = 0;
            }
            if(configManager->calculateL2Norm)
                sumOfSquaredDeviations = 0.0;
            for (int datasetIndex = 0; datasetIndex < arraysOfOtherDatasets.size(); ++datasetIndex) {
                deviationOfCurrentDataset = arraysOfOtherDatasets[datasetIndex]->GetValue(currentValueIndex) - arrayOfReferenceDataset->GetValue(currentValueIndex);
                if(configManager->calculateL2Norm)
                    sumOfSquaredDeviations += deviationOfCurrentDataset * deviationOfCurrentDataset;
                if(isDeviationMaxNeeded && fabs(deviationOfCurrentDataset) > fabs(deviationMax) )
                {
                    deviationMax = deviationOfCurrentDataset;
                    deviationSourceDatasetID = datasetIndex;
                }
            }
            // Store the calculated metrics in the arrays
            if(configManager->calculateDeviationMax)
                arrayDeviationMax->SetVariantValue( currentValueIndex, deviationMax );
            if(configManager->calculateDeviationMaxAbsolute)
                arrayDeviationAbs->SetVariantValue( currentValueIndex, std::fabs(deviationMax) );
            if(isDeviationMaxNeeded)
                arrayDeviationSource->SetVariantValue( currentValueIndex, deviationSourceDatasetID );
            if(configManager->calculateL2Norm)
                arrayL2Norm->SetVariantValue( currentValueIndex, std::sqrt(sumOfSquaredDeviations) );
        }

        // ------------------------------------------------------
        //   END: FOR ALL VALUES IN THE REFERENCE ARRAY
        // ------------------------------------------------------


        // Add the calculated metrics-arrays to the output
        programmableFilter->GetUnstructuredGridOutput()->GetPointData()->AddArray(arrayOriginalReferenceValues);
        if(configManager->calculateDeviationMax)
            programmableFilter->GetUnstructuredGridOutput()->GetPointData()->AddArray(arrayDeviationMax);
        if(configManager->calculateDeviationMaxAbsolute)
            programmableFilter->GetUnstructuredGridOutput()->GetPointData()->AddArray(arrayDeviationAbs);
        if(isDeviationMaxNeeded)
            programmableFilter->GetUnstructuredGridOutput()->GetPointData()->AddArray(arrayDeviationSource);
        if(configManager->calculateL2Norm)
            programmableFilter->GetUnstructuredGridOutput()->GetPointData()->AddArray(arrayL2Norm);

    }
    // ------------------------------------------------------
    //   END: FOR ALL ARRAYS
    // ------------------------------------------------------
}












