#include "DatasetSetup.h"

#include <DatasetSetup.h>
#include <FileUtilsForVTK.h>
#include <QFile>
#include <QMessageBox>
#include <QMovie>
#include <QString>
#include <ui_DatasetSetup.h>

#include <string>
#include <iostream>
#include <filesystem>
#include <QStringListModel>
#include <QFileInfo>
#include <cctype>
#include <sstream>

namespace fs = std::filesystem;

DatasetSetup::DatasetSetup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DatasetSetup)
{
    ui->setupUi(this);

    // Hide files-table
    ui->lvFiles->setVisible(false);

    //Bind button
    QObject::connect(ui->btLoadDataset, &QPushButton::clicked, this, &DatasetSetup::onLoadDatasetClick);

    ui->btLoadDataset->setCursor(QCursor(Qt::PointingHandCursor));
}

DatasetSetup::~DatasetSetup()
{
    delete ui;
}

void DatasetSetup::SetCallbackReference(PageDataSets *dataSetsPage){
    mDataSetsPage = dataSetsPage;
}

void DatasetSetup::SetHeadline(std::string headline){
    ui->lbHeader->setText(QString::fromStdString(headline));
}

void DatasetSetup::SetDatasetID(int datasetID){
    mDatasetID = datasetID;
}

void DatasetSetup::SetNameAndFiles(std::string name, std::string filePrefix, std::string folder){
    ui->tfFilenamePrefix->setText(QString::fromStdString(filePrefix));
    ui->tfFolder->setText(QString::fromStdString(folder));
    ui->tfDescriptiveName->setText(QString::fromStdString(name));
}

void DatasetSetup::SetHighlightColor(vtkStdString colorString)
{
    QString style = QString::fromStdString("border-radius: 4px; border: 2px solid " + colorString + ";");
    ui->lbLoadSummary->setStyleSheet(style);
}

std::string DatasetSetup::GetDescriptiveName()
{
    return ui->tfDescriptiveName->text().toStdString();
}

bool DatasetSetup::isDescriptiveNameEmpty()
{
    auto name = ui->tfDescriptiveName->text();
    return name==NULL || name.isEmpty() || name.trimmed().isEmpty();
}

std::string DatasetSetup::ModifyPath(std::string path){
    for(int i = 0; i < path.length(); i++){
        if(path.back() == ' '){
            path.pop_back();
        }else{
            break;
        }
    }
    if(path.back() != '/'){
        path += "/";
    }
    return path;
}

bool DatasetSetup::IsPathValid(std::string path){
     QFileInfo check_file(QString::fromStdString(path));
    // check if file exists and if it is a directory
    return check_file.exists() && !check_file.isFile();
}

void DatasetSetup::onLoadDatasetClick()
{
    /*   Deprecated gif test
    // Load style sheet
    QFile styleFile("C:\\Users\\graeblin\\equivalent\\ufz\\VisCoSiR\\code_v2\\style\\Eclippy\\Eclippy.qss");
    styleFile.open(QFile::ReadOnly);
    QString style = QLatin1String(styleFile.readAll());

    QMessageBox msgBox;
    msgBox.setText("The dataset will be loaded.");

    QMovie *movie = new QMovie("C:\\Users\\graeblin\\Desktop\\wheel.gif");
    QLabel *processLabel = new QLabel();
    processLabel->setMovie(movie);
    movie->start();

    msgBox.layout()->addWidget(processLabel);
    msgBox.setStyleSheet(style);
    msgBox.exec();
    */

    // Create new dataset
    std::shared_ptr<Dataset> newDataset = std::make_shared<Dataset>();
    newDataset->mDescriptiveName = ui->tfDescriptiveName->text().toStdString();
    newDataset->mTimestepFolder = ui->tfFolder->text().toStdString();
    newDataset->mTimestepPrefix = ui->tfFilenamePrefix->text().toStdString();
    newDataset->mTimestepFiles = {};

    // Find all timesteps
    std::string path = ui->tfFolder->text().toStdString();
    path = ModifyPath(path);
    if(IsPathValid(path)){
        std::cout << "Searching for timesteps in: " << path << std::endl;
        for (const auto & entry : fs::directory_iterator(path))
        {
            if(!entry.is_directory())
            {
                std::string fullPath = entry.path().string();
                std::string fileName = entry.path().string().substr(path.find_last_of("/\\") + 1);
                std::string fileType = fileName.substr(fileName.find_last_of("."));
                bool hasCorrectPrefix = fileName.rfind(ui->tfFilenamePrefix->text().toStdString(), 0) == 0;
                bool hasCorrectSuffix = fileType == ".vtu";
                if (hasCorrectPrefix && hasCorrectSuffix)
                {
                    newDataset->mTimestepFiles.push_back(fullPath);
                    std::cout << fullPath << std::endl;
                }
            }
        }

        // Sort the list
        std::sort(std::begin(newDataset->mTimestepFiles), std::end(newDataset->mTimestepFiles), compareNat);

        std::string ts_case = "timesteps";
        if(newDataset->mTimestepFiles.size() == 1)
        {
            ts_case = "timestep";
        }

        // Display result (successfull loading + timestep count)
        std::string loadResult = "Finished loading (" + std::to_string(newDataset->mTimestepFiles.size()) + " " + ts_case + ")";
        ui->lbLoadSummary->setText(QString::fromStdString(loadResult));

        // Display bounding box (if at least one timestep was found)
        if(newDataset->mTimestepFiles.size() > 0)
        {
            // Fill and reveal the files-table
            QStringList fileNames = QStringList();
            for (std::string fullPath : newDataset->mTimestepFiles) {
                std::string fileName = fullPath.substr(fullPath.find_last_of("/\\") + 1);
                fileNames.append(QString::fromStdString(fileName));
            }
            QStringListModel *model = new QStringListModel(this);
            model->setStringList(fileNames);
            ui->lvFiles->setModel(model);
            ui->lvFiles->setVisible(true);

            QString fileToLoad = QString::fromStdString(newDataset->mTimestepFiles.at(0));
            std::cout << "Loading: " << fileToLoad.toStdString() << std::endl;
            auto grid = FileUtilsForVTK::ReadUnstructuredGrid(fileToLoad);
            grid->Print(std::cout);

            newDataset->mGrid = grid;
            mDataSetsPage->OnDatasetLoaded(newDataset, mDatasetID);
        }
    }else{
        cout << "Invalid Path - cant search for Timesteps" << endl;
        // Display unsuccessfull loading, reason: invalid path
        std::string msgInvalidPath = "Invalid Path!";
        ui->lbLoadSummary->setStyleSheet("color: rgb(255, 0, 0);");
        ui->lbLoadSummary->setText(QString::fromStdString(msgInvalidPath));
        ui->lvFiles->setVisible(false);

    }

}

// Natural Sorting Solution from:  https://stackoverflow.com/questions/9743485/natural-sort-of-directory-filenames-in-c
const bool DatasetSetup::compareNat(const std::string& a, const std::string& b)
{
    if (a.empty())
        return true;
    if (b.empty())
        return false;
    if (isdigit(a[0]) && !isdigit(b[0]))
        return true;
    if (!isdigit(a[0]) && isdigit(b[0]))
        return false;
    if (!isdigit(a[0]) && !isdigit(b[0]))
    {
        if (toupper(a[0]) == toupper(b[0]))
            return compareNat(a.substr(1), b.substr(1));
        return (toupper(a[0]) < toupper(b[0]));
    }

    // Both strings begin with digit --> parse both numbers
    std::istringstream issa(a);
    std::istringstream issb(b);
    int ia, ib;
    issa >> ia;
    issb >> ib;
    if (ia != ib)
        return ia < ib;

    // Numbers are the same --> remove numbers and recurse
    std::string anew, bnew;
    std::getline(issa, anew);
    std::getline(issb, bnew);
    return (compareNat(anew, bnew));
}
