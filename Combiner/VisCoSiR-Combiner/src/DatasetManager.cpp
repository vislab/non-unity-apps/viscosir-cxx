#include "DatasetManager.h"

DatasetManager::DatasetManager()
{
    simulationDatasets = std::vector<std::shared_ptr<Dataset>>();
    contextDatasets = std::vector<std::shared_ptr<Dataset>>();
    allDatasets = std::vector<std::shared_ptr<Dataset>>();
}

void DatasetManager::NewDataset(std::shared_ptr<Dataset> newDataset, int datasetID)
{
    switch(datasetID) {
        case DatasetManager::DATASET_ID_FIRST:
            firstDataset = newDataset;
            firstDataset->SetVisualisationColor(QColorConstants::Blue);
            simulationDatasets.push_back(newDataset);
            allDatasets.push_back(newDataset);
            break;
        case DatasetManager::DATASET_ID_SECOND:
            secondDataset = newDataset;
            secondDataset->SetVisualisationColor(QColorConstants::Magenta);
            simulationDatasets.push_back(newDataset);
            allDatasets.push_back(newDataset);
            break;
        case DatasetManager::DATASET_ID_CONTEXT:
            contextDatasets.push_back(newDataset);
            allDatasets.push_back(newDataset);
            break;
        case DatasetManager::DATASET_ID_RESULTS:
            simulationDatasets.push_back(newDataset);
            allDatasets.push_back(newDataset);
            break;
        case DatasetManager::DATASET_ID_UNKNOWN:
            std::cout << "WARNING: Could not add the Dataset because the ID is UNKNOWN." << std::endl;
            break;
    }
}
