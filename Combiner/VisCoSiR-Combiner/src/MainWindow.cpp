#include "./ui_mainwindow.h"

#include <MainWindow.h>
#include <QtCore>
#include <ViscosirPage.h>
#include <ui_mainwindow.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Prepare Header
    ui->header->setStyleSheet("background-color: #757579; margin-left: 0; margin-bottom: 0; border-bottom: 1px solid #333333;");
    ui->lbTitle->setStyleSheet("border-bottom: 0px none #990000;");
    ui->header->setContentsMargins(0,0,0,0);
    ui->centralwidget->layout()->setContentsMargins(0,0,0,0);

    // Create Data Set Manager and pass reference to relevant pages
    mDatasetManager = std::make_shared<DatasetManager>();
    ui->page_2_DataSets->setDatasetManager(mDatasetManager);
    ui->page_3_Transform->setDatasetManager(mDatasetManager);
    ui->page_4_Combination->setDatasetManager(mDatasetManager);
    ui->page_5_Timesteps->setDatasetManager(mDatasetManager);
    ui->page_6_Summary->setDatasetManager(mDatasetManager);

    // Bind navigation buttons
    QObject::connect(ui->btPrevious, &QPushButton::clicked, this, &MainWindow::goToPreviousStep);
    QObject::connect(ui->btNext, &QPushButton::clicked, this, &MainWindow::goToNextStep);
    ui->btPrevious->setCursor(QCursor(Qt::PointingHandCursor));
    ui->btNext->setCursor(QCursor(Qt::PointingHandCursor));

    // Go to first step & Update the header
    displayPage(0);

    // Create ConfigManager and pass reference to relevant pages
    configManager = std::make_shared<ConfigurationManager>();
    ui->page_1_LoadConfig->setConfigurationManager(configManager);
    ui->page_2_DataSets->setConfigurationManager(configManager);
    ui->page_3_Transform->setConfigurationManager(configManager);
    ui->page_4_Combination->setConfigurationManager(configManager);
    ui->page_5_Timesteps->setConfigurationManager(configManager);
    ui->page_6_Summary->setConfigurationManager(configManager);

    // Set the callback for when the config is loaded
    ui->page_1_LoadConfig->setCallbackListener(this);
}

void MainWindow::goToPreviousStep()
{
    int proposedNewIndex = ui->stackedWidget->currentIndex() - 1;
    if(proposedNewIndex >= 0)
    {
        displayPage(proposedNewIndex, ui->stackedWidget->currentIndex());
    }
}

void MainWindow::goToNextStep()
{
    int proposedNewIndex = ui->stackedWidget->currentIndex() + 1;
    if(proposedNewIndex < ui->stackedWidget->count())
    {
        displayPage(proposedNewIndex, ui->stackedWidget->currentIndex());
    }
}

void MainWindow::displayPage(int newIndex, int oldIndex)
{
    // Notify current page
    if(oldIndex != -1)
    {
        ViscosirPage* currentPage = getCurrentViscosirPage();
        if(currentPage)
        {
            currentPage->onLeaving();
        }
        else
        {
            std::cout << "Warning: Could not notify page on leaving because pointer was null! "<< std::endl;
        }
    }

    // Set the new page
    ui->stackedWidget->setCurrentIndex(newIndex);

    // Notify new page
    ViscosirPage* newPage = getCurrentViscosirPage();
    if(newPage)
    {
        newPage->onBecomingVisible();
    }
    else
    {
        std::cout << "Warning: Could not notify page on becoming visible because pointer was null! "<< std::endl;
    }

    updateNavigationHeader();
}

ViscosirPage* MainWindow::getCurrentViscosirPage()
{
    QWidget* currentWidget = ui->stackedWidget->currentWidget();
    if (dynamic_cast<ViscosirPage*>(currentWidget))
    {
        return dynamic_cast<ViscosirPage*>(currentWidget);
    }
    else
    {
        return NULL;
    }
}

void MainWindow::updateNavigationHeader()
{
    // Update the title
    std::string newTitle = std::to_string( ui->stackedWidget->currentIndex()+1 ) + "/" + std::to_string( ui->stackedWidget->count() ) + "  " + ui->stackedWidget->currentWidget()->whatsThis().toStdString();
    ui->lbTitle->setText(QString::fromStdString(newTitle));

    // Update buttons
    bool isPreviousEnabled = ui->stackedWidget->currentIndex() - 1 >= 0;
    bool isNextEnabled = ui->stackedWidget->currentIndex() + 1 < ui->stackedWidget->count();
    ui->btPrevious->setEnabled(isPreviousEnabled);
    ui->btNext->setEnabled(isNextEnabled);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onConfigLoaded()
{
    ui->page_2_DataSets->applyConfig();
}

