#include "ViscosirPage.h"


ViscosirPage::ViscosirPage(QWidget *parent) :
    QWidget(parent)
{
}

ViscosirPage::~ViscosirPage()
{
}

void ViscosirPage::setConfigurationManager(std::shared_ptr<ConfigurationManager> newConfigManager)
{
    mConfigManager = newConfigManager;
}
