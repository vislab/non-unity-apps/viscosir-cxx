#include "Dataset.h"

#include <vtkNew.h>

Dataset::Dataset()
{
    mVisProps = std::make_shared<VisualisationProperties>();
    mColors = vtkNamedColors::New();
}

void Dataset::SetVisualisationColor(QColor qColor)
{
    mVisualisationColor = qColor;
}

std::string Dataset::GetColorAsHexString()
{
    mColors->RGBToHTMLColor(mColors->GetColor3ub("Blue"));
    return mVisualisationColor.name().toStdString();
}

QColor Dataset::GetColorAsQColor()
{
    return mVisualisationColor;
}

vtkColor3d Dataset::GetColorAsVtkColor()
{
    return vtkColor3d(mVisualisationColor.red()/255.0f, mVisualisationColor.green()/255.0f, mVisualisationColor.blue()/255.0f);
}

vtkSmartPointer<vtkTransform> Dataset::GetCombinedTransform()
{
    vtkSmartPointer<vtkTransform> combinedTranform = vtkSmartPointer<vtkTransform>::New();
    int amountOfTransforms = mTransformationList.size();
    for(int i = 0; i < amountOfTransforms; i++){
       if(!mTransformationList[i]->IsDisabled()){
           //cout << mTransformationList[i]->GetType().toStdString() << endl;
           combinedTranform->Concatenate(mTransformationList[i]->mTransform);
           }
    }
    //combinedTranform->Print(std::cout);
    return combinedTranform;
}
