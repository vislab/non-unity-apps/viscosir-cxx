#include <PageCombination.h>
#include <ui_PageCombination.h>
#include <iostream>
#include <ParameterCombination.h>
#include <DatasetManager.h>

PageCombination::PageCombination(QWidget *parent) :
    ViscosirPage(parent),
    ui(new Ui::PageCombination)
{
    ui->setupUi(this);

    //Bind UI
    QObject::connect(ui->btnAddArrayCombination, &QPushButton::clicked, this, &PageCombination::AddParameterCombination);
    connect(ui->cbReference, &QComboBox::currentTextChanged, this, &PageCombination::OnReferenceSelectionChange);

    ui->btnAddArrayCombination->setCursor(QCursor(Qt::PointingHandCursor));
    ui->chAbsDeviationMaximum->setCursor(QCursor(Qt::PointingHandCursor));
    ui->chDeviationMaximum->setCursor(QCursor(Qt::PointingHandCursor));
    ui->chL2Norm->setCursor(QCursor(Qt::PointingHandCursor));
}

void PageCombination::onBecomingVisible()
{
    std::cout << "Entering Combination" << std::endl;

    // Fill or update ComboBox
    auto previousSelection = 0;
    if(ui->cbReference->count() != 0)
        previousSelection = ui->cbReference->currentIndex();
    ui->cbReference->clear();
    ui->cbReference->addItem("Average", "black");
    ui->cbReference->addItem(QString::fromStdString(mDatasetManager->firstDataset->mDescriptiveName), QString::fromStdString(mDatasetManager->firstDataset->GetColorAsHexString()));
    ui->cbReference->addItem(QString::fromStdString(mDatasetManager->secondDataset->mDescriptiveName), QString::fromStdString(mDatasetManager->secondDataset->GetColorAsHexString()));
    ui->cbReference->setCurrentIndex(previousSelection);
}



void PageCombination::onLeaving()
{
    std::cout << "Leaving Combination" << std::endl;

    // Update the list of arrays to be calculated in the ConfigurationManager
    mConfigManager->arraysToBeCalculated = {};
    for (ParameterCombination* parameterConfig : ui->scrollAreaWidgetContents->findChildren<ParameterCombination*>()) {
        mConfigManager->arraysToBeCalculated.push_back(parameterConfig->getData());
    }

    // Update the metrics to be calculated in the ConfigurationManager
    mConfigManager->calculateDeviationMax = ui->chDeviationMaximum->isChecked();
    mConfigManager->calculateDeviationMaxAbsolute = ui->chAbsDeviationMaximum ->isChecked();
    mConfigManager->calculateL2Norm = ui->chL2Norm->isChecked();

    // Update the reference in the ConfigurationManager
    mConfigManager->referenceForCalculation = ui->cbReference->currentIndex();
}

void PageCombination::AddParameterCombination()
{
    ParameterCombination* newParameterCombi = new ParameterCombination();
    newParameterCombi->SetUserInputHandler(this);
    newParameterCombi->SetDatasets(mDatasetManager->firstDataset, mDatasetManager->secondDataset);
    ui->vlParameterCombisHolder->insertWidget(((ui->vlParameterCombisHolder->count())-2), newParameterCombi);
}


void PageCombination::setDatasetManager(std::shared_ptr<DatasetManager> newDatasetManager)
{
    mDatasetManager = newDatasetManager;
}

PageCombination::~PageCombination()
{
    delete ui;
}

void PageCombination::OnReferenceSelectionChange(const QString& newSelection)
{
    // Update color of ComboBox
    QString colorString = ui->cbReference->itemData(ui->cbReference->currentIndex()).toString();
    ui->cbReference->setStyleSheet(QString::fromStdString("border: 1px solid ") +colorString);
}

void PageCombination::onDeleteButtonClick(QWidget *parameterCombination)
{
    delete parameterCombination;
}



