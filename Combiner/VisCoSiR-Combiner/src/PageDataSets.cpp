#include <PageDataSets.h>
#include <ui_PageDataSets.h>
#include <iostream>

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkDataSetMapper.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <QScreen>
#include <Dataset.h>
#include <DatasetManager.h>
#include <VisualisationOfOneDataset.h>

PageDataSets::PageDataSets(QWidget *parent) :
    ViscosirPage(parent),
    ui(new Ui::PageDataSets)
{
    ui->setupUi(this);

    // Set Callbacks
    ui->dsOne->SetCallbackReference(this);
    ui->dsOne->SetHeadline("Dataset #1");
    ui->dsOne->SetDatasetID(DatasetManager::DATASET_ID_FIRST);
    ui->dsOne->SetNameAndFiles("FE TS 174", "MT_FE_E_HEAT_TRANSPORT", "C:\\Users\\graeblin\\equivalent\\ufz\\VisCoSiR\\data\\");
    ui->dsTwo->SetCallbackReference(this);
    ui->dsTwo->SetHeadline("Dataset #2");
    ui->dsTwo->SetDatasetID(DatasetManager::DATASET_ID_SECOND);
    ui->dsTwo->SetNameAndFiles("CDA TS 420", "cd-a_results_MT_CDA", "C:\\Users\\graeblin\\equivalent\\ufz\\VisCoSiR\\data\\");
}

void PageDataSets::onBecomingVisible()
{
    std::cout << "Entering Data Sets" << std::endl;
}

void PageDataSets::onLeaving()
{
    std::cout << "Leaving Data Sets" << std::endl;

    if(ui->dsOne->isDescriptiveNameEmpty() || ui->dsTwo->isDescriptiveNameEmpty())
    {
        cout << "Error: Descriptive names for data sets must not be empty!" << endl;
    }
    else
    {
        mConfigManager->firstDataset->mDescriptiveName = ui->dsOne->GetDescriptiveName();
        mConfigManager->secondDataset->mDescriptiveName = ui->dsTwo->GetDescriptiveName();
    }
}

void PageDataSets::OnDatasetLoaded(std::shared_ptr<Dataset> dataset, int datasetID)
{
    // Add dataset to the DatasetManager
    mDatasetManager->NewDataset(dataset, datasetID);

    if(datasetID==DatasetManager::DATASET_ID_FIRST){
        // Set colors
        dataset->mVisProps->UpdateColor(VisualisationProperties::all, dataset->GetColorAsVtkColor());
        ui->dsOne->SetHighlightColor(dataset->GetColorAsHexString());
        mConfigManager->firstDataset = dataset;
    }
    else if(datasetID==DatasetManager::DATASET_ID_SECOND)
    {
        // Set colors
        dataset->mVisProps->UpdateColor(VisualisationProperties::all, dataset->GetColorAsVtkColor());
        ui->dsTwo->SetHighlightColor(dataset->GetColorAsHexString());
        mConfigManager->secondDataset = dataset;
    }
    else {
        std::cout << "Error: Tried to load a data set which is non of the two basic data sets." << std::endl;
    }
}

PageDataSets::~PageDataSets()
{
    delete ui;
}

void PageDataSets::setDatasetManager(std::shared_ptr<DatasetManager> newDatasetManager)
{
    mDatasetManager = newDatasetManager;
}

void PageDataSets::applyConfig()
{
    ui->dsOne->SetNameAndFiles(mConfigManager->firstDataset->mDescriptiveName,
                               mConfigManager->firstDataset->mTimestepPrefix,
                               mConfigManager->firstDataset->mTimestepFolder);
    ui->dsTwo->SetNameAndFiles(mConfigManager->secondDataset->mDescriptiveName,
                               mConfigManager->secondDataset->mTimestepPrefix,
                               mConfigManager->secondDataset->mTimestepFolder);
}


