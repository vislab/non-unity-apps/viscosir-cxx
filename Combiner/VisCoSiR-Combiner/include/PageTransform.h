#ifndef PAGETRANSFORM_H
#define PAGETRANSFORM_H

#include <QWidget>
#include <ViscosirPage.h>
#include <DatasetManager.h>
#include <QVTKInteractor.h>
#include <QWidget>
#include <ViscosirPage.h>
#include <VisualisationOfOneDataset.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkNamedColors.h>
#include <vtkRenderer.h>
#include <vtkOrientationMarkerWidget.h>

namespace Ui {
class PageTransform;
}

class PageTransform : public ViscosirPage
{
    Q_OBJECT

public:
    explicit PageTransform(QWidget *parent = nullptr);
    ~PageTransform();

    void onBecomingVisible();
    void onLeaving();
    void DisplayDatasets();
    void setDatasetManager(std::shared_ptr<DatasetManager> newDatasetManager);
    void DisplayTransformedDataset();
private:
    Ui::PageTransform *ui;
    std::shared_ptr<DatasetManager> mDatasetManager;
    VisualisationOfOneDataset *mVisualisationOfFirstDataset;
    VisualisationOfOneDataset *mVisualisationOfSecondDataset;

    vtkSmartPointer<vtkGenericOpenGLRenderWindow> mRenderWindow;
    vtkSmartPointer<vtkRenderer> mRenderer;
    vtkSmartPointer<QVTKInteractor> mInteractor;
    vtkSmartPointer<vtkInteractorStyleTrackballCamera> mInteractorStyle;
    vtkSmartPointer<vtkNamedColors> mColors;
    vtkSmartPointer<vtkAxesActor> MakeAxesActor();
    vtkSmartPointer<vtkAxesActor> mActorAxes;
    vtkSmartPointer<vtkOrientationMarkerWidget> mOm2;
    void OnSpatialStructureSelectionChange();
};

#endif // PAGETRANSFORM_H
