#ifndef VISUALISATIONOFONEDATASET_H
#define VISUALISATIONOFONEDATASET_H

#include <Dataset.h>
#include <vtkOutlineFilter.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>



class VisualisationOfOneDataset
{
public:
    VisualisationOfOneDataset();
    void SetRenderingUtils(vtkSmartPointer<vtkRenderer> vtkRenderer, vtkSmartPointer<vtkRenderWindow> vtkRenderWindow);
    void SetDataset(std::shared_ptr<Dataset> dataset);
    void SetBoundingBoxVisibility(bool isVisible, bool resetCamera = false);
    void SetMeshVisibility(bool isVisible, bool resetCamera);

    void UpdateColor(VisualisationProperties::colorProp colorProp, vtkColor3d colorValue);

    void OnTransformationsChange();

private:
    vtkSmartPointer<vtkRenderer> mRenderer;
    vtkSmartPointer<vtkRenderWindow> mRenderWindow;

    std::shared_ptr<Dataset> currentDataset;

};

#endif // VISUALISATIONOFONEDATASET_H
