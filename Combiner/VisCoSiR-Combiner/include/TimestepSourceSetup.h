#ifndef TIMESTEPSOURCESETUP_H
#define TIMESTEPSOURCESETUP_H

#include <Dataset.h>
#include <PageTimesteps.h>
#include <QWidget>

#include <External/exprtk.hpp>

namespace Ui {
class TimestepSourceSetup;
}

class TimestepSourceSetup : public QWidget
{
    Q_OBJECT

    static const QString SOURCE_NONE;
    static const QString SOURCE_CSV;
    static const QString SOURCE_VTU;
    static const QString SOURCE_PVD;

public:
    explicit TimestepSourceSetup(QWidget *parent = nullptr);
    ~TimestepSourceSetup();
    void SetDataset(std::shared_ptr<Dataset> dataset, int datasetID);
    void SetCallbackReference(PageTimesteps *timestepsPage);

private:
    Ui::TimestepSourceSetup *ui;
    void InitPreviewTable(std::vector<std::string> timestepFilenames);
    void UpdatePreviewTable();
    void OnSourceSelectionChange(const QString& newSelection);
    void CropFilenames();
    void LoadCsv();
    void LoadPvd();
    PageTimesteps *mTimestepsPage;
    int mDatasetID;
    std::shared_ptr<Dataset> mDataset;
    void UpdateStatusMessage(std::string text, std::string colorString, bool isTextcolor = true);
    void OnCalculationSelectionChange();

    std::vector<std::string> mRawTimevaluesFromSource;
    QString lastCustomCalculationString = "time";

    // For the calculations
    exprtk::symbol_table<float> mSymbolTable;
    exprtk::expression<float> mExpression;
    exprtk::parser<float> mParser;
    float mCurrentValueForCalculation;
    void UpdateCalculatedValues();
    void OnCalculationStringUserEdit();
};

#endif // TIMESTEPSOURCESETUP_H
