#ifndef PAGETIMESTEPS_H
#define PAGETIMESTEPS_H

#include <QtCharts>
#include <QWidget>
#include <ViscosirPage.h>
#include <DatasetManager.h>

namespace Ui {
class PageTimesteps;
}

class PageTimesteps : public ViscosirPage
{
    Q_OBJECT

public:
    explicit PageTimesteps(QWidget *parent = nullptr);
    ~PageTimesteps();
    void setDatasetManager(std::shared_ptr<DatasetManager> newDatasetManager);
    void DisplayTimevaluesInChart(int datasetID);
    void onBecomingVisible();
    void onLeaving();
    void OnSelectionChange(const QString& newSelection);

    void OnTemporalReferenceSelectionChange();
private:
    Ui::PageTimesteps *ui;
    std::shared_ptr<DatasetManager> mDatasetManager;
    QChartView *mChartView;
    QChart *mChart;
    QLineSeries* mLineseries;
    QScatterSeries *seriesOne;
    QScatterSeries *seriesTwo;
    QScatterSeries *seriesCombined;

    void deleteReloadedSeriesInChart(QString series_name);
    void updateSelectionVisibility();
    void AddTimestepsToChart(QScatterSeries* series, float heightLevel, std::vector<float> timesteps, QColor color, QString series_name);
    void UpdateReferenceLines(std::vector<float> timesteps, QColor color);

    float mCurrentMin = NULL;
    float mCurrentMax = NULL;
    bool mSuccessfullyLoadedFirst = false;
    bool mSuccessfullyLoadedSecond = false;
    void RecalculateExtremas();
};

#endif // PAGETIMESTEPS_H
