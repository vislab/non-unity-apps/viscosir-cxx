#ifndef SUMMARYITEM_H
#define SUMMARYITEM_H

#include <QWidget>

namespace Ui {
class SummaryItem;
}

class SummaryItem : public QWidget
{
    Q_OBJECT

public:
    explicit SummaryItem(QWidget *parent = nullptr);
    ~SummaryItem();
    void SetContent(std::string headline, std::string content);
    void SetContent(std::string headline, std::string contentLeft, std::string contentRight);
    void SetContent(std::string headline, std::string contentLeft, std::string contentRight, bool highlightLeft, bool highlightRight);

    static SummaryItem* CreateSingleContentItem(std::string headline, std::string content);
    static SummaryItem* CreateContentItem(std::string headline, std::string contentLeft, std::string contentRight, bool isHighlighted);
    static SummaryItem* CreateCheckItem(std::string headline, bool isLeftSelected, bool isRightSelected);
    static SummaryItem* CreateSelectionItem(std::string headline, bool isLeftSelected, bool isRightSelected);

private:
    Ui::SummaryItem *ui;
};

#endif // SUMMARYITEM_H
