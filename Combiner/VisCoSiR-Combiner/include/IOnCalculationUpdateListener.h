#ifndef IONCALCULATIONUPDATELISTENER_H
#define IONCALCULATIONUPDATELISTENER_H


class IOnCalculationUpdateListener
{
public:
    virtual void UpdateStatusOfCalculation(int countDone, int countAll)=0;
};


#endif // IONCALCULATIONUPDATELISTENER_H
