#ifndef DATASETSETUP_H
#define DATASETSETUP_H

#include <DatasetManager.h>
#include <MainWindow.h>
#include <PageDataSets.h>
#include <QWidget>
#include <string>
#include <vtkColor.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>

namespace Ui {
class DatasetSetup;
}

class DatasetSetup : public QWidget
{
    Q_OBJECT

public:
    explicit DatasetSetup(QWidget *parent = nullptr);
    ~DatasetSetup();
    void SetCallbackReference(PageDataSets *dataSetsPage);
    void SetHeadline(std::string headline);
    void SetDatasetID(int datasetID);
    void SetNameAndFiles(std::string name, std::string filePrefix, std::string folder);
    void SetHighlightColor(vtkStdString color);
    std::string GetDescriptiveName();
    bool isDescriptiveNameEmpty();
    bool IsPathValid(std::string path);
    std::string ModifyPath(std::string path);
    vtkSmartPointer<vtkUnstructuredGrid> mGrid;

private:
    Ui::DatasetSetup *ui;
    PageDataSets *mDataSetsPage;
    int mDatasetID = DatasetManager::DATASET_ID_UNKNOWN;
    void onLoadDatasetClick();
    const static bool compareNat(const std::string& a, const std::string& b);
};

#endif // DATASETSETUP_H
