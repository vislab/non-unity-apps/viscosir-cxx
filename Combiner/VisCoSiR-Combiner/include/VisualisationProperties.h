#ifndef VISUALISATIONPROPERTIES_H
#define VISUALISATIONPROPERTIES_H

#include <vtkActor.h>
#include <vtkColor.h>
#include <vtkOutlineFilter.h>
#include <vtkNew.h>
#include <vtkTransformFilter.h>
#include <vtkAxesActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkDataSetMapper.h>


class VisualisationProperties
{
public:
    VisualisationProperties();
    enum colorProp{all=0, boundingBox=1};
    void UpdateColor(colorProp colorProp, vtkColor3d colorValue);

    // Visibilities
    bool mShowBoundingBox = false;
    bool mShowMesh = false;

    // Actors
    vtkSmartPointer<vtkActor> mActorBoundingBox;
    vtkSmartPointer<vtkActor> mActorWireframe;
    vtkSmartPointer<vtkOutlineFilter> mFilterBoundingBox;

    vtkSmartPointer<vtkTransformFilter> mFilterTransform;
    void SetTransformInFilter(vtkSmartPointer<vtkTransform> currTransformation);
};

#endif // VISUALISATIONPROPERTIES_H
