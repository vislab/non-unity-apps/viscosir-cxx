#ifndef FileUtilsForVTK_H
#define FileUtilsForVTK_H

#include <QString>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>

#include <chrono>


class FileUtilsForVTK
{


public:
    explicit FileUtilsForVTK();
    ~FileUtilsForVTK();
    static vtkSmartPointer<vtkUnstructuredGrid> ReadUnstructuredGrid(QString const& qFileName);

};

#endif
