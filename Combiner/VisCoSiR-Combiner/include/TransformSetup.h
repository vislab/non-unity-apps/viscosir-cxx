#ifndef TRANSFORMSETUP_H
#define TRANSFORMSETUP_H

#include "IUserInputHandler.h"

#include <DatasetManager.h>
#include <MainWindow.h>
#include <PageTransform.h>
#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QHash>
#include <vtkColor.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkNew.h>

namespace Ui {
class TransformSetup;
}

class TransformSetup : public QWidget, public IUserInputHandler
{
    Q_OBJECT

public:
    explicit TransformSetup(QWidget *parent = nullptr);
    ~TransformSetup();
    void SetCallbackReference(PageTransform *dataSetsPage);
    vtkSmartPointer<vtkUnstructuredGrid> mGrid;
    void SetDataset(std::shared_ptr<Dataset> dataset);
    void AddTransforms();
    void updateTransformListForDataSets();
    void updateTransformVisualisation();

    // Methods from interface
    virtual void onUserInputInTransformation();
    virtual void onDeleteButtonClickInTransformation(QPushButton* btnRemove);

private:
    Ui::TransformSetup *ui;
    PageTransform *mTransformsPage;
    int mDatasetID = DatasetManager::DATASET_ID_UNKNOWN;
    std::shared_ptr<Dataset> mDataset;
};

#endif // TRANSFORMSETUP_H
