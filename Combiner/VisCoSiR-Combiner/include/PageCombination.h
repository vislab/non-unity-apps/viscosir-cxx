#ifndef PAGECOMBINATION_H
#define PAGECOMBINATION_H

#include "IParameterCombinationUserInputHandler.h"

#include <DatasetManager.h>
#include <QPushButton>
#include <QWidget>
#include <ViscosirPage.h>

namespace Ui {
class PageCombination;
}

class PageCombination : public ViscosirPage, public IParameterCombinationUserInputHandler
{
    Q_OBJECT

public:
    explicit PageCombination(QWidget *parent = nullptr);
    ~PageCombination();

    void onBecomingVisible();
    void onLeaving();
    void AddParameterCombination();
    void setDatasetManager(std::shared_ptr<DatasetManager> newDatasetManager);
    void OnReferenceSelectionChange(const QString& newSelection);

    // IParameterCombinationUserInputHandler interface
    void onDeleteButtonClick(QWidget *parameterCombination);


private:
    Ui::PageCombination *ui;
    void setFirstMinusSecond();
    void setSecondMinusFirst();
    std::shared_ptr<DatasetManager> mDatasetManager;

};

#endif // PAGECOMBINATION_H
