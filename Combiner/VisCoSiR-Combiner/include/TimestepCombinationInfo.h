#ifndef TIMESTEPCOMBINATIONINFO_H
#define TIMESTEPCOMBINATIONINFO_H

#include <TemporalInterpolationInfo.h>
#include <memory>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>


class TimestepCombinationInfo
{
public:
    // das Grid dessen Struktur verwendet wird
    vtkSmartPointer<vtkUnstructuredGrid> referenceGrid;
    TemporalInterpolationInfo temporalInterpolationInfoMinuend;
    TemporalInterpolationInfo temporalInterpolationInfoSubtrahend;
};

#endif // TIMESTEPCOMBINATIONINFO_H
