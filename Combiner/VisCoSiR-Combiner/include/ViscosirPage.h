#ifndef VISCOSIRPAGE_H
#define VISCOSIRPAGE_H

#include <ConfigurationManager.h>
#include <QWidget>
#include <iostream>

namespace Ui {
class ViscosirPage;
}

class ViscosirPage : public QWidget
{
    Q_OBJECT

public:
    explicit ViscosirPage(QWidget *parent = nullptr);
    ~ViscosirPage();
    void setConfigurationManager(std::shared_ptr<ConfigurationManager> newConfigManager);
    virtual void onBecomingVisible()
    {
        std::cout << "On becoming visible // please override in the derived classes!" << std::endl;
    }
    virtual void onLeaving()
    {
        std::cout << "On leaving // please override in the derived classes!" << std::endl;
    }
    virtual void applyConfig()
    {
        std::cout << "WARNING: Please override 'applyConfig' in the derived class!" << std::endl;
    }


protected:
    std::shared_ptr<ConfigurationManager> mConfigManager;

private:
    Ui::ViscosirPage *ui;

};

#endif // VISCOSIRPAGE_H
