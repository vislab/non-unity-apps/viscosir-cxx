#ifndef CONFIGURATIONMANAGER_H
#define CONFIGURATIONMANAGER_H

#include <Dataset.h>
#include <string>
#include <memory>

#include <Model/ParameterCombinationModel.h>


class ConfigurationManager
{
public:
    ConfigurationManager();
    std::string loadConfigFromFile(std::string filePathAndName);
    std::string writeConfigToFile(std::string filePathAndName);
    bool areBothDatasetsSet();
    std::vector<std::string> getNamesOfRelevantArraysForDataset(std::string datasetName);

    std::shared_ptr<Dataset> firstDataset;
    std::shared_ptr<Dataset> secondDataset;

    bool calculateDeviationMax = true;
    bool calculateDeviationMaxAbsolute = true;
    bool calculateL2Norm = true;

    std::vector<ParameterCombinationModel> arraysToBeCalculated;

    /**
     * @brief flag storing the spatial structure reference
     */
    int structMode;
    static constexpr int STRUCT_MODE_UNKNOWN = -1;
    static constexpr int STRUCT_MODE_USE_FIRST = 0;
    static constexpr int STRUCT_MODE_USE_SECOND = 1;

    /**
     * @brief flag storing information about the temporal reference dataset
     */
    int timestepSource;
    static constexpr int TIMESTEP_SOURCE_UNKNOWN = -1;
    static constexpr int TIMESTEP_SOURCE_FIRST = 0;
    static constexpr int TIMESTEP_SOURCE_SECOND = 1;

    /**
     * @brief flag storing information about the reference dataset for calculating the array values
     */
    int referenceForCalculation;
    static constexpr int CALC_REFERENCE_UNKNOWN = -1;
    static constexpr int CALC_REFERENCE_AVG = 0;
    static constexpr int CALC_REFERENCE_FIRST = 1;
    static constexpr int CALC_REFERENCE_SECOND = 2;

private:
    void DatasetToJson(std::shared_ptr<Dataset> dataset, QJsonObject &json);
    void JsonToDataset(std::shared_ptr<Dataset> dataset, QJsonObject &json);
};

#endif // CONFIGURATIONMANAGER_H
