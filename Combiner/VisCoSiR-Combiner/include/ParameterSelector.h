#ifndef PARAMETERSELECTOR_H
#define PARAMETERSELECTOR_H

#include <Dataset.h>
#include <QWidget>

namespace Ui {
class ParameterSelector;
}

class ParameterSelector : public QWidget
{
    Q_OBJECT

public:
    explicit ParameterSelector(QWidget *parent = nullptr);
    ~ParameterSelector();
    void SetDataset(std::shared_ptr<Dataset> dataset);
    std::string getSelectedArrayName();
    std::string getDatasetName();

private:
    Ui::ParameterSelector *ui;
    std::shared_ptr<Dataset> mDataset;
};

#endif // PARAMETERSELECTOR_H
