#ifndef TEMPORALINTERPOLATIONINFO_H
#define TEMPORALINTERPOLATIONINFO_H

#include <iostream>
#include <string>


class TemporalInterpolationInfo
{
    public:
        float timeValue;
        std::string fileTimestep1;
        std::string fileTimestep2;
        // Ratio of the two timesteps: 1 for no consideration of TS2, i.e. pure TS1
        float ratioFirst;

        void PrintInfo()
        {
            std::cout << "\n\n**********************\n\nTemporalInterpolationInfo:" << "\nFirst: " << fileTimestep1 << "\nSecond: " << fileTimestep2  << "\nRatioFirst: " << ratioFirst << "\n**********************\n\n";
        }

};
#endif // TEMPORALINTERPOLATIONINFO_H
