#ifndef DATASETMANAGER_H
#define DATASETMANAGER_H


#include <Dataset.h>
class DatasetManager
{
public:
    DatasetManager();
    std::shared_ptr<Dataset> firstDataset;
    std::shared_ptr<Dataset> secondDataset;
    std::vector<std::shared_ptr<Dataset>> simulationDatasets;
    std::vector<std::shared_ptr<Dataset>> contextDatasets;
    std::vector<std::shared_ptr<Dataset>> allDatasets;

    void NewDataset(std::shared_ptr<Dataset> grid, int datasetID);

    static constexpr int DATASET_ID_FIRST = 0;
    static constexpr int DATASET_ID_SECOND = 1;
    static constexpr int DATASET_ID_RESULTS = 2;
    static constexpr int DATASET_ID_CONTEXT = 3;
    static constexpr int DATASET_ID_UNKNOWN = 4;
};

#endif // DATASETMANAGER_H
