#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "IOnConfigLoadedListener.h"

#include <ConfigurationManager.h>
#include <DatasetManager.h>
#include <QMainWindow>
#include <ViscosirPage.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow, public IOnConfigLoadedListener
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    virtual void onConfigLoaded();

private:
    Ui::MainWindow *ui;
    std::shared_ptr<DatasetManager> mDatasetManager;
    void goToPreviousStep();
    void goToNextStep();
    void updateNavigationHeader();
    std::shared_ptr<ConfigurationManager> configManager;
    void displayPage(int newIndex, int oldIndex = -1);
    ViscosirPage* getCurrentViscosirPage();
};
#endif // MAINWINDOW_H
