#ifndef PAGEDATASETS_H
#define PAGEDATASETS_H

#include <Dataset.h>
#include <DatasetManager.h>
#include <QVTKInteractor.h>
#include <QWidget>
#include <ViscosirPage.h>
#include <VisualisationOfOneDataset.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkNamedColors.h>
#include <vtkRenderer.h>

namespace Ui {
class PageDataSets;
}

class PageDataSets : public ViscosirPage
{
    Q_OBJECT

public:
    explicit PageDataSets(QWidget *parent = nullptr);
    ~PageDataSets();

    void setDatasetManager(std::shared_ptr<DatasetManager> newDatasetManager);
    void onBecomingVisible();
    void onLeaving();
    void applyConfig();
    void OnDatasetLoaded(std::shared_ptr<Dataset> dataset, int datasetID);


private:
    Ui::PageDataSets *ui;

    std::shared_ptr<DatasetManager> mDatasetManager;
    VisualisationOfOneDataset *mVisualisationOfFirstDataset;
    VisualisationOfOneDataset *mVisualisationOfSecondDataset;

    vtkSmartPointer<vtkGenericOpenGLRenderWindow> mRenderWindow;
    vtkSmartPointer<vtkRenderer> mRenderer;
    vtkSmartPointer<QVTKInteractor> mInteractor;
    vtkSmartPointer<vtkInteractorStyleTrackballCamera> mInteractorStyle;
    vtkSmartPointer<vtkNamedColors> mColors;

};

#endif // PAGEDATASETS_H
