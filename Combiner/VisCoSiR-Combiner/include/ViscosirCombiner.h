#ifndef VISCOSIRCOMBINER_H
#define VISCOSIRCOMBINER_H

#include <ConfigurationManager.h>
#include <DatasetManager.h>
#include <IOnCalculationUpdateListener.h>
#include <TimestepCombinationInfo.h>
#include <memory>
#include <vtkProgrammableFilter.h>



class ViscosirCombiner
{
public:
    ViscosirCombiner();

    // Timestep preparations
    static void generateCombinedTimestepInfosForAllDatasets(std::shared_ptr<DatasetManager> datasetManager, std::shared_ptr<ConfigurationManager> configManager);
    static void generateCombinedTimestepInfosForOneDataset(std::shared_ptr<Dataset> dataset, std::vector<float> referenceTimeValues);

    // Final combination
    static void calculateFullCombination(std::shared_ptr<DatasetManager> datasetManager, std::shared_ptr<ConfigurationManager> configManager, std::string outPathAndFilePrefix, IOnCalculationUpdateListener* updateListener);
    static void calculateOneCombination(std::shared_ptr<Dataset> calculationReferenceDataset, std::vector<std::shared_ptr<Dataset> > otherDatasets, std::shared_ptr<ConfigurationManager> configManager, vtkSmartPointer<vtkUnstructuredGrid> spatialReferenceGrid, int timestepIndex, std::string fileName);
    static void writeCombinerConfigLogFile(std::shared_ptr<Dataset> calculationReferenceDataset, std::vector<std::shared_ptr<Dataset> > otherDatasets, std::string outputFolder);
    static std::string constructAbsoluteFileNameForTimestep(int timestepIndex, std::string outputPath);
    static void InterpolateTwoTimesteps(void *arguments);
    static void CalculateMetrics(void *arguments);

    inline static const std::string ARRAY_NAME_SEPARATOR = "___";
    inline static const std::string ARRAY_NAME_DEVIATION_MAX = "DeviationMax";
    inline static const std::string ARRAY_NAME_DEVIATION_MAX_ABS = "DeviationMaxAbs";
    inline static const std::string ARRAY_NAME_DEVIATION_SOURCE = "DeviationSource";
    inline static const std::string ARRAY_NAME_L2NORM = "L2Norm";

    // Capsulated Parameters for the ProgrammableFilter for Temporal Interpolation of two timesteps
    struct ParamsTemporalInterpolation
    {
      double ratioFirst;
      vtkPointData* pointDataFirstTS;
      vtkPointData* pointDataSecondTS;
      std::vector<std::string> namesOfRelevantArrays;
      vtkProgrammableFilter* filter;
    };

    // Capsulated Tuple of Point-Data and Dataset-Name for better code readability
    struct OneDatasetForCalculation
    {
        std::string originalDatasetName;
        vtkPointData* pointData;
    };

    // Capsulated Parameters for the ProgrammableFilter for calculating the metrics
    struct ParamsCalculateMetrics
    {
        std::shared_ptr<ConfigurationManager> configManager;
        OneDatasetForCalculation dataOfTheReferenceDataset;
        std::vector<OneDatasetForCalculation> dataOfTheOtherDatasets;
        vtkProgrammableFilter* filter;
    };


};

#endif // VISCOSIRCOMBINER_H
