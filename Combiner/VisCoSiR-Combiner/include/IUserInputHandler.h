#ifndef IUSERINPUTHANDLER_H
#define IUSERINPUTHANDLER_H

#include <QPushButton>


class IUserInputHandler
{
public:
    virtual void onUserInputInTransformation()=0;
    virtual void onDeleteButtonClickInTransformation(QPushButton* btnRemove)=0;
};

#endif // IUSERINPUTHANDLER_H
