#ifndef PAGELOADCONFIG_H
#define PAGELOADCONFIG_H

#include <IOnConfigLoadedListener.h>
#include <QWidget>
#include <ViscosirPage.h>

namespace Ui {
class PageLoadConfig;
}

class PageLoadConfig : public ViscosirPage
{
    Q_OBJECT

public:
    explicit PageLoadConfig(QWidget *parent = nullptr);
    ~PageLoadConfig();

    void onBecomingVisible();
    void onLeaving();
    void setCallbackListener(IOnConfigLoadedListener *configLoadedlistener);


    void onLoadConfigClick();
    void onSaveConfigClick();
private:
    Ui::PageLoadConfig *ui;
    IOnConfigLoadedListener *mConfigLoadedlistener;
    void initPvd1Selection();
    void initPvd2Selection();
};

#endif // PAGELOADCONFIG_H
