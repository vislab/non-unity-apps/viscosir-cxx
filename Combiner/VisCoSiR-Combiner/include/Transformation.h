#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "IUserInputHandler.h"

#include <QPushButton>
#include <QWidget>
#include <vtkNew.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>

namespace Ui {
class Transformation;
}

class Transformation : public QWidget
{
    Q_OBJECT

public:
    explicit Transformation(QWidget *parent = nullptr);
    vtkSmartPointer<vtkTransform> mTransform;
    void RemoveTransform();
    ~Transformation();
    void SetTransformation(std::string type, double x, double y, double z);
    void onChangeByUser();
    void SetTransformationSetup(IUserInputHandler *userInputHandler);
    void DisableTransform();
    QString GetType();
    QPushButton* GetDeleteBtn();
    bool IsDisabled();
    void SetDefaults();
    void onTypeChanged();

private:
    Ui::Transformation *ui;
    IUserInputHandler *mUserInputHandler;
    bool IsScaleInitialized = false;
    bool IsTranslateInitialized = false;
    bool IsRotateInitialized = false;
};

#endif // TRANSFORMATION_H
