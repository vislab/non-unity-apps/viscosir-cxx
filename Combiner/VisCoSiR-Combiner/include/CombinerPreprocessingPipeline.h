#ifndef COMBINERPREPROCESSINGPIPELINE_H
#define COMBINERPREPROCESSINGPIPELINE_H

#include <Dataset.h>
#include <vtkAlgorithmOutput.h>
#include <vtkDataObject.h>
#include <vtkProgrammableFilter.h>
#include <ViscosirCombiner.h>
#include <vtkResampleWithDataSet.h>
#include <vtkXMLUnstructuredGridReader.h>



class CombinerPreprocessingPipeline
{

public:
    CombinerPreprocessingPipeline(std::shared_ptr<Dataset> inputDataset, std::vector<std::string> namesOfRelevantArrays, vtkDataSet* spatialReference, int currentTimestepIndex);
    vtkDataObject* GetOutput();
    vtkAlgorithmOutput* GetOutputPort();

private:
    vtkSmartPointer<vtkXMLUnstructuredGridReader> mReaderFirstTimestep;
    vtkSmartPointer<vtkXMLUnstructuredGridReader> mReaderSecondTimestep;
    vtkSmartPointer<vtkProgrammableFilter> mTimestepInterpolationFilter;
    vtkSmartPointer<vtkTransformFilter> mTransformFilter;
    vtkSmartPointer<vtkResampleWithDataSet> mResamplingFilter;
};

#endif // COMBINERPREPROCESSINGPIPELINE_H
