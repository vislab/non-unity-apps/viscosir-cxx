#ifndef PARAMETERCOMBINATIONMODEL_H
#define PARAMETERCOMBINATIONMODEL_H

#include <map>
#include <string>


class ParameterCombinationModel
{
    public:
        std::string resultName;
        std::map<std::string, std::string> mapDatasetNameToArrayName;
};

#endif // PARAMETERCOMBINATIONMODEL_H
