#ifndef IONCONFIGLOADEDLISTENER_H
#define IONCONFIGLOADEDLISTENER_H

class IOnConfigLoadedListener
{
public:
    virtual void onConfigLoaded()=0;
};


#endif // IONCONFIGLOADEDLISTENER_H
