#ifndef PARAMETERCOMBINATION_H
#define PARAMETERCOMBINATION_H

#include "IParameterCombinationUserInputHandler.h"

#include <Dataset.h>
#include <QWidget>

#include <Model/ParameterCombinationModel.h>

namespace Ui {
class ParameterCombination;
}

class ParameterCombination : public QWidget
{
    Q_OBJECT

public:
    explicit ParameterCombination(QWidget *parent = nullptr);
    ~ParameterCombination();
    void SetDatasets(std::shared_ptr<Dataset> ds1, std::shared_ptr<Dataset> ds2);
    void SetUserInputHandler(IParameterCombinationUserInputHandler *userInputHandler);
    ParameterCombinationModel getData();

private:
    Ui::ParameterCombination *ui;
    IParameterCombinationUserInputHandler *mUserInputHandler;
};

#endif // PARAMETERCOMBINATION_H
