#ifndef DATASET_H
#define DATASET_H

#include <QColor>
#include <QString>
#include <VisualisationProperties.h>
#include <list>
#include <vtkNamedColors.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <memory>
#include <Transformation.h>
#include <TemporalInterpolationInfo.h>


class Dataset
{
public:
    Dataset();
    std::string mDescriptiveName;
    std::string mTimestepFolder;
    std::string mTimestepPrefix;
    std::vector<std::string> mTimestepFiles;
    std::vector<float> mTimestepValues;
    vtkSmartPointer<vtkUnstructuredGrid> mGrid;
    std::shared_ptr<VisualisationProperties> mVisProps;
    QList<Transformation*> mTransformationList;
    std::vector<TemporalInterpolationInfo> mTemporalInterpolationInfos;

    void SetVisualisationColor(QColor qColor);
    std::string GetColorAsHexString();
    QColor GetColorAsQColor();
    vtkColor3d GetColorAsVtkColor();
    vtkSmartPointer<vtkTransform> GetCombinedTransform();

private:
    vtkSmartPointer<vtkNamedColors> mColors;
    QColor mVisualisationColor;

};

#endif // DATASET_H
