#ifndef IPARAMETERCOMBINATIONUSERINPUTHANDLER_H
#define IPARAMETERCOMBINATIONUSERINPUTHANDLER_H

#include <QWidget>


class IParameterCombinationUserInputHandler
{
public:
    virtual void onDeleteButtonClick(QWidget* parameterCombination)=0;
};


#endif // IPARAMETERCOMBINATIONUSERINPUTHANDLER_H
