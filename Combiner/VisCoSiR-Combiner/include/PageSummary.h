#ifndef PAGESUMMARY_H
#define PAGESUMMARY_H


#include <QWidget>
#include <ViscosirPage.h>
#include <ViscosirCombiner.h>

namespace Ui {
class PageSummary;
}

class PageSummary : public ViscosirPage, public IOnCalculationUpdateListener
{
    Q_OBJECT

public:
    explicit PageSummary(QWidget *parent = nullptr);
    ~PageSummary();

    void onBecomingVisible();
    void onLeaving();
    void setDatasetManager(std::shared_ptr<DatasetManager> newDatasetManager);

public slots:
    void OnComputationFinished();

private:
    void RunCalculation();
    Ui::PageSummary *ui;
    void AddNewItemToLayout(QWidget *newItem);
    std::shared_ptr<DatasetManager> mDatasetManager;
    void StartTheCalculation();
    QString outFolder;

    // IOnCalculationUpdateListener interface
public:
    void UpdateStatusOfCalculation(int countDone, int countAll);
};

#endif // PAGESUMMARY_H
