# OGS VisCoSiR - Visual Comparison of Simulation Results

OGS VisCoSiR is a tool, that supports domain experts in combining and analysing multiple spatial simulation results, i.e. from different simulation software or conducted with different parameter setups. In contrast to other tools, OGS VisCoSiR focuses on the visual comparison in 3D.

## Structure of the Project

The tool consists of two parts: A combiner tool to initially combine multiple datasets. In this step, users define the spatial and temporal setup as well as the dataset used as a reference during the analysis. This is only done once. The resulting datasets (vtk file format) can then be analysed in the analyser tool in 3D. Because the datasets are stored in the vtk file format, the analysis can also be done in other tools, like ParaView. 


## Building the Tool

The following software versions are required for building/developing:

* Qt 5.14.2
* VTK 8.2

We recommend the following tools for building/developing:

* Cmake 3.2.4
* Qt Creator Version 8.0.0


## Project Status

Current status of development: Prototype


## Funding

This work has been co-financed within the framework of EURAD, the European Joint Programme on Radioactive Waste Management (Grant Agreement No 847593), this support is gratefully acknowledged.
