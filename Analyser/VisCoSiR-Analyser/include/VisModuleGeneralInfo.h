#ifndef VISMODULEGENERALINFO_H
#define VISMODULEGENERALINFO_H

#include <QWidget>
#include <VisModule.h>

namespace Ui {
class VisModuleGeneralInfo;
}

class VisModuleGeneralInfo : public VisModule
{
    Q_OBJECT

public:
    explicit VisModuleGeneralInfo(QWidget *parent = nullptr);
    ~VisModuleGeneralInfo();

private:
    Ui::VisModuleGeneralInfo *ui;

    // VisModule interface
public:
    void updateOnDataChanged();
};

#endif // VISMODULEGENERALINFO_H
