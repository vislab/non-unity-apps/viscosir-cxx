#ifndef VISMODULE_H
#define VISMODULE_H

#include <Analyser3DView.h>
#include <DataManager.h>
#include <QWidget>
#include <iostream>


class VisModule : public QWidget
{
    Q_OBJECT

public:
    explicit VisModule(QWidget *parent = nullptr);
    ~VisModule();
    void setDataManager(std::shared_ptr<DataManager> dataManager);
    void setAnalyser3DView(std::shared_ptr<Analyser3DView> analyser3DView);
    virtual void updateOnDataChanged()
    {
        std::cout << "WARNING: Please override 'updateOnDataChanged' in the derived class!" << std::endl;
    }

protected:
    std::shared_ptr<DataManager> mDataManager;
    std::shared_ptr<Analyser3DView> mAnalyser3DView;

};

#endif // VISMODULE_H
