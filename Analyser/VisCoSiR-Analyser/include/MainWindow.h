#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <Analyser3DView.h>
#include <DataManager.h>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void initInputInfoPopup();

private:
    Ui::MainWindow *ui;
    QApplication* mApp;
    std::string mInputMessage;
    inline static std::string MESSAGE_INPUT_DEFAULT = "Please select the data that you want to explore. The folder that you select must contain the vtu files and the configuration file 'viscosir_combination_info.json' - both created by the VisCoSiR Combiner Tool.";
    inline static std::string MESSAGE_INPUT_ERROR = "Error: The selected folder does not contain a (valid) configuration file, like 'viscosir_combination_info.json'.\n\n";

    std::shared_ptr<DataManager> mDataManager;
    std::shared_ptr<Analyser3DView> mAnalyser3DView;

    void initInputFolderSelection();
    void updateAllVisModules();

};
#endif // MAINWINDOW_H
