#ifndef VISMODULETIMESTEPS_H
#define VISMODULETIMESTEPS_H

#include <IOnTimestepChangeByUser.h>
#include <QWidget>
#include <VisModule.h>

namespace Ui {
class VisModuleTimesteps;
}

class VisModuleTimesteps : public VisModule
{
    Q_OBJECT

public:
    explicit VisModuleTimesteps(QWidget *parent = nullptr);
    ~VisModuleTimesteps();
    void setTimestep(int timestepIndex);
    void onSliderValueChangeByUser();
    void setTimestepChangeHandler(IOnTimestepChangeByUser *handler);
    void setHighlights(std::vector<bool> highlights, QString information);
    void updateOnDataChanged();

private:
    Ui::VisModuleTimesteps *ui;

};

#endif // VISMODULETIMESTEPS_H
