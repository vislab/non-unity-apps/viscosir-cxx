#ifndef ANALYSER3DVIEW_H
#define ANALYSER3DVIEW_H

#include <DataManager.h>
#include <QVTKInteractor.h>
#include <QWidget>
#include <vtkAxesActor.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkNamedColors.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkOutlineFilter.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkXMLUnstructuredGridReader.h>

namespace Ui {
class Analyser3DView;
}

class Analyser3DView : public QWidget
{
    Q_OBJECT

public:
    explicit Analyser3DView(QWidget *parent = nullptr);
    ~Analyser3DView();
    void updateOnTimestepChanged(int timestepIndex);
    void addActor(vtkSmartPointer<vtkActor> newActor);
    void removeActor(vtkSmartPointer<vtkActor> newActor);
    void updateRendering();
    vtkAlgorithmOutput* getOutputPortOfReader();
    vtkUnstructuredGrid *getOutputOfReader();
    void setDataManager(std::shared_ptr<DataManager> dataManager);

    vtkSmartPointer<QVTKInteractor> mInteractor;
private:
    Ui::Analyser3DView *ui;

    std::shared_ptr<DataManager> mDataManager;
    bool isFirstDataRead = true;

    vtkSmartPointer<vtkGenericOpenGLRenderWindow> mRenderWindow;
    vtkSmartPointer<vtkRenderer> mRenderer;
    vtkSmartPointer<vtkInteractorStyleTrackballCamera> mInteractorStyle;
    vtkNamedColors* mColors;
    vtkSmartPointer<vtkOrientationMarkerWidget> mOm2;
    vtkSmartPointer<vtkAxesActor> mActorAxes;
    vtkSmartPointer<vtkXMLUnstructuredGridReader> mReader;

    vtkSmartPointer<vtkOutlineFilter> mOutlineFilter;
    vtkSmartPointer<vtkPolyDataMapper> mOutlineMapper;
    vtkSmartPointer<vtkActor> mOutlineActor;

    vtkSmartPointer<vtkAxesActor> MakeAxesActor();
    void setUpOutlineRendering();
};

#endif // ANALYSER3DVIEW_H
