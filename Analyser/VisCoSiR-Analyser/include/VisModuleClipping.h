#ifndef VISMODULECLIPPING_H
#define VISMODULECLIPPING_H

#include <QWidget>
#include <VisModule.h>
#include <vtkCutter.h>
#include <vtkDataSetMapper.h>
#include <vtkPlane.h>
#include <vtkScalarBarActor.h>
#include <vtkScalarBarWidget.h>

namespace Ui {
class VisModuleClipping;
}

class VisModuleClipping : public VisModule
{
    Q_OBJECT

public:
    explicit VisModuleClipping(QWidget *parent = nullptr);
    ~VisModuleClipping();
    void updateOnDataChanged();
    void applyValuesAndUpdateVisualisation();

private:
    Ui::VisModuleClipping *ui;
    vtkSmartPointer<vtkActor> mActorX, mActorY, mActorZ;
    vtkSmartPointer<vtkPlane> mPlaneX, mPlaneY, mPlaneZ;
    vtkSmartPointer<vtkCutter> mCutterX, mCutterY, mCutterZ;
    vtkSmartPointer<vtkDataSetMapper> mMapperX, mMapperY, mMapperZ;
    vtkSmartPointer<vtkScalarBarActor> mLegendActor;
    vtkSmartPointer<vtkScalarBarWidget> mLegendWidget;
    vtkSmartPointer<vtkLookupTable> mLUT;
    bool mIsActorActiveX = false;
    bool mIsActorActiveY = false;
    bool mIsActorActiveZ = false;

    void UpdatePlaneVisualisation(vtkSmartPointer<vtkDataSetMapper> mapper, vtkSmartPointer<vtkActor> actor, vtkSmartPointer<vtkCutter> cutter, vtkSmartPointer<vtkPlane> plane, double position, double *range);
    bool isFirstTime = true;

};

#endif // VISMODULECLIPPING_H
