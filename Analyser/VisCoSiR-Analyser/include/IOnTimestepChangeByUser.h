#ifndef IONTIMESTEPCHANGEBYUSER_H
#define IONTIMESTEPCHANGEBYUSER_H

class IOnTimestepChangeByUser
{
public:
    virtual void onTimestepSelectionByUser(int newTimestepIndex)=0;
};

#endif // IONTIMESTEPCHANGEBYUSER_H
