#ifndef POINTLIST_H
#define POINTLIST_H

#include <QWidget>
#include <vtkPointSet.h>
#include <vtkSmartPointer.h>

namespace Ui {
class PointList;
}

class PointList : public QWidget
{
    Q_OBJECT

public:
    explicit PointList(QWidget *parent = nullptr);
    ~PointList();
    void SetPoints(vtkSmartPointer<vtkPoints> points);
    vtkSmartPointer<vtkPoints> GetPoints();

private:
    Ui::PointList *ui;
    void InitTable();
    void AddPoint();
    void RemovePoint();
};

#endif // POINTLIST_H
