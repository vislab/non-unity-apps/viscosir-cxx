#ifndef METRICSUTILS_H
#define METRICSUTILS_H

#include <QString>
#include <iterator>
#include <map>


class MetricsUtils{

public:

    inline static const QString METRICS_UNKNOWN = "UNKNOWN-METRICS";
    inline static const QString METRICS_SEPARATOR = "___";

    /**
     * @brief Mapping rawIDs to human readable metrics
     */
    inline const static std::map<QString, QString> metricsMap = { {"DeviationMax", "Deviation Maximum"}, {"DeviationMaxAbs", "Deviation Maximum (absolute)"}, {"L2Norm", "L2 Norm"},  };

    static QString toHumanReadable(const QString rawID)
    {
        for( auto it = metricsMap.begin(); it != metricsMap.end(); ++it ){
          if( it->first == rawID )
              return it->second;
        }
        return METRICS_UNKNOWN;
    }

    static QString toID(const QString humanReadable)
    {
        for( auto it = metricsMap.begin(); it != metricsMap.end(); ++it ){
          if( it->second == humanReadable )
              return it->first;
        }
        return METRICS_UNKNOWN;
    }

};



#endif // METRICSUTILS_H
