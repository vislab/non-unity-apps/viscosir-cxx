#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QFileInfo>



class DataManager
{

public:

    struct Timestep
    {
        Timestep(double time, std::string file) {
            this->time = time;
            this->file = file;
        }
        double time;
        std::string file;
    };


    DataManager();
    bool readCombinerInfoFile(QFileInfo combinerInfoFile);
    std::string getListOfOtherDatasets();

    std::string mNameOfReferenceDataset;
    std::vector<std::string> mNamesOfOtherDatasets;
    std::vector<QString> mArrayNames;
    std::vector<QString> mMetricsRawIDs;
    std::vector<Timestep> mTimesteps;

private:
    void extractMetricAndArrayName(QString rawArrayName);


};

#endif // DATAMANAGER_H
