#ifndef VISMODULECONTOURSURFACE_H
#define VISMODULECONTOURSURFACE_H

#include "vtkContourFilter.h"
#include <QWidget>
#include <VisModule.h>
#include <vtkOutlineFilter.h>
#include <vtkScalarBarActor.h>
#include <vtkScalarBarWidget.h>
#include "vtkPolyDataMapper.h"

namespace Ui {
class VisModuleContourSurface;
}

class VisModuleContourSurface : public VisModule
{
    Q_OBJECT

public:
    explicit VisModuleContourSurface(QWidget *parent = nullptr);
    ~VisModuleContourSurface();
    void applyValuesAndUpdateVisualisation();
    void onSurfArraySelectionChange();
    void onColorArraySelectionChange();
    double* getSurfArrayRange();
    double* getColorArrayRange();

private:
    Ui::VisModuleContourSurface *ui;
    vtkSmartPointer<vtkContourFilter> mContourFilter;
    vtkSmartPointer<vtkPolyDataMapper> mMapper;
    vtkSmartPointer<vtkActor> mActor;
    vtkSmartPointer<vtkScalarBarActor> mLegendActor;
    vtkSmartPointer<vtkScalarBarWidget> mLegendWidget;

    bool isFirstTime = true;

    // VisModule interface
public:
    void updateOnDataChanged();
};

#endif // VISMODULECONTOURSURFACE_H
