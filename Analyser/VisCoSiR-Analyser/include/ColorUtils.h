#ifndef COLORUTILS_H
#define COLORUTILS_H

#include <vtkColorTransferFunction.h>
#include <vtkLookupTable.h>
#include <array>
#include <QColor>


class ColorUtils {

public:

    static inline vtkSmartPointer<vtkLookupTable> mBlackBodyLUT;
    static inline bool isBlackBodyLutAlreadyGenerated = false;
    static inline std::vector<QColor> categoricalSix = { QColor::fromRgb(228,26,28), QColor::fromRgb(55,126,184), QColor::fromRgb(77,175,74), QColor::fromRgb(152,78,163), QColor::fromRgb(255,127,0), QColor::fromRgb(255,255,51) };

    // clang-format off
    /**
     * From: https://examples.vtk.org/site/Cxx/PolyData/CurvaturesAdjustEdges/
     * See: [Diverging Color Maps for Scientific Visualization](https://www.kennethmoreland.com/color-maps/)
     *
     *                   start point         midPoint            end point
     * cool to warm:     0.230, 0.299, 0.754 0.865, 0.865, 0.865 0.706, 0.016, 0.150
     * purple to orange: 0.436, 0.308, 0.631 0.865, 0.865, 0.865 0.759, 0.334, 0.046
     * green to purple:  0.085, 0.532, 0.201 0.865, 0.865, 0.865 0.436, 0.308, 0.631
     * blue to brown:    0.217, 0.525, 0.910 0.865, 0.865, 0.865 0.677, 0.492, 0.093
     * green to red:     0.085, 0.532, 0.201 0.865, 0.865, 0.865 0.758, 0.214, 0.233
     *
     */
    // clang-format on
    static vtkSmartPointer<vtkLookupTable> GetDivergingLut()
    {

      vtkNew<vtkColorTransferFunction> ctf;
      ctf->SetColorSpaceToDiverging();
      ctf->AddRGBPoint(0.0, 0.230, 0.299, 0.754);
      ctf->AddRGBPoint(0.5, 0.865, 0.865, 0.865);
      ctf->AddRGBPoint(1.0, 0.706, 0.016, 0.150);

      auto tableSize = 256;
      vtkNew<vtkLookupTable> lut;
      lut->SetNumberOfTableValues(tableSize);
      lut->Build();

      for (auto i = 0; i < lut->GetNumberOfColors(); ++i)
      {
        std::array<double, 3> rgb;
        ctf->GetColor(static_cast<double>(i) / lut->GetNumberOfColors(), rgb.data());
        std::array<double, 4> rgba{0.0, 0.0, 0.0, 1.0};
        std::copy(std::begin(rgb), std::end(rgb), std::begin(rgba));
        lut->SetTableValue(static_cast<vtkIdType>(i), rgba.data());
      }

      return lut;
    }

    // clang-format off
    /**
     * See: [Color Maps for Scientific Visualization](https://www.kennethmoreland.com/color-maps/)
     */
    // clang-format on
    static vtkSmartPointer<vtkLookupTable> GetBlackBodyLut()
    {
        if( isBlackBodyLutAlreadyGenerated )
        {
            return ColorUtils::mBlackBodyLUT;
        }
        else
        {
            vtkNew<vtkColorTransferFunction> ctf;
            ctf->SetColorSpaceToLabCIEDE2000();
            ctf->AddRGBPoint(0.0,0.0,0.0,0.0);
            ctf->AddRGBPoint(0.14285714285714285,0.2567618382302789,0.08862237092250158,0.06900234709883349);
            ctf->AddRGBPoint(0.2857142857142857,0.502299529628274,0.12275205976842546,0.10654041357261984);
            ctf->AddRGBPoint(0.42857142857142855,0.7353154662963063,0.1982320329476474,0.12428036101896534);
            ctf->AddRGBPoint(0.5714285714285714,0.8771435867383445,0.39490510462624345,0.03816328606394868);
            ctf->AddRGBPoint(0.7142857142857142,0.911232394909533,0.631724377007152,0.10048201891972874);
            ctf->AddRGBPoint(0.8571428571428571,0.9072006655243174,0.8550025783221541,0.18879408728283467);
            ctf->AddRGBPoint(1.0,1.0,1.0,1.0);

            auto tableSize = 256;
            ColorUtils::mBlackBodyLUT = vtkSmartPointer<vtkLookupTable>::New();
            ColorUtils::mBlackBodyLUT->SetNumberOfTableValues(tableSize);
            ColorUtils::mBlackBodyLUT->Build();

            for (auto i = 0; i < ColorUtils::mBlackBodyLUT->GetNumberOfColors(); ++i)
            {
              std::array<double, 3> rgb;
              ctf->GetColor(static_cast<double>(i) / ColorUtils::mBlackBodyLUT->GetNumberOfColors(), rgb.data());
              std::array<double, 4> rgba{0.0, 0.0, 0.0, 1.0};
              std::copy(std::begin(rgb), std::end(rgb), std::begin(rgba));
              ColorUtils::mBlackBodyLUT->SetTableValue(static_cast<vtkIdType>(i), rgba.data());
            }

            isBlackBodyLutAlreadyGenerated = true;
            return ColorUtils::mBlackBodyLUT;
        }
    }

    /**
     * @brief Get Color from a Categorical Six-Item ColorMap (from Color Brewer 2, see: https://colorbrewer2.org/#type=qualitative&scheme=Set1&n=6)
     * @param index the index in the ColorMap: 0 <= index <= 6
     * @return the color from the map for the given index (if index larger than list % is applied)
     */
    static QColor GetCategoricalSix(int index)
    {
        return categoricalSix[ index % categoricalSix.size() ];
    }


};

#endif // COLORUTILS_H
