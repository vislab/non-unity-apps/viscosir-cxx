#ifndef VISMODULESAMPLEPOINTS_H
#define VISMODULESAMPLEPOINTS_H

#include <QtCharts>
#include <QWidget>
#include <VisModule.h>
#include <vtkActor.h>
#include <vtkGlyph3D.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkResampleWithDataSet.h>
#include <vtkSphereSource.h>

#include <Model/SamplingResult.h>

namespace Ui {
class VisModuleSamplePoints;
}

class VisModuleSamplePoints : public VisModule
{
    Q_OBJECT

public:
    explicit VisModuleSamplePoints(QWidget *parent = nullptr);
    ~VisModuleSamplePoints();
    void UpdateSampling();
    void updateOnDataChanged();

private:
    Ui::VisModuleSamplePoints *ui;
    bool mIsNotYetAddedToTheVisualization = true;
    vtkSmartPointer<vtkPolyData> mPolydata;
    vtkSmartPointer<vtkResampleWithDataSet> mResamplingFilter;
    vtkSmartPointer<vtkSphereSource> mSphereSource;
    vtkSmartPointer<vtkPolyDataMapper> mMapper;
    vtkSmartPointer<vtkActor> mActor;
    vtkSmartPointer<vtkGlyph3D> mGlyphFilter;

    QChart* mChart;

    void updateFiltersAndSamplingWithUsersValues();
    void initialSetup();
    void updateChartWithSamplingResults(std::shared_ptr<std::vector<SamplingResult> > samplingResults);
};

#endif // VISMODULESAMPLEPOINTS_H
