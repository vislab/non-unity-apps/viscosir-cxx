#ifndef VISMODULEFILTER_H
#define VISMODULEFILTER_H

#include "VisModuleTimesteps.h"
#include <QWidget>
#include <VisModule.h>
#include <vtkXMLUnstructuredGridReader.h>

#include <External/exprtk.hpp>

namespace Ui {
class VisModuleFilter;
}

class VisModuleFilter : public VisModule
{
    Q_OBJECT

public:
    explicit VisModuleFilter(QWidget *parent = nullptr);
    ~VisModuleFilter();
    void applyFilterToAllTimesteps();
    void updateOnDataChanged();
    void setTimestepModule( VisModuleTimesteps* timestepModule );

private:
    Ui::VisModuleFilter *ui;

    // For iterating over all timesteps
    vtkSmartPointer<vtkXMLUnstructuredGridReader> mReader;

    // For the condition
    exprtk::symbol_table<double> mSymbolTable;
    exprtk::expression<double> mExpression;
    exprtk::parser<double> mParser;
    double mCurrentValueForConditionCalculation;

    // For visualizing filter results
    VisModuleTimesteps* mTimestepModule;

};

#endif // VISMODULEFILTER_H
