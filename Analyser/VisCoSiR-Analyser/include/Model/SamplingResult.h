#ifndef SAMPLINGRESULT_H
#define SAMPLINGRESULT_H

#include <vector>

class SamplingResult{

public:
    int index;
    std::vector<double> sample;
    double* point;
};

#endif // SAMPLINGRESULT_H
