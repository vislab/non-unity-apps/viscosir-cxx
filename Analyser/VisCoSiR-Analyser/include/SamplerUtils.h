#ifndef SAMPLERUTILS_H
#define SAMPLERUTILS_H

#include "DataManager.h"
#include "Model/SamplingResult.h"
#include "vtkPointData.h"
#include "vtkPoints.h"
#include "vtkUnstructuredGrid.h"

#include <vtkDoubleArray.h>
#include <vtkPolyData.h>
#include <vtkResampleWithDataSet.h>
#include <vtkSmartPointer.h>
#include <vtkXMLUnstructuredGridReader.h>
class SamplerUtils
{

public:

    static void SamplePointsInDataset( vtkSmartPointer<vtkPoints> points, std::vector<DataManager::Timestep> timesteps, std::string arrayName, std::shared_ptr<std::vector<SamplingResult>> results )
    {
        // Prepare the result vector
        results->clear();
        for (int indexPoint = 0; indexPoint < points->GetNumberOfPoints(); ++indexPoint) {
            results->push_back( SamplingResult() );
            results->at(indexPoint).index = indexPoint;
            results->at(indexPoint).point = points->GetPoint(indexPoint);
            results->at(indexPoint).sample = std::vector<double>();
        }

        // Create pipeline
        vtkNew<vtkXMLUnstructuredGridReader> reader;
        vtkNew<vtkPolyData> polyData;
        polyData->SetPoints( points );
        vtkNew<vtkResampleWithDataSet> resampler;
        resampler->SetMarkBlankPointsAndCells( false );
        // Input specifies the 'structure-provider'
        resampler->SetInputData( polyData );

        // Iterate over timesteps
        for (int indexTimestep = 0; indexTimestep < timesteps.size(); ++indexTimestep) {
            // Read and resample the dataset
            reader->SetFileName( timesteps[indexTimestep].file.c_str() );
            reader->Update();
            // Source specifies the 'values-provider'
            resampler->SetSourceData( reader->GetOutput() );
            resampler->Update();
            auto pointData = static_cast<vtkUnstructuredGrid*>( resampler->GetOutput() )->GetPointData();
            vtkDoubleArray* array = static_cast<vtkDoubleArray*>( pointData->GetArray( arrayName.c_str() ) );
            // Iterate over points
            for (int indexPoint = 0; indexPoint < points->GetNumberOfPoints(); ++indexPoint) {
                // Append value in results
                results->at(indexPoint).sample.push_back( array->GetValue( indexPoint ) );
            }
        }
    }

};

#endif // SAMPLERUTILS_H
