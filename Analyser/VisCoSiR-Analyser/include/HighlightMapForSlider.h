#ifndef HIGHLIGHTMAPFORSLIDER_H
#define HIGHLIGHTMAPFORSLIDER_H

#include <QPushButton>
#include <QWidget>

namespace Ui {
class HighlightMapForSlider;
}

class HighlightMapForSlider : public QWidget
{
    Q_OBJECT

public:
    explicit HighlightMapForSlider(QWidget *parent = nullptr);
    ~HighlightMapForSlider();

    void init(int elementCount);
    void setHighlights(std::vector<bool> highlights);

private:
    Ui::HighlightMapForSlider *ui;
    inline static const std::string btStyle = "margin-left: 0px; margin-right: 0px; border:none; ";
    inline static const std::string btStyle_Highlight_Enabled = " background-color: #858db2; ";
    inline static const std::string btStyle_Highlight_Disabled = " background-color: #52576e; ";
    inline static const std::string btStyle_Initial = " background-color: #4e4e4e; ";
    std::vector<QPushButton*> mElements;
};

#endif // HIGHLIGHTMAPFORSLIDER_H
