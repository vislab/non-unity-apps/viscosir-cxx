#include "VisModuleTimesteps.h"
#include "ui_VisModuleTimesteps.h"

VisModuleTimesteps::VisModuleTimesteps(QWidget *parent) :
    VisModule(parent),
    ui(new Ui::VisModuleTimesteps)
{
    ui->setupUi(this);

    // Setup the slider
    ui->timestepSlider->setSingleStep(1);
    ui->timestepSlider->setPageStep(1);
    ui->timestepSlider->setCursor( QCursor( Qt::PointingHandCursor ) );

    // Hide legend as long as there is no highlight / filter
    ui->wiHighlightColor->setVisible( false );
    ui->lbHighlightInfo->setVisible( false );

    connect(ui->timestepSlider, &QSlider::valueChanged, this, &VisModuleTimesteps::setTimestep);
}

VisModuleTimesteps::~VisModuleTimesteps()
{
    delete ui;
}

void VisModuleTimesteps::setTimestep(int timestepIndex)
{
    if(timestepIndex < mDataManager->mTimesteps.size())
    {
        // Set text for timestep + time value
        ui->tfCurrentTimestepIndex->setText( std::to_string(timestepIndex+1).c_str() );
        QString timeFormattedString = QString("  (%1 min)").arg( mDataManager->mTimesteps[timestepIndex].time / 3600.0 );
        ui->lbTimeValue->setText( timeFormattedString );
        // Adjust slider position/value
        ui->timestepSlider->setTracking(false);
        ui->timestepSlider->setValue(timestepIndex);
        ui->timestepSlider->setTracking(true);
        // External feedback
        mAnalyser3DView->updateOnTimestepChanged(timestepIndex);
    }
    else
    {
        std::cout << "Error! Could not set timestep because index was out of list range: " << timestepIndex << std::endl;
    }
}

void VisModuleTimesteps::onSliderValueChangeByUser()
{
    setTimestep( ui->timestepSlider->value() );
}

void VisModuleTimesteps::setHighlights(std::vector<bool> highlights, QString information)
{
    // Set the highlights
    ui->highlightMapForSlider->setHighlights( highlights );
    // Display the legend / info
    ui->wiHighlightColor->setVisible( true );
    ui->lbHighlightInfo->setVisible( true );
    ui->lbHighlightInfo->setText( information );
}

void VisModuleTimesteps::updateOnDataChanged()
{
    // Init the highlight map
    ui->highlightMapForSlider->init( mDataManager->mTimesteps.size() );

    // Set count of all timesteps
    ui->lbTimestepsCountAll->setText( std::to_string(mDataManager->mTimesteps.size()).c_str() );

    // Adjust the silder range
    ui->timestepSlider->setMinimum(0);
    ui->timestepSlider->setMaximum(mDataManager->mTimesteps.size()-1);

    // Set current timestep to the first one
    setTimestep(0);
}
