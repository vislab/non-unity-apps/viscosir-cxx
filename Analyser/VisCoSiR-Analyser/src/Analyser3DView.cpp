#include "Analyser3DView.h"
#include "ui_Analyser3DView.h"
#include "vtkCamera.h"
#include "vtkCaptionActor2D.h"
#include "vtkUnstructuredGrid.h"

#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkTextProperty.h>



#include <vtkContourFilter.h>
#include <vtkOutlineFilter.h>
#include <vtkPerlinNoise.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkSampleFunction.h>


Analyser3DView::Analyser3DView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Analyser3DView),
    mRenderWindow(vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New()),
    mRenderer(vtkSmartPointer<vtkRenderer>::New()),
    mInteractor(vtkSmartPointer<QVTKInteractor>::New()),
    mInteractorStyle(vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New()),
    mColors(vtkNamedColors::New()),
    mOm2(vtkSmartPointer<vtkOrientationMarkerWidget>::New()),
    mReader(vtkSmartPointer<vtkXMLUnstructuredGridReader>::New()),
    mOutlineFilter(vtkSmartPointer<vtkOutlineFilter>::New()),
    mOutlineMapper(vtkSmartPointer<vtkPolyDataMapper>::New()),
    mOutlineActor(vtkSmartPointer<vtkActor>::New())
{
    ui->setupUi(this);

    // Set up the rendering
    mRenderWindow->AddRenderer(mRenderer);
    mRenderWindow->SetInteractor(mInteractor);
    ui->openGLWidget->SetRenderWindow(mRenderWindow);
    mInteractor->SetInteractorStyle(mInteractorStyle);
    mInteractor->Initialize();
    mRenderer->GetActiveCamera()->Azimuth(45);
    mRenderer->GetActiveCamera()->Elevation(45);

    // Set the background color
    mRenderer->SetBackground( vtkColor3d(0.32, 0.34, 0.43).GetData() );

    // Create and add Axes
    mActorAxes = MakeAxesActor();
    mOm2->SetOrientationMarker(mActorAxes);
    mOm2->SetInteractor(mInteractor);
    mOm2->SetViewport(0.0, 0.8, 0.2, 1.0);
    mOm2->EnabledOn();
    mOm2->InteractiveOn();
}

Analyser3DView::~Analyser3DView()
{
    delete ui;
}

/**
 * @brief Setter for the reference to the DataManager
 * @param dataManager the reference to the DataManager
 */
void Analyser3DView::setDataManager(std::shared_ptr<DataManager> dataManager)
{
    mDataManager = dataManager;
}

void Analyser3DView::updateOnTimestepChanged(int timestepIndex)
{
    cout << "Update timestep: " << timestepIndex << endl;
    // Read the file of the current timestep
    std::string fileName = mDataManager->mTimesteps[timestepIndex].file;
    mReader->SetFileName(fileName.c_str());
    mReader->Update();
    if(isFirstDataRead)
    {
        isFirstDataRead = false;
        setUpOutlineRendering();
    }
    mRenderWindow->Render();
}

/**
 * @brief Add an actors to the current visualisation
 * @param newActor the new actor
 */
void Analyser3DView::addActor(vtkSmartPointer<vtkActor> newActor)
{
    mRenderer->AddActor(newActor);
    mRenderer->ResetCamera();
    mRenderWindow->Render();
}

void Analyser3DView::removeActor(vtkSmartPointer<vtkActor> newActor)
{
    mRenderer->RemoveActor(newActor);
    mRenderer->ResetCamera();
    mRenderWindow->Render();
}

void Analyser3DView::updateRendering()
{
    mRenderWindow->Render();
}

/**
 * @brief Getter for the reader's output-port. The reader always provides the data according to the current timestep
 * @return the reader's output-port
 */
vtkAlgorithmOutput* Analyser3DView::getOutputPortOfReader()
{
    return mReader->GetOutputPort();
}

/**
 * @brief Getter for the reader's output. The reader always provides the data according to the current timestep
 * @return the reader's output as vtkUnstructuredGrid
 */
vtkUnstructuredGrid* Analyser3DView::getOutputOfReader()
{
    return mReader->GetOutput();
}

vtkSmartPointer<vtkAxesActor> Analyser3DView::MakeAxesActor()
{
  vtkNew<vtkAxesActor> axes;
  axes->SetScale(2.0, 2.0, 2.0);
  axes->SetShaftTypeToCylinder();
  axes->SetXAxisLabelText("X");
  axes->SetYAxisLabelText("Y");
  axes->SetZAxisLabelText("Z");
  axes->SetCylinderRadius(0.5 * axes->GetCylinderRadius());
  axes->SetConeRadius(1.025 * axes->GetConeRadius());
  axes->SetSphereRadius(1.5 * axes->GetSphereRadius());
  vtkTextProperty* tprop = axes->GetXAxisCaptionActor2D()->GetCaptionTextProperty();
  tprop->ItalicOn();
  tprop->ShadowOn();
  tprop->SetFontFamilyToTimes();
  tprop->SetFontSize(5);
  tprop->SetColor(0.0, 0.0, 0.0);
  // Use the same text properties on the other two axes.
  axes->GetYAxisCaptionActor2D()->GetCaptionTextProperty()->ShallowCopy(tprop);
  axes->GetZAxisCaptionActor2D()->GetCaptionTextProperty()->ShallowCopy(tprop);
  axes->GetXAxisCaptionActor2D()->SetHeight(0.15);
  axes->GetYAxisCaptionActor2D()->SetHeight(0.15);
  axes->GetZAxisCaptionActor2D()->SetHeight(0.15);
  return axes;
}

void Analyser3DView::setUpOutlineRendering()
{
    // Set up outline rendering
    mOutlineFilter->SetInputConnection(mReader->GetOutputPort());
    mOutlineMapper->SetInputConnection(mOutlineFilter->GetOutputPort());
    mOutlineActor->SetMapper(mOutlineMapper);
    mOutlineActor->GetProperty()->SetColor(0, 0, 0);
    addActor(mOutlineActor);
}
