#include "VisModuleClipping.h"
#include "ui_VisModuleClipping.h"
#include "vtkPointData.h"
#include "vtkProperty.h"
#include "vtkUnstructuredGrid.h"

#include <ColorUtils.h>
#include <MetricsUtils.h>

VisModuleClipping::VisModuleClipping(QWidget *parent) :
    VisModule(parent),
    ui(new Ui::VisModuleClipping),
    mLUT(vtkSmartPointer<vtkLookupTable>::New()),
    mPlaneX(vtkSmartPointer<vtkPlane>::New()),
    mPlaneY(vtkSmartPointer<vtkPlane>::New()),
    mPlaneZ(vtkSmartPointer<vtkPlane>::New()),
    mCutterX(vtkSmartPointer<vtkCutter>::New()),
    mCutterY(vtkSmartPointer<vtkCutter>::New()),
    mCutterZ(vtkSmartPointer<vtkCutter>::New()),
    mMapperX(vtkSmartPointer<vtkDataSetMapper>::New()),
    mMapperY(vtkSmartPointer<vtkDataSetMapper>::New()),
    mMapperZ(vtkSmartPointer<vtkDataSetMapper>::New()),
    mActorX(vtkSmartPointer<vtkActor>::New()),
    mActorY(vtkSmartPointer<vtkActor>::New()),
    mActorZ(vtkSmartPointer<vtkActor>::New()),
    mLegendActor(vtkSmartPointer<vtkScalarBarActor>::New()),
    mLegendWidget(vtkSmartPointer<vtkScalarBarWidget>::New())
{
    ui->setupUi(this);

    // Prepare visualisation
    mLegendActor->SetOrientationToHorizontal();

    // Prepare button
    connect(ui->btUpdateVis, &QPushButton::clicked, this, &VisModuleClipping::applyValuesAndUpdateVisualisation );
    ui->btUpdateVis->setCursor(QCursor(Qt::PointingHandCursor));

}

VisModuleClipping::~VisModuleClipping()
{
    delete ui;
}


void VisModuleClipping::applyValuesAndUpdateVisualisation()
{
    // Initially create the LUT & Set planes' normals
    if(isFirstTime)
    {
        mLUT = ColorUtils::GetBlackBodyLut();
        mPlaneX->SetNormal(1, 0, 0);
        mPlaneY->SetNormal(0, 1, 0);
        mPlaneZ->SetNormal(0, 0, 1);
    }

    // Set user's array selection and get range
    QString arrayName = ui->cbSelectedArray->currentText() + MetricsUtils::METRICS_SEPARATOR + MetricsUtils::toID(ui->cbMetricForColoring->currentText());
    mAnalyser3DView->getOutputOfReader()->GetPointData()->SetActiveScalars( arrayName.toStdString().c_str() );
    auto range = mAnalyser3DView->getOutputOfReader()->GetPointData()->GetArray( arrayName.toStdString().c_str() )->GetRange();

    // Update the visualisation properties
    UpdatePlaneVisualisation( mMapperX,
                              mActorX,
                              mCutterX,
                              mPlaneX,
                              std::stod(ui->tfPositionX->text().toStdString()),
                              range);
    // Add or remove the actor if necessary
    if( ui->cbX->isChecked() && !mIsActorActiveX )
    {
        mIsActorActiveX = true;
        mAnalyser3DView->addActor( mActorX );
    }
    else if( !ui->cbX->isChecked() && mIsActorActiveX )
    {
        mAnalyser3DView->removeActor( mActorX );
        mIsActorActiveX = false;
    }

    // Update the visualisation properties
    UpdatePlaneVisualisation( mMapperY,
                              mActorY,
                              mCutterY,
                              mPlaneY,
                              std::stod(ui->tfPositionY->text().toStdString()),
                              range);
    // Add or remove the actor if necessary
    if( ui->cbY->isChecked() && !mIsActorActiveY )
    {
        mIsActorActiveY = true;
        mAnalyser3DView->addActor( mActorY );
    }
    else if( !ui->cbY->isChecked() && mIsActorActiveY )
    {
        mAnalyser3DView->removeActor( mActorY );
        mIsActorActiveY = false;
    }

    // Update the visualisation properties
    UpdatePlaneVisualisation( mMapperZ,
                              mActorZ,
                              mCutterZ,
                              mPlaneZ,
                              std::stod(ui->tfPositionZ->text().toStdString()),
                              range);
    // Add or remove the actor if necessary
    if( ui->cbZ->isChecked() && !mIsActorActiveZ )
    {
        mIsActorActiveZ = true;
        mAnalyser3DView->addActor( mActorZ );
    }
    else if( !ui->cbZ->isChecked() && mIsActorActiveZ )
    {
        mAnalyser3DView->removeActor( mActorZ );
        mIsActorActiveZ = false;
    }


    // Update Scalar Bar / Legend
    mLegendActor->SetLookupTable( mLUT );
    mLegendActor->Modified();

    // If necessary: Add the actor to the visualisation
    if(isFirstTime)
    {
        isFirstTime = false;

        // Create the scalarBarWidget
        mLegendWidget->SetInteractor( mAnalyser3DView->mInteractor );
        mLegendWidget->SetScalarBarActor( mLegendActor );
        mLegendWidget->On();
    }
}

void VisModuleClipping::UpdatePlaneVisualisation(vtkSmartPointer<vtkDataSetMapper> mapper, vtkSmartPointer<vtkActor> actor, vtkSmartPointer<vtkCutter> cutter, vtkSmartPointer<vtkPlane> plane, double position, double* range)
{
    // Set the plane's origin
    plane->SetOrigin( plane->GetNormal()[0] * position , plane->GetNormal()[1] * position, plane->GetNormal()[2] * position);

    // Setup of the cutter
    cutter->SetInputConnection( mAnalyser3DView->getOutputPortOfReader() );
    cutter->SetCutFunction( plane );

    // Setup of the mapper
    mapper->SetInputConnection( cutter->GetOutputPort() );
    mapper->SetScalarRange( range );
    mapper->SetLookupTable( mLUT );
    mapper->Update();

    // Set up the actor
    actor->SetMapper( mapper );
    actor->Modified();
    actor->GetProperty()->LightingOff();
}

void VisModuleClipping::updateOnDataChanged()
    {
    // Update the list of available arrays
    ui->cbSelectedArray->clear();
    for( auto const &arrayName : mDataManager->mArrayNames )
    {
        ui->cbSelectedArray->addItem( arrayName );
    }

    // Update the list of available metrics
    ui->cbMetricForColoring->clear();
    for ( auto const &metricsID : mDataManager->mMetricsRawIDs )
    {
        QString metricsHumanReadable = MetricsUtils::toHumanReadable( metricsID );
        if( metricsHumanReadable != MetricsUtils::METRICS_UNKNOWN )
            ui->cbMetricForColoring->addItem( MetricsUtils::toHumanReadable( metricsID ) );
    }
}
