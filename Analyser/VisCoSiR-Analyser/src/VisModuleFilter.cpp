#include "VisModuleFilter.h"
#include "ui_VisModuleFilter.h"
#include "vtkPointData.h"
#include "vtkUnstructuredGrid.h"

#include <MetricsUtils.h>
#include <vtkDoubleArray.h>

VisModuleFilter::VisModuleFilter(QWidget *parent) :
    VisModule(parent),
    ui(new Ui::VisModuleFilter),
    mReader(vtkSmartPointer<vtkXMLUnstructuredGridReader>::New())
{
    ui->setupUi(this);

    // Prepare condition calculation setup
    mCurrentValueForConditionCalculation = 0.0;
    mSymbolTable.add_variable("value", mCurrentValueForConditionCalculation);
    mExpression.register_symbol_table(mSymbolTable);

    // Prepare button
    connect(ui->btUpdateVis, &QPushButton::clicked, this, &VisModuleFilter::applyFilterToAllTimesteps );
    ui->btUpdateVis->setCursor(QCursor(Qt::PointingHandCursor));

}


void VisModuleFilter::applyFilterToAllTimesteps()
{
    // Result
    std::vector<bool> result;

    // Prepare condition
    std::string expression_string = ui->tfCondition->text().toStdString();
    mParser.compile(expression_string, mExpression);

    // Prepare fileName and arrayName
    QString arrayName = ui->cbArray->currentText() + MetricsUtils::METRICS_SEPARATOR + MetricsUtils::toID(ui->cbMetric->currentText());
    std::string fileName;


    // For all timesteps
    for (int timestepIndex = 0; timestepIndex < mDataManager->mTimesteps.size(); ++timestepIndex) {
        fileName = mDataManager->mTimesteps[timestepIndex].file;
        mReader->SetFileName(fileName.c_str());
        mReader->Update();

        bool resultForCurrentTimestep = false;
        vtkDoubleArray* array = static_cast<vtkDoubleArray*>( mReader->GetOutput()->GetPointData()->GetArray( arrayName.toStdString().c_str() ) );

        // For all values in the array
        for (int currentValueIndex = 0; currentValueIndex < array->GetSize(); ++currentValueIndex) {
            mCurrentValueForConditionCalculation = array->GetValue(currentValueIndex);
            if( mExpression.value() == 1.0 )
            {
                resultForCurrentTimestep = true;
                break;
            }
        }
        result.push_back( resultForCurrentTimestep );
    }

    QString expression_qstring = QString::fromStdString( expression_string );
    QString dataName = ui->cbMetric->currentText() + " of " + ui->cbArray->currentText();
    QString information = expression_qstring.replace( "value",  dataName );

    mTimestepModule->setHighlights( result, information );
}


void VisModuleFilter::updateOnDataChanged()
    {
    // Update the list of available arrays
    ui->cbArray->clear();
    for( auto const &arrayName : mDataManager->mArrayNames )
    {
        ui->cbArray->addItem( arrayName );
    }

    // Update the list of available metrics
    ui->cbMetric->clear();
    for ( auto const &metricsID : mDataManager->mMetricsRawIDs )
    {
        QString metricsHumanReadable = MetricsUtils::toHumanReadable( metricsID );
        if( metricsHumanReadable != MetricsUtils::METRICS_UNKNOWN )
            ui->cbMetric->addItem( MetricsUtils::toHumanReadable( metricsID ) );
    }
}

void VisModuleFilter::setTimestepModule(VisModuleTimesteps* timestepModule)
{
    mTimestepModule = timestepModule;
}

VisModuleFilter::~VisModuleFilter()
{
    delete ui;

}
