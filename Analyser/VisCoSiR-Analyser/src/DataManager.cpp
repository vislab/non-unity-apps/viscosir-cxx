#include "DataManager.h"
#include "qjsonarray.h"
#include "qjsondocument.h"
#include "qjsonobject.h"

#include <vtkSmartPointer.h>
#include <vtkPointData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <QString>
#include <MetricsUtils.h>

/**
 * @brief Manage the data created by the combiner
 */
DataManager::DataManager(){}

/**
 * @brief Read and store information from the given file
 * @param combinerInfoFile the file to read from
 * @return the success as bool; potential fails: file not accessable or not all relevant attributes set
 */
bool DataManager::readCombinerInfoFile(QFileInfo combinerInfoFile)
{
    // Reset values
    mNameOfReferenceDataset = "unknown";
    mNamesOfOtherDatasets.clear();
    mArrayNames.clear();
    mMetricsRawIDs.clear();
    mTimesteps.clear();

    // Load + check file
    QFile combinerFile(combinerInfoFile.absoluteFilePath());
    if (!combinerFile.open(QIODevice::ReadOnly)) {
        return false;
    }

    // Read and convert to Json
    QByteArray combinerData = combinerFile.readAll();
    QJsonDocument combinerDocument(QJsonDocument::fromJson(combinerData));
    QJsonObject combinerInfo = combinerDocument.object();

    // Set dataset names
    if ( combinerInfo.contains("calculationReferenceDatasetName") )
        mNameOfReferenceDataset = combinerInfo["calculationReferenceDatasetName"].toString().toStdString();
    if ( combinerInfo.contains("otherDatasets") && combinerInfo["otherDatasets"].isArray() )
    {
        QJsonArray otherDatasetsArray = combinerInfo["otherDatasets"].toArray();
        for (int datasetIndex = 0; datasetIndex < otherDatasetsArray.size(); ++datasetIndex) {
            mNamesOfOtherDatasets.push_back( otherDatasetsArray.at(datasetIndex).toString().toStdString() );
        }
    }

    // Read timestep-information
    if ( combinerInfo.contains("timesteps") && combinerInfo["timesteps"].isArray() )
    {
        QJsonArray timestepsArray = combinerInfo["timesteps"].toArray();
        for (int timestepIndex = 0; timestepIndex < timestepsArray.size(); ++timestepIndex) {
            std::string file = timestepsArray.at(timestepIndex)["file"].toString().toStdString();
            double time = timestepsArray.at(timestepIndex)["time"].toDouble();
            mTimesteps.push_back( Timestep(time, file) );
        }
    }

    // Read arrays (from first vtu file)
    if(mTimesteps.size()>0)
    {
        std::string fileName = mTimesteps[0].file;
        vtkNew<vtkXMLUnstructuredGridReader> reader;
        reader->SetFileName(fileName.c_str());
        reader->Update();
        vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid = reader->GetOutput();
        for (int arrayIndex = 0; arrayIndex < unstructuredGrid->GetPointData()->GetNumberOfArrays(); ++arrayIndex) {
            extractMetricAndArrayName( unstructuredGrid->GetPointData()->GetArray(arrayIndex)->GetName() );
        }
    }

    // Report success
    return mNameOfReferenceDataset!="unknown" && mNamesOfOtherDatasets.size()>0 && mTimesteps.size()>0;
}

/**
 * @brief Convenience getter for a list of the names of the datasets (excluding the reference dataset)
 * @return the list of names separated by ','
 */
std::string DataManager::getListOfOtherDatasets()
{
    std::string list = "";
    std::string delimiter = ", ";
    for (int i = 0; i < mNamesOfOtherDatasets.size(); ++i) {
        list += mNamesOfOtherDatasets[i];
        if( i != mNamesOfOtherDatasets.size()-1 )
            list += delimiter;
    }
    return list;
}

/**
 * @brief Extract the available metrics and the array name + add these values in mArrayNames and mAvailableMetrics (if they are not yet contained)
 * @param rawArrayName the raw array name as created by the combiner tool
 */
void DataManager::extractMetricAndArrayName(QString rawArrayName)
{
    if (rawArrayName.contains( MetricsUtils::METRICS_SEPARATOR ) )
    {
        auto namesList = rawArrayName.split( MetricsUtils::METRICS_SEPARATOR );
        QString pureArrayName = namesList[0];
        QString pureMetricsID = namesList[1];
        if( std::find(mArrayNames.begin(), mArrayNames.end(), pureArrayName) == mArrayNames.end() )
            mArrayNames.push_back( pureArrayName );
        if( std::find(mMetricsRawIDs.begin(), mMetricsRawIDs.end(), pureMetricsID) == mMetricsRawIDs.end() )
            mMetricsRawIDs.push_back( pureMetricsID );
    }
    else
    {
        mArrayNames.push_back( rawArrayName );
    }
}



