#include "VisModule.h"


VisModule::VisModule(QWidget *parent) : QWidget(parent){}

VisModule::~VisModule(){}

/**
 * @brief Setter for the reference to the DataManager
 * @param dataManager the reference to the DataManager
 */
void VisModule::setDataManager(std::shared_ptr<DataManager> dataManager)
{
    mDataManager = dataManager;
}


/**
 * @brief Setter for the reference to the 3D view
 * @param dataManager the reference
 */
void VisModule::setAnalyser3DView(std::shared_ptr<Analyser3DView> analyser3DView)
{
    mAnalyser3DView = analyser3DView;
}
