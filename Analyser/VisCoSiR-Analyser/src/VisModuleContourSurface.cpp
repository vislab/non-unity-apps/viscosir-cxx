#include "VisModuleContourSurface.h"
#include "ui_VisModuleContourSurface.h"
#include "vtkPointData.h"
#include "vtkProperty.h"
#include "vtkUnstructuredGrid.h"
#include <QPushButton>
#include <MetricsUtils.h>
#include <vtkOutlineFilter.h>
#include <vtkPerlinNoise.h>
#include <vtkSampleFunction.h>
#include <vtkScalarBarActor.h>
#include <vtkScalarBarWidget.h>
#include <vtkScalarsToColors.h>
#include <ColorUtils.h>

VisModuleContourSurface::VisModuleContourSurface(QWidget *parent) :
    VisModule(parent),
    ui(new Ui::VisModuleContourSurface),
    mContourFilter(vtkSmartPointer<vtkContourFilter>::New()),
    mMapper(vtkSmartPointer<vtkPolyDataMapper>::New()),
    mActor(vtkSmartPointer<vtkActor>::New()),
    mLegendActor(vtkSmartPointer<vtkScalarBarActor>::New()),
    mLegendWidget(vtkSmartPointer<vtkScalarBarWidget>::New())
{
    ui->setupUi(this);

    // Prepare visualisation
    mLegendActor->SetOrientationToHorizontal();

    // Prepare button
    connect(ui->btUpdateVis, &QPushButton::clicked, this, &VisModuleContourSurface::applyValuesAndUpdateVisualisation );
    ui->btUpdateVis->setCursor(QCursor(Qt::PointingHandCursor));

}

VisModuleContourSurface::~VisModuleContourSurface()
{
    delete ui;
}


void VisModuleContourSurface::applyValuesAndUpdateVisualisation()
{
    // Set the arrays to be used for surface and coloring
    QString dataNameForColoring = ui->cbSelectedArray->currentText() + MetricsUtils::METRICS_SEPARATOR + MetricsUtils::toID(ui->cbMetricForColoring->currentText());
    QString dataNameForSurface = ui->cbSelectedArray->currentText();
    mAnalyser3DView->getOutputOfReader()->GetPointData()->SetActiveScalars( dataNameForSurface.toStdString().c_str() );

    // Create surface
    mContourFilter->SetInputConnection( mAnalyser3DView->getOutputPortOfReader() );
    double value = std::stod(ui->tfContourValue->text().toStdString());
    mContourFilter->SetValue(0, value);
    mContourFilter->Update();

    // Create mapper and specify array to be used for coloring
    mMapper->SetInputConnection(mContourFilter->GetOutputPort());
    mMapper->ScalarVisibilityOn();
    auto range = getColorArrayRange();
    mMapper->SetScalarRange(range[0], range[1]);
    mMapper->SetLookupTable( ColorUtils::GetDivergingLut() );
    mMapper->SetScalarModeToUsePointFieldData();
    mMapper->ColorByArrayComponent(dataNameForColoring.toStdString().c_str(), 0);
    mMapper->Update();

    // Create link between actor and mapper
    mActor->SetMapper(mMapper);
    mActor->GetProperty()->LightingOff();
    mActor->Modified();

    // Update Scalar Bar / Legend
    mLegendActor->SetLookupTable(mMapper->GetLookupTable());
    mLegendActor->Modified();

    // If necessary: Add the actor to the visualisation
    if(isFirstTime)
    {
        isFirstTime = false;
        mAnalyser3DView->addActor(mActor);

        // Create the scalarBarWidget
        mLegendWidget->SetInteractor( mAnalyser3DView->mInteractor );
        mLegendWidget->SetScalarBarActor( mLegendActor );
        mLegendWidget->On();

        //mAnalyser3DView->addActor(mLegendActor);
    }
}

void VisModuleContourSurface::onSurfArraySelectionChange()
{
    auto range = getSurfArrayRange();
    QString rangeText = QString("[ %1 - %2 ]").arg(range[0], 0, 'f', 8).arg(range[1], 0, 'f', 8);
    ui->lbRangeInfoSurfArray->setText(rangeText);
}

void VisModuleContourSurface::onColorArraySelectionChange()
{
    auto range = getColorArrayRange();
    QString rangeText = QString("[ %1 - %2 ]").arg(range[0], 0, 'f', 8).arg(range[1], 0, 'f', 8);
    ui->lbRangeInfoColorArray->setText(rangeText);
}

double *VisModuleContourSurface::getSurfArrayRange()
{
    auto currentlySelectedArray = ui->cbSelectedArray->currentText().toStdString();
    double* range = mAnalyser3DView->getOutputOfReader()->GetPointData()->GetArray( currentlySelectedArray.c_str() )->GetRange();
    return range;
}

double *VisModuleContourSurface::getColorArrayRange()
{
    auto currentlySelectedArrayForColor = ui->cbSelectedArray->currentText() + MetricsUtils::METRICS_SEPARATOR + MetricsUtils::toID(ui->cbMetricForColoring->currentText());
    double* range = mAnalyser3DView->getOutputOfReader()->GetPointData()->GetArray( currentlySelectedArrayForColor.toStdString().c_str() )->GetRange();
    return range;
}

void VisModuleContourSurface::updateOnDataChanged()
    {
    // Update the list of available arrays
    ui->cbSelectedArray->clear();
    for( auto const &arrayName : mDataManager->mArrayNames )
    {
        ui->cbSelectedArray->addItem( arrayName );
    }

    // Update the list of available metrics
    ui->cbMetricForColoring->clear();
    for ( auto const &metricsID : mDataManager->mMetricsRawIDs )
    {
        QString metricsHumanReadable = MetricsUtils::toHumanReadable( metricsID );
        if( metricsHumanReadable != MetricsUtils::METRICS_UNKNOWN )
            ui->cbMetricForColoring->addItem( MetricsUtils::toHumanReadable( metricsID ) );
    }

    // Bind CB selections
    connect( ui->cbSelectedArray, &QComboBox::currentTextChanged, this, &VisModuleContourSurface::onSurfArraySelectionChange );
    connect( ui->cbSelectedArray, &QComboBox::currentTextChanged, this, &VisModuleContourSurface::onColorArraySelectionChange );
    connect( ui->cbMetricForColoring, &QComboBox::currentTextChanged, this, &VisModuleContourSurface::onColorArraySelectionChange );

}
