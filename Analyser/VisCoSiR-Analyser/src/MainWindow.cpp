#include "./ui_mainwindow.h"

#include <MainWindow.h>
#include <QFileDialog>
#include <QMessageBox>
#include <QMovie>
#include <QtCore>
#include <ui_mainwindow.h>
#include <QApplication>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    mInputMessage = MESSAGE_INPUT_DEFAULT;

    // Instantiate managers + references
    mDataManager = std::make_shared<DataManager>();
    mAnalyser3DView = std::shared_ptr<Analyser3DView>( ui->module3DView);

    // Set manager in modules + 3D-View
    ui->moduleGeneralInfo->setDataManager( mDataManager );
    ui->moduleFilter->setDataManager( mDataManager );
    ui->moduleTimesteps->setDataManager( mDataManager );
    ui->moduleContourSurface->setDataManager( mDataManager );
    ui->moduleClipping->setDataManager( mDataManager );
    ui->moduleSamplePoints->setDataManager( mDataManager );
    ui->module3DView->setDataManager( mDataManager );

    // Set reference between filter and timestep module
    ui->moduleFilter->setTimestepModule( ui->moduleTimesteps );

    // Set 3D-View reference in modules
    ui->moduleGeneralInfo->setAnalyser3DView( mAnalyser3DView );
    ui->moduleTimesteps->setAnalyser3DView( mAnalyser3DView );
    ui->moduleClipping->setAnalyser3DView( mAnalyser3DView );
    ui->moduleContourSurface->setAnalyser3DView( mAnalyser3DView );
    ui->moduleSamplePoints->setAnalyser3DView( mAnalyser3DView );

    // This slot-based setup is necessary because the application does not quit on wrong user input otherwise
    QTimer::singleShot(0, this, SLOT(initInputInfoPopup()));

}


MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief Show an information popup to ask the user to select the input folder
 */
void MainWindow::initInputInfoPopup()
{

    auto mainWindow = this;

    // Load style sheet
    QFile styleFile("C:\\Users\\graeblin\\equivalent\\ufz\\VisCoSiR\\code_v2\\style\\Eclippy\\Eclippy.qss");
    styleFile.open(QFile::ReadOnly);
    QString style = QLatin1String(styleFile.readAll());

    QMessageBox msgBox(QMessageBox::Icon::NoIcon, QString::fromStdString("Open Combiner Results"), QString::fromStdString(mInputMessage));
    msgBox.addButton(QMessageBox::Cancel);
    msgBox.addButton(QMessageBox::Open);
    msgBox.setStyleSheet(style);
    int ret = msgBox.exec();

    switch (ret) {
        case QMessageBox::Open:
            // Open the folder selection popup
            initInputFolderSelection();
            break;
        default:
            // Quit the application
            mainWindow->close();
            break;
    }
}

/**
 * @brief Let users select the input folder
 */
void MainWindow::initInputFolderSelection()
{
    QString inputFile = QFileDialog::getOpenFileName( this, tr("Input Folder"), "C:/Users/graeblin/equivalent/ufz/VisCoSiR/data/test-output", tr("VisCoSiR Combination Info (*.json)") );

    if(!inputFile.isNull() && !inputFile.isEmpty() )
    {
        QFileInfo combinerInfoFile = QFileInfo(inputFile);
        bool success = mDataManager->readCombinerInfoFile(combinerInfoFile);
        if(success)
            updateAllVisModules();
        else
        {
            mInputMessage = MESSAGE_INPUT_ERROR + MESSAGE_INPUT_DEFAULT;
            initInputInfoPopup();
        }
    }
    else
    {
        mInputMessage = MESSAGE_INPUT_ERROR + MESSAGE_INPUT_DEFAULT;
        initInputInfoPopup();
    }
}

/**
 * @brief Call the update methods of all visModules when the data in the dataManager changed
 */
void MainWindow::updateAllVisModules()
{
    ui->moduleGeneralInfo->updateOnDataChanged();
    ui->moduleTimesteps->updateOnDataChanged();
    ui->moduleContourSurface->updateOnDataChanged();
    ui->moduleClipping->updateOnDataChanged();
    ui->moduleFilter->updateOnDataChanged();
    ui->moduleSamplePoints->updateOnDataChanged();
}
