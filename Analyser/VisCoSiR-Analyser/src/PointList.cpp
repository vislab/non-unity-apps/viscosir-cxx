#include "PointList.h"
#include "ui_PointList.h"


PointList::PointList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PointList)
{
    ui->setupUi(this);

    InitTable();
}

void PointList::InitTable()
{
    ui->table->clear();
    ui->table->setRowCount( 0 );
    ui->table->setColumnCount( 4 );
    ui->table->setHorizontalHeaderLabels({"Point", "x", "y", "z"});
    ui->table->verticalHeader()->setVisible(false);
    ui->table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->table->setStyleSheet("QHeaderView::section { background-color:black }");
    ui->table->setSelectionMode( QAbstractItemView::SelectionMode::SingleSelection );
    ui->table->setSelectionBehavior( QAbstractItemView::SelectionBehavior::SelectRows );

    // Prepare buttons
    ui->btAdd->setCursor( Qt::PointingHandCursor );
    ui->btRemove->setCursor( Qt::PointingHandCursor );
    connect( ui->btAdd, &QToolButton::clicked, this, &PointList::AddPoint );
    connect( ui->btRemove, &QToolButton::clicked, this, &PointList::RemovePoint );
}

void PointList::AddPoint()
{
    ui->table->insertRow( ui->table->rowCount() );
    std::string name = "P" + std::to_string(ui->table->rowCount());
    ui->table->setItem(ui->table->rowCount()-1, 0, new QTableWidgetItem( QString::fromStdString(name) ) );
    ui->table->setItem(ui->table->rowCount()-1, 1, new QTableWidgetItem( "0" ) );
    ui->table->setItem(ui->table->rowCount()-1, 2, new QTableWidgetItem( "0" ) );
    ui->table->setItem(ui->table->rowCount()-1, 3, new QTableWidgetItem( "0" ) );
}

void PointList::RemovePoint()
{
    if( ! ui->table->selectedRanges().empty() )
    {
        int rowIndex = ui->table->selectedRanges().first().topRow();
        ui->table->removeRow( rowIndex );
    }
}

vtkSmartPointer<vtkPoints> PointList::GetPoints()
{
    vtkSmartPointer<vtkPoints> currentPoints = vtkSmartPointer<vtkPoints>::New();
    currentPoints->SetNumberOfPoints( ui->table->rowCount() );
    for (int index = 0; index < ui->table->rowCount(); ++index) {
        float x = std::stod( ui->table->item( index, 1 )->text().toStdString() );
        float y = std::stod( ui->table->item( index, 2 )->text().toStdString() );
        float z = std::stod( ui->table->item( index, 3 )->text().toStdString() );
        currentPoints->SetPoint( index, new double[3]{x, y, z} );
    }
    return currentPoints;
}


void PointList::SetPoints( vtkSmartPointer<vtkPoints> points )
{
    ui->table->setRowCount( points->GetNumberOfPoints() );
    // Prepare table
    ui->table->clearContents();
    ui->table->setRowCount( points->GetNumberOfPoints() );
    // Fill table
    for (int index = 0; index < points->GetNumberOfPoints(); ++index)
    {
        auto point = points->GetPoint( index );
        std::string x = std::to_string( point[0] );
        std::string y = std::to_string( point[1] );
        std::string z = std::to_string( point[2] );
        std::string name = "P" + std::to_string( index+1 );
        ui->table->setItem(index, 0, new QTableWidgetItem( QString::fromStdString(name) ) );
        ui->table->setItem(index, 1, new QTableWidgetItem( QString::fromStdString(x) ) );
        ui->table->setItem(index, 2, new QTableWidgetItem( QString::fromStdString(y) ) );
        ui->table->setItem(index, 3, new QTableWidgetItem( QString::fromStdString(z) ) );
    }
}

PointList::~PointList()
{
    delete ui;
}


