#include "HighlightMapForSlider.h"
#include "ui_HighlightMapForSlider.h"

#include <QPushButton>
#include <iostream>

HighlightMapForSlider::HighlightMapForSlider(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HighlightMapForSlider)
{
    ui->setupUi(this);

    this->setStyleSheet( "background-color: #900; height: 100px;" );
    this->layout()->setSpacing(0);
    this->layout()->setContentsMargins(0, 0, 0, 0);
}

HighlightMapForSlider::~HighlightMapForSlider()
{
    delete ui;
}

void HighlightMapForSlider::init(int elementCount)
{
    // Create and add elements
    for (int i = 0; i < elementCount; ++i) {
        QPushButton* bt = new QPushButton(this);
        bt->setVisible( true );
        bt->setCursor( QCursor( Qt::PointingHandCursor ) );
        bt->setStyleSheet( QString::fromStdString( btStyle + btStyle_Initial) );
        this->layout()->addWidget( bt );
        mElements.push_back( bt );
    }
}

void HighlightMapForSlider::setHighlights(std::vector<bool> highlights)
{
    if( highlights.size() == mElements.size() )
    {
        // Create and add elements
        for (int i = 0; i < mElements.size(); ++i) {
            if( highlights[i] )
                mElements[i]->setStyleSheet( QString::fromStdString( btStyle + btStyle_Highlight_Enabled ) );
            else
                mElements[i]->setStyleSheet( QString::fromStdString( btStyle + btStyle_Highlight_Disabled ) );
        }
    }
    else
    {
        std::cout << "Error! Highlights-parameter-count (" << highlights.size() << ") does not match the element count (" << mElements.size() << ")" << std::endl;
    }
}
