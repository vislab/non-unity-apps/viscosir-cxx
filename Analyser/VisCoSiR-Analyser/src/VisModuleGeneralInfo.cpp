#include "VisModuleGeneralInfo.h"
#include "ui_VisModuleGeneralInfo.h"

VisModuleGeneralInfo::VisModuleGeneralInfo(QWidget *parent) :
    VisModule(parent),
    ui(new Ui::VisModuleGeneralInfo)
{
    ui->setupUi(this);
}

VisModuleGeneralInfo::~VisModuleGeneralInfo()
{
    delete ui;
}


void VisModuleGeneralInfo::updateOnDataChanged()
{
    ui->lbReferenceData->setText( QString::fromStdString( mDataManager->mNameOfReferenceDataset ) );
    ui->lbOtherData->setText( QString::fromStdString( mDataManager->getListOfOtherDatasets() ) );
}
