#include "VisModuleSamplePoints.h"
#include "ui_VisModuleSamplePoints.h"
#include "vtkPointData.h"
#include "vtkUnstructuredGrid.h"

#include <ColorUtils.h>
#include <MetricsUtils.h>
#include <SamplerUtils.h>

VisModuleSamplePoints::VisModuleSamplePoints(QWidget *parent) :
    VisModule(parent),
    ui(new Ui::VisModuleSamplePoints)
{
    ui->setupUi(this);

    connect( ui->btUpdateVis, &QPushButton::clicked, this, &VisModuleSamplePoints::UpdateSampling );
    ui->btUpdateVis->setCursor(QCursor(Qt::PointingHandCursor));

    // Init point list with a default point
    vtkSmartPointer<vtkPoints> defaultPoints = vtkSmartPointer<vtkPoints>::New();
    defaultPoints->SetNumberOfPoints(1);
    defaultPoints->SetPoint( 0, new double[3]{0.0, 0.0, 0.0} );
    ui->pointList->SetPoints( defaultPoints );

    // Init the chart
    mChart = new QChart();
    auto chartView = static_cast<QChartView*>(ui->samplingResultChart);
    chartView->setChart( mChart );
    chartView->setRenderHint(QPainter::Antialiasing);
    mChart->setAnimationOptions(QChart::AllAnimations);
    chartView->setRubberBand(QChartView::RectangleRubberBand);
    chartView->setVisible( false );
}

VisModuleSamplePoints::~VisModuleSamplePoints()
{
    delete ui;
}

void VisModuleSamplePoints::UpdateSampling()
{
    if( mIsNotYetAddedToTheVisualization )
    {
        initialSetup();
        mIsNotYetAddedToTheVisualization = false;
    }
    else
    {
        updateFiltersAndSamplingWithUsersValues();
    }
}



void VisModuleSamplePoints::initialSetup()
{
    // Set the arrays to be used for coloring
    QString dataName = ui->cbSelectedArray->currentText() + MetricsUtils::METRICS_SEPARATOR + MetricsUtils::toID(ui->cbMetricForColoring->currentText());
    mAnalyser3DView->getOutputOfReader()->GetPointData()->SetActiveScalars( dataName.toStdString().c_str() );
    auto range = mAnalyser3DView->getOutputOfReader()->GetPointData()->GetArray( dataName.toStdString().c_str() )->GetRange();

    // Update vtkPolyData object with current points
    auto currentPoints = ui->pointList->GetPoints();
    mPolydata = vtkSmartPointer<vtkPolyData>::New();
    mPolydata->SetPoints( currentPoints );

    // Resample original dataset with the structure of this vtkPolyData
    mResamplingFilter = vtkSmartPointer<vtkResampleWithDataSet>::New();
    mResamplingFilter->SetMarkBlankPointsAndCells( false );
    // Input specifies the 'structure-provider'
    mResamplingFilter->SetInputData( mPolydata );
    // Source specifies the 'values-provider'
    mResamplingFilter->SetSourceData( mAnalyser3DView->getOutputOfReader() );
    mResamplingFilter->Update();

    // Display values of the current timestep as sphere-glyphs
    mSphereSource = vtkSmartPointer<vtkSphereSource>::New();
    mGlyphFilter = vtkSmartPointer<vtkGlyph3D>::New();
    mGlyphFilter->SetSourceConnection( mSphereSource->GetOutputPort() );
    mGlyphFilter->SetColorModeToColorByScalar();
    mGlyphFilter->SetInputConnection( mResamplingFilter->GetOutputPort() );
    mGlyphFilter->ScalingOff();
    mGlyphFilter->Update();

    // Create a mapper and actor
    mMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mMapper->SetInputConnection(mGlyphFilter->GetOutputPort());
    mMapper->SetScalarRange( range );
    mMapper->SetLookupTable( ColorUtils::GetBlackBodyLut() );
    mActor = vtkSmartPointer<vtkActor>::New();
    mActor->SetMapper( mMapper );

    mAnalyser3DView->addActor(mActor);


    // Display values of all timesteps in the chart
    std::shared_ptr<std::vector<SamplingResult>> samplingResults = std::make_shared<std::vector<SamplingResult>>(std::initializer_list<SamplingResult>{});
    SamplerUtils::SamplePointsInDataset( currentPoints, mDataManager->mTimesteps, dataName.toStdString(), samplingResults );
    updateChartWithSamplingResults( samplingResults );
}


void VisModuleSamplePoints::updateFiltersAndSamplingWithUsersValues()
{
    auto currentPoints = ui->pointList->GetPoints();
    // Update the arrays to be used for coloring
    QString dataName = ui->cbSelectedArray->currentText() + MetricsUtils::METRICS_SEPARATOR + MetricsUtils::toID(ui->cbMetricForColoring->currentText());
    mAnalyser3DView->getOutputOfReader()->GetPointData()->SetActiveScalars( dataName.toStdString().c_str() );
    auto range = mAnalyser3DView->getOutputOfReader()->GetPointData()->GetArray( dataName.toStdString().c_str() )->GetRange();

    // Update the points
    mPolydata->SetPoints( currentPoints );
    mResamplingFilter->SetInputData( mPolydata );
    mResamplingFilter->Modified();
    mResamplingFilter->Update();
    mMapper->SetScalarRange( range );
    mMapper->Modified();
    mMapper->Update();
    mActor->Modified();

    mAnalyser3DView->updateRendering();

    // Display values of all timesteps in the chart
    std::shared_ptr<std::vector<SamplingResult>> samplingResults = std::make_shared<std::vector<SamplingResult>>(std::initializer_list<SamplingResult>{});
    SamplerUtils::SamplePointsInDataset( currentPoints, mDataManager->mTimesteps, dataName.toStdString(), samplingResults );
    updateChartWithSamplingResults( samplingResults );
}


void VisModuleSamplePoints::updateChartWithSamplingResults( std::shared_ptr<std::vector<SamplingResult>> samplingResults )
{
    ui->samplingResultChart->setVisible( true );
    mChart->removeAllSeries();
    for (int index = 0; index < samplingResults->size(); ++index) {
        // Init the reference lines for the chart
        auto lineseries = new QLineSeries();
        lineseries->setName( QString::fromStdString("P" + std::to_string(index+1) ) );
        lineseries->setUseOpenGL(true);
        for (int indexSampledValue = 0; indexSampledValue < samplingResults->at(index).sample.size(); ++indexSampledValue) {
            auto value = samplingResults->at(index).sample.at( indexSampledValue );
            auto time = indexSampledValue+1; //mDataManager->mTimesteps.at( index ).time;
            lineseries->append( QPointF( time, value ) );
        }
        lineseries->setPointsVisible( true );
        lineseries->setColor( ColorUtils::GetCategoricalSix( index ) );
        lineseries->setPointsVisible(true);
        mChart->addSeries( lineseries );
    }

    mChart->createDefaultAxes();
}

void VisModuleSamplePoints::updateOnDataChanged()
{
    // Update the list of available arrays
    ui->cbSelectedArray->clear();
    for( auto const &arrayName : mDataManager->mArrayNames )
    {
        ui->cbSelectedArray->addItem( arrayName );
    }

    // Update the list of available metrics
    ui->cbMetricForColoring->clear();
    for ( auto const &metricsID : mDataManager->mMetricsRawIDs )
    {
        QString metricsHumanReadable = MetricsUtils::toHumanReadable( metricsID );
        if( metricsHumanReadable != MetricsUtils::METRICS_UNKNOWN )
            ui->cbMetricForColoring->addItem( MetricsUtils::toHumanReadable( metricsID ) );
    }
}
